<?php
// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'SKY_Autocomplete_Field' ) )
{
	class SKY_Autocomplete_Field extends SKY_Field_Multiple_Values
	{
		/**
		 * Enqueue scripts and styles
		 *
		 * @return void
		 */
		static function admin_enqueue_scripts()
		{
			wp_enqueue_style( 'sky-autocomplete', SKY_CSS_URL . 'autocomplete.css', array( 'wp-admin' ), SKY_VER );
			wp_enqueue_script( 'sky-autocomplete', SKY_JS_URL . 'autocomplete.js', array( 'jquery-ui-autocomplete' ), SKY_VER, true );
			wp_localize_script( 'sky-autocomplete', 'SKY_Autocomplete', array( 'delete' => __( 'Delete', 'sky-game' ) ) );
		}

		/**
		 * Get field HTML
		 *
		 * @param mixed $meta
		 * @param array $field
		 *
		 * @return string
		 */
		static function html( $meta, $field )
		{
			if ( ! is_array( $meta ) )
				$meta = array( $meta );

			if ( is_string( $field['options'] ) )
			{
				$options = $field['options'];
			}
			else
			{
				$options = array();
				foreach ( $field['options'] as $value => $label )
				{
					$options[] = array(
						'value' => $value,
						'label' => $label,
					);
				}
				$options = wp_json_encode( $options );
			}

			// Input field that triggers autocomplete.
			// This field doesn't store field values, so it doesn't have "name" attribute.
			// The value(s) of the field is store in hidden input(s). See below.
			$html = sprintf(
				'<input type="text" class="sky-autocomplete" id="%s" data-name="%s" data-options="%s" size="%s">',
				$field['id'],
				$field['field_name'],
				esc_attr( $options ),
				$field['size']
			);

			$html .= '<div class="sky-autocomplete-results">';

			// Each value is displayed with label and 'Delete' option
			// The hidden input has to have ".sky-*" class to make clone work
			$tpl = '
				<div class="sky-autocomplete-result">
					<div class="label">%s</div>
					<div class="actions">%s</div>
					<input type="hidden" class="sky-autocomplete-value" name="%s" value="%s">
				</div>
			';

			if ( is_array( $field['options'] ) )
			{
				foreach ( $field['options'] as $value => $label )
				{
					if ( in_array( $value, $meta ) )
					{
						$html .= sprintf(
							$tpl,
							$label,
							__( 'Delete', 'sky-game' ),
							$field['field_name'],
							$value
						);
					}
				}
			}
			else
			{
				foreach ( $meta as $value )
				{
					if ( empty( $value ) )
						continue;
					$label = apply_filters( 'sky_autocomplete_result_label', $value, $field );
					$html .= sprintf(
						$tpl,
						$label,
						__( 'Delete', 'sky-game' ),
						$field['field_name'],
						$value
					);
				}
			}

			$html .= '</div>'; // .sky-autocomplete-results

			return $html;
		}

		/**
		 * Normalize parameters for field
		 *
		 * @param array $field
		 *
		 * @return array
		 */
		static function normalize_field( $field )
		{
			$field = parent::normalize_field( $field );
			$field = wp_parse_args( $field, array(
				'size' => 30,
			) );
			return $field;
		}
	}
}
