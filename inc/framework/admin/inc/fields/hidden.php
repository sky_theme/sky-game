<?php
// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

// Make sure "input" field is loaded
require_once SKY_FIELDS_DIR . 'input.php';

if ( ! class_exists( 'SKY_Hidden_Field' ) )
{
	class SKY_Hidden_Field extends SKY_Input_Field
	{
		/**
		 * Normalize parameters for field
		 *
		 * @param array $field
		 *
		 * @return array
		 */
		static function normalize_field( $field )
		{
			$field['attributes'] = array(
				'name' => $field['field_name'],
				'id'   => $field['clone'] ? false : $field['id']
			);

			return $field;
		}
	}
}
