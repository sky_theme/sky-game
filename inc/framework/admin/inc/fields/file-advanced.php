<?php
// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

require SKY_FIELDS_DIR . 'media.php';
if ( ! class_exists( 'SKY_File_Advanced_Field' ) )
{
	class SKY_File_Advanced_Field extends SKY_Media_Field {}
}
