<?php
/**
 * Creat VC Sky New Ticker
 */
vc_map( 
	array( 
		'base'        => 'sky_new_ticker', 
		'name'        => esc_html__( 'Sky New Ticker', 'sky-game' ), 
		'weight'      => 809,
		'category'    => $category_name, 
		'style' => '', 
		'params'      => array( 
			array( 
				'param_name'  => 'title', 
				'heading'     => esc_html__( 'Title', 'sky-game' ), 
				'type'        => 'textfield', 
				'admin_label' => true, 
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'show',
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Show', 'sky-game' ),
				'std'         => 'post',
				'value'       => 
					array(
						esc_html__( 'Post', 'sky-game' ) => 'post',
						esc_html__( 'Game', 'sky-game' ) => 'game'
					),
				'holder'      => 'div'
			)
		) 
	) 
);