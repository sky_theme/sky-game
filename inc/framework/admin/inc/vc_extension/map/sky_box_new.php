<?php
/**
 * Creat VC Sky Box New
 */
vc_map( 
	array( 
		'base'        => 'sky_box_new', 
		'name'        => esc_html__( 'Sky Box New', 'sky-game' ), 
		'weight'      => 809,
		'category'    => $category_name, 
		'style' => '', 
		'params'      => array( 
			array( 
				'param_name'  => 'title', 
				'heading'     => esc_html__( 'Title', 'sky-game' ), 
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array( 
				'param_name'  => 'icon', 
				'heading'     => esc_html__( 'Icon', 'sky-game' ), 
				'type'        => 'iconpicker',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'show',
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Show', 'sky-game' ),
				'std'         => 'post',
				'value'       => 
					array(
						esc_html__( 'Post', 'sky-game' ) => 'post',
						esc_html__( 'Game', 'sky-game' ) => 'game'
					),
				'holder'      => 'div'
			),
			array(
				'param_name' => 'category_post',
				'heading'    => __( 'Category Post', 'sky-game' ),
				'dependency' => array( 'element' => 'show', 'value' => array( 'post' ) ),
				'type'       => 'dropdown',
				'std'        => 'all',
				'value' 	 => vc_sky_get_list_category('post'),
				'holder'     => 'div'
			),
			array(
				'param_name' => 'category_game',
				'heading'    => __( 'Category Game', 'sky-game' ),
				'dependency' => array( 'element' => 'show', 'value' => array( 'game' ) ),
				'type'       => 'dropdown',
				'std'        => 'all',
				'value' 	 => vc_sky_get_list_category('game'),
				'holder'     => 'div'
			),
		) 
	) 
);