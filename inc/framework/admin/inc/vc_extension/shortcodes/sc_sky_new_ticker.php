<?php
/* -------------------------------------------------------
 * Create functions sky_new_ticker
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_new_ticker' ) ) :
	
	function sky_new_ticker( $atts, $content = null ) {
		wp_enqueue_script( 'sky-ticker' );
		extract(shortcode_atts(array(
			'title' => '',
			'show'  => 'post'
		), $atts));

		if ( $show == 'post' ) :
			$url = get_bloginfo( 'rss2_url' );
		elseif ( $show == 'game' ) :
			$url = get_bloginfo( 'rss2_url' ) . '?post_type=sky-game';
		endif;
		$id = uniqid( 'ticker-id' );
		$html  = '';
		$html .= '<ul id="' . $id . '" class="js-hidden"></ul>';
		$html .= '
				<script type="text/javascript">
				
					jQuery(document).ready(function($) {
				        $(\'#' . $id .'\').ticker({
				        	titleText : \'' . $title .'\',
				            htmlFeed: false,
				            ajaxFeed: true,
				            feedUrl: \'' . $url .'\',
				            feedType: \'xml\'
				        });
					});

				</script>';

		return $html;

	}

	add_shortcode('sky_new_ticker', 'sky_new_ticker');

endif;

/** ====== END sky_new_ticker ====== **/