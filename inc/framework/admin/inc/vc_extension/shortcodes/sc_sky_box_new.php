<?php
/* -------------------------------------------------------
 * Create functions sky_box_new
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_box_new' ) ) :
	
	function sky_box_new( $atts, $content = null ) {
		extract(shortcode_atts(array(
			'title'              => '',
			'icon'               => '',
			'show'               => 'post',
			'category_post'      => 'all',
			'category_game'      => 'all',
		), $atts));

		$html = '';
			
		// ===== <<< checking post type
			$post_type = ( ($show == 'post' ) ? 'post' : 'sky-game' );
			
		// ===== <<< array
			$args = array(
				'post_type'           => $post_type,
				'post_status' 		  => 'publish',
				'posts_per_page'      => 5,
				'ignore_sticky_posts' => true,
			);

		// ===== <<< checking category
			if ( $show == 'post' ) :

				// ===== <<< post query
					if ( $category_post != 'all' ) :
						
						$args['category_name'] = $category_post;

					endif;
					$link_cat = get_category_by_slug( $category_post );

			elseif( $show == 'game' ) :

				// ===== <<< game query
					$args['tax_query'] = array( 'relation' => 'AND' );
					$category_game_slug = sky_get_option_setting( 'sky_general', 'category_game_slug', 'sky-game-cat' );
					if ( $category_game != 'all' ) {
						$args['tax_query'][] = array(
							'taxonomy' => $category_game_slug,
							'field'    => 'slug',
							'terms'    => $category_game
						);
					}
					$link_cat = get_term_link( $category_game );

			endif;

		// =====
			$html .= '<div class="sky_box_new">';
			$html .= '	<div class="item_title">';
			$html .= '		<span class="icon">';
			$html .= '			<i class="' . $icon . '"></i> ';
			$html .= '		</span>';
			$html .= '		<a href="" title="' . $title . '">' . $title . '</a>';
			$html .= '		<span class="more">';
			$html .= '			<a href="" title="' . esc_html__( 'Xem thêm', 'sky-game' ) . '">' .  esc_html__( 'Xem thêm', 'sky-game' )  . '</a>';
			$html .= '		</span>';
			$html .= '	</div>';
			$html .= '	<div class="sky_box_new_container">';

		// ===== Query
			$wp_query = new WP_Query( $args );
			ob_start();
			if ( $wp_query->have_posts() ) :
				$i = 1;
		        while ( $wp_query->have_posts() ) :
					$wp_query->the_post();
						
						if ( $i == 1 ) :

							$html .= '<div class="sky-box-featured">';

							$html .= '	<div class="sky-thumb">';
							$html .= '		<a href="' . get_permalink() . '" title="' . get_the_title( ) . '">';
							$html .= '			<img src="' . sky_get_thumb( ) . '" alt="' . get_the_title( ) . '" />';
							$html .= '		</a>';
							$html .= '	</div>';

							$html .= '	<div class="sky-content">';
							$html .= '		<div class="title">';
							$html .= '			<a href="' . get_permalink() . '" title="' . get_the_title( ) . '">';
							$html .= 				get_the_title();
							$html .= '			</a>';
							$html .= '		</div>';
							$html .= '		<p>';
							$html .= 			sky_substr( get_the_excerpt(), 23 ) . '&nbsp';
							$html .= '		</p>';
							$html .= '	</div>';
							$html .= '</div>';

						else :

							$html .= '		<div class="item">';
							$html .= '			<a href="' . get_permalink() . '" title="' . get_the_title( ) . '">';
							$html .= 				get_the_title();
							$html .= '			</a>';
							$html .= '		</div>';

						endif;

					$i++;
				endwhile;
				
				// Restore original Post Data
				wp_reset_postdata();
				
			else :
				$html .= sprintf( '<p class="no_post">%s</p>', sky_html_content_filter( __( 'Sorry, no posts matched your criteria.', 'sky-game' ) ) );
			endif;

		// ====
			$html .= '	</div><!-- /.sky_box_new_container -->';
			$html .= '</div><!-- /.sky_box_new -->';
			echo ob_get_clean();
			return $html;
	}

	add_shortcode( 'sky_box_new', 'sky_box_new' );

endif;

/** ====== END noo_list_job ====== **/

