<?php
/* -------------------------------------------------------
 * Create functions sky_recent_post
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_recent_post' ) ) :
	
	function sky_recent_post( $atts, $content = null ) {
		if ( isset( $_POST['load_paging_ajax'] ) ) $atts = $_POST['atts'][0];
		extract(shortcode_atts(array(
			'title'              => '',
			'show'               => 'post',
			'category_post'      => 'all',
			'category_game'      => 'all',
			'style'              => 'list',
			'column'             => '5',
			'posts_per_page'     => '10',
			'orderby'            => 'date',
			'order'              => 'desc',
			'show_pagination'    => 'no',
			'show_readmore'      => 'yes',
			'show_link_download' => 'yes',
			'show_excerpt' 		 => 'yes',
			'show_thumbnail'     => 'yes',
			'show_support' 		 => 'yes'
		), $atts));
		$id_result = uniqid( 'result_' );
		$data = '';
		$html = '';
		foreach ($atts as $key => $value) :

			$data .= " data-{$key}=\"{$value}\"";

		endforeach;
		if ( ! isset( $_POST['load_paging_ajax'] ) ) :
			$html .= '<div class="sky_recent_post ' . $style . '">';
			$html .= '<div class="item_title">' . $title . '</div>';
		endif;

				// ===== <<< Get paging number

					// ===== <<< Checking load paging ajax
						if ( isset( $_POST['load_paging_ajax'] ) ) :
							$process_ajax = true;
							if ( isset( $_POST['paging'] ) ) :

								$paged = $_POST['paging'];

							endif;

						else :
					// ===== <<< Default get paging url
							if( is_front_page() || is_home()) :
								$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : ( ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1 );
							else :
								$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
							endif;

						endif;
					
				// ===== <<< checking post type
					$post_type = ( ($show == 'post' ) ? 'post' : 'sky-game' );
					
				// ===== <<< array
					$args = array(
						'post_type'           => $post_type,
						'paged'               => $paged,
						'post_status' 		  => 'publish',
						'posts_per_page'      => $posts_per_page,
						'ignore_sticky_posts' => true,
					);

				// ===== <<< checking category
					if ( $show == 'post' ) :

						// ===== <<< post query
							if ( $category_post != 'all' ) :
								
								$args['category_name'] = $category_post;

							endif;

					elseif( $show == 'game' ) :

						// ===== <<< game query
							$args['tax_query'] = array( 'relation' => 'AND' );
							$category_game_slug = sky_get_option_setting( 'sky_general', 'category_game_slug', 'sky-game-cat' );
							if ( $category_game != 'all' ) {
								$args['tax_query'][] = array(
									'taxonomy' => $category_game_slug,
									'field'    => 'slug',
									'terms'    => $category_game
								);
							}

					endif;

				//  -- Check order by......
				
					if ( $orderby == 'view' ) {
						$args['orderby'] = 'meta_value_num';
						$args['meta_key'] = '_sky_views_count';
			 		} elseif ( $orderby == 'date' ) {
			 			$args['orderby'] = 'date';
			 		} else {
			 			$args['orderby'] = 'rand';
			 		}

			 	//  -- Check order
			 		if ( $orderby != 'rand' ) :
				 		if ( $order == 'asc' ) {
				 			$args['order'] = 'ASC';
				 		} else {
				 			$args['order'] = 'DESC';
				 		}
				 	endif;

				// --- Process column

				$wp_query = new WP_Query( $args );
				ob_start();
				$html .= '<div id="' . $id_result .'"' . $data .'>';
				if ( $wp_query->have_posts() ) :
			        while ( $wp_query->have_posts() ) :
						$wp_query->the_post();
						$html .= '<div class="sky-item ' . (($style == 'grid') ? 'sky-xs-6 sky-sm-4 sky-md-' .$column : '') . '">';
						$html .= '<div class="sky-game-item">';
						
						if($show_thumbnail == 'yes'):

							$html .= '<div class="sky-thumb">';
							$html .= '<a href="' . get_permalink() . '" title="' . get_the_title( ) . '">';
							$html .= '<img src="' . sky_get_thumb( ) . '" alt="' . get_the_title( ) . '" />';
							$html .= '</a>';
							$html .= '</div>';

						endif;
						
						$html .= '<div class="sky-entry">';
						$html .= '<a href="' . get_permalink() .'" title="' . get_the_title( ) . '">';
						$html .= '<h4>';
						if($show_thumbnail == 'no'):
							$html .= '<i class="fa fa-angle-double-right"></i>';
						endif;
						$html .= get_the_title( );
						$html .= '</h4>';
						$html .= '</a>';
						
						if( ( $show == 'game' ) && ( $show_support == 'yes' ) ) :
							$html .= '<p>';
							$html .= esc_html__( 'Hỗ trợ &nbsp;', 'sky-game' );
							$html .= sky_get_list_support_os( true );
							$html .= '</p>';
							
							elseif ( $show == 'post' ) :

								$html .= '<p>';
								$html .= sky_substr( get_the_excerpt(), 13 ) . '&nbsp';
								$html .= '</p>';

							endif;
							
							if( ($show == 'game') && ($show_link_download == 'yes') ) :

								$html .= '<a target="_blank" href="' . sky_get_url_download() . '" title="' . esc_html__( 'Tải về', 'sky-game') . '">';
								$html .= '<span class="sky-btn-download">';
								$html .= '<i class="fa fa-download"></i>&nbsp; ' . esc_html__( 'Tải về', 'sky-game' );
								$html .= '</span>';
								$html .= '</a>';

							endif;
							
							if($show == 'post' && $show_readmore == 'yes') :

								$html .= '<a class="sky-read-more" href="' . get_permalink() .'">' . esc_html__( 'Chi tiết', 'sky-game' ) .'<i class="fa fa-arrow-circle-right"></i></a>';
							
							endif;

						$html .= '		</div>';
						$html .= '	</div>';
						$html .= '</div>';

					endwhile;
					if( isset($process_ajax) ) wp_die();
					$html .= '</div><!-- /.' . $id_result . ' -->';
					
					// Restore original Post Data
					$html .= wp_reset_postdata();
					$total = $wp_query->found_posts / $posts_per_page;
					if ( $show_pagination == 'yes' && $total > 1 ) :
						$html .= sky_pagination( true, $total, 2, $id_result );
						$i = null;
					endif;
				else :
					$html .= sprintf( '<p class="no_post">%s</p>', sky_html_content_filter( __( 'Sorry, no posts matched your criteria.', 'sky-game' ) ) );
				endif;
				$html .= '</div><!-- /.sky_recent_post -->';
				echo ob_get_clean();
				return $html;
	}

	add_action( 'wp_ajax_recent_post', 'sky_recent_post' );
	add_action( 'wp_ajax_nopriv_recent_post', 'sky_recent_post' );
	add_shortcode( 'sky_recent_post', 'sky_recent_post' );

endif;

/** ====== END noo_list_job ====== **/

