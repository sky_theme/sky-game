<?php
/* -------------------------------------------------------
 * Create functions sky_shortcode_offered_client
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_shortcode_offered_client' ) ) :
	
	function sky_shortcode_offered_client( $atts, $content = null ) {

		extract(shortcode_atts(array(
			'title'                  => '',
			'sub_title'              => ''
		), $atts));
		$terms = get_terms( sky_get_option_setting( 'sky_general', 'offered_game_slug', 'sky-game-offered' ), array(
		    'orderby'    => 'count',
		    'hide_empty' => 0
		) );

		$html = '';

		if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) :

			$html .= '<div class="sky-xs-12 sky-sm-3 sky_offered_client">';
				if ( !empty($title) ) :
					$html .= '<h2 class="offered_title">' . $title . '</h2>';
					$html .= '<h3 class="sub_offered_title">' . $sub_title . '</h3>';
				endif;

		    $html .= '</div>';

		    $html .= '<div class="sky-xs-12 sky-sm-9 sky_offered_client_list">';

			foreach( $terms as $term ) :

	            $html .= '<div class="sky_offered_client_item">';
	            $html .= '	<a href="' . get_term_link( $term ) . '" title="' . $term->name . '">';
	            $html .= '		<img src="' . wp_get_attachment_url( $term->term_id ) . '" title="' . $term->name . '" alt="' . $term->name . '" width="60px" height="60px" />';
	            $html .= '	</a>';
	            $html .= '</div>';

			endforeach;

			$html .= '</div>';
		    $html .= '<script type="text/javascript">
					jQuery(document).ready(function($) {
						var client = $(".sky_offered_client_list");
					  	client.owlCarousel({
							items : 3,
							autoPlay: 7500,
					  	});
					});
				</script>';

			$html .= wp_reset_postdata();
			return $html;
				

		endif;

	}

	add_shortcode('sky_offered_client', 'sky_shortcode_offered_client');

endif;

/** ====== END sky_list_job ====== **/

