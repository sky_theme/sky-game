<?php
$options[]            = array(
  'name'              => 'sky_header_panel',
  'title'             => esc_html__( 'Sky Header', 'sky-game' ),
  'sections'          => array(
    array(
      'name'              => 'sky_header_logo',
      'title'             => esc_html__( 'Logo', 'sky-game' ),
      'settings'          => array(

        // === <<< Swich using logo image
          array(
            'name'          => 'using_logo_image',
            'control'       => array(
              'type'        => 'sky_field',
              'options'     => array(
                'type'       => 'switcher',
                'title'      => esc_html__( 'Using Logo Images', 'sky-game' ),
                'label'      => esc_html__( 'Do you want to ?', 'sky-game' ),
              ),
            ),
          ),

        // === <<< Section Not check using logo image
          // === <<< Add text logo
            array(
              'name'          => 'text_logo',
              'control'       => array(
                'type'        => 'sky_field',
                'options'     => array(
                  'type'       => 'text',
                  'title'      => esc_html__( 'Add Your Text', 'sky-game' ),
                  'dependency' => array( 'using_logo_image', '==', 'false' ),
                ),
              ),
              'default'       => get_bloginfo( 'name' )
            ),


        // === <<< Section check using logo image
          // === <<< Select logo image
            array(
              'name'          => 'image_logo',
              'control'       => array(
                'type'      => 'sky_field',
                'options'   => array(
                  'type'       => 'image',
                  'title'      => esc_html__( 'Upload Logo', 'sky-game' ),
                  'dependency' => array( 'using_logo_image', '==', 'true' ),
                ),
              ),
            ),

        // === <<< Section check using logo image mobile
          // === <<< Select logo image mobile
            array(
              'name'          => 'image_logo_mobile',
              'control'       => array(
                'type'      => 'sky_field',
                'options'   => array(
                  'type'       => 'image',
                  'title'      => esc_html__( 'Upload Logo Mobile', 'sky-game' ),
                  'dependency' => array( 'using_logo_image', '==', 'true' ),
                ),
              ),
            ),

      )
    ),
    array(
      'name'              => 'sky_navbar',
      'title'             => esc_html__( 'Navbar', 'sky-game' ),
      'settings'          => array(
          // === <<< Nav style Fixed and Static

          array(
            'name'          => 'nav_style',
            'control'       => array(
              'label'       => esc_html__('Navbar Style','sky-game'),
              'type'        => 'radio',
              'std'         => 'fixed',
              'choices'     => array(
                'fixed'      => esc_html__('Fixed','sky-game'),
                'static'      => esc_html__('Static','sky-game'),
              )
            ),
          ),

      )
    )
  )
);