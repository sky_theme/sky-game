<?php
$options[]            = array(
  'name'              => 'sky_footer_panel',
  'title'             => esc_html__( 'Sky Footer', 'sky-game' ),
  'sections'          => array(
    array(
      'name'              => 'sky_footer_column',
      'title'             => esc_html__( 'Sky Footer Column', 'sky-game' ),
      'settings'          => array(

        // === <<< Sky Footer Column
          // array(
          //   'name'          => 'sky_footer_column',
          //   'control'       => array(
          //     'type'        => 'sky_field',
          //     'options'     => array(
          //         'type'      => 'subheading',
          //         'content'   => esc_html__( 'Sky Footer Column', 'sky-game' ),
          //     ),
          //   ),
          // ),

        // === <<< Select Column
          array(
            'name'      => 'number_footer_column',
            'control'   => array(
              'type'    => 'select',
               'title'  => esc_html__( 'Select Column', 'sky-game' ),
              'choices' => array(
                'none'  =>  esc_html__( 'None (No Footer Main Content)', 'sky-game' ),
                '1'     => esc_html__( 'One', 'sky-game' ),
                '2'     => esc_html__( 'Two', 'sky-game' ),
                '3'     => esc_html__( 'Three', 'sky-game' ),
                '4'     => esc_html__( 'Four', 'sky-game' ),
              )
            ),
          ),

        // === <<< Swich Background Footer
          array(
            'name'          => 'background_column_footer',
            'control'       => array(
              'type'        => 'sky_field',
              'options'     => array(
                'type'       => 'switcher',
                'title'      => esc_html__( 'Background Column', 'sky-game' ),
                'label'      => esc_html__( 'Do you want to ?', 'sky-game' ),
              ),
            ),
          ),

        // === <<< Section Not check background
          // === <<< Select color
            array(
              'name'        => 'color_footer_column',
              'default'     => '#333',
              'control'     => array(
                'type'      => 'sky_field',
                'options'   => array(
                  'type'       => 'color_picker',
                  'title'      => esc_html__( 'Color Background', 'sky-game' ),
                  'dependency' => array( 'background_column_footer', '==', 'false' ),
                ),
              ),
            ),


        // === <<< Section check background
          // === <<< Select background image
            array(
              'name'          => 'background_footer_column',
              'control'       => array(
                'type'      => 'sky_field',
                'options'   => array(
                  'type'       => 'image',
                  'title'      => esc_html__( 'Image Background', 'sky-game' ),
                  'dependency' => array( 'background_column_footer', '==', 'true' ),
                ),
              ),
            ),

      )
    ),
    array(
      'name'              => 'sky_footer_copyright',
      'title'             => esc_html__( 'Sky Footer Copyright', 'sky-game' ),
      'settings'          => array(

        // === <<< Text Copyright
          array(
            'name'          => 'sky_copyright',
            'control'       => array(
              'type'        => 'sky_field',
              'options'     => array(
                'type'       => 'textarea',
                'title'      => esc_html__( 'Bottom Bar Content (HTML)', 'sky-game' ),
                'attributes' => array(
                  'placeholder' => esc_html__( 'Enter your text...', 'sky-game' ),
                  'rows'        => 10,
                )
              ),
            ),
          ),

        // === <<< List Socials
          // === <<< Sky Footer Column
          array(
            'name'          => 'sky_footer_sub_social',
            'control'       => array(
              'type'        => 'sky_field',
              'options'     => array(
                  'type'      => 'subheading',
                  'content'   => esc_html__( 'Socical Page', 'sky-game' ),
              ),
            ),
          ),
          array(
            'name'          => 'sky_footer_social_fb',
            'control'       => array(
              'type'        => 'sky_field',
              'options'     => array(
                'type'       => 'text',
                'title'      => esc_html__( 'Facebook', 'sky-game' )
              ),
            ),
          ),

          array(
            'name'          => 'sky_footer_social_gg',
            'control'       => array(
              'type'        => 'sky_field',
              'options'     => array(
                'type'       => 'text',
                'title'      => esc_html__( 'Google', 'sky-game' )
              ),
            ),
          ),

          array(
            'name'          => 'sky_footer_social_tw',
            'control'       => array(
              'type'        => 'sky_field',
              'options'     => array(
                'type'       => 'text',
                'title'      => esc_html__( 'Twitter', 'sky-game' )
              ),
            ),
          ),

          array(
            'name'          => 'sky_footer_social_in',
            'control'       => array(
              'type'        => 'sky_field',
              'options'     => array(
                'type'       => 'text',
                'title'      => esc_html__( 'Linkedin', 'sky-game' )
              ),
            ),
          ),

          array(
            'name'          => 'sky_footer_social_yt',
            'control'       => array(
              'type'        => 'sky_field',
              'options'     => array(
                'type'       => 'text',
                'title'      => esc_html__( 'Youtube', 'sky-game' )
              ),
            ),
          ),

      )
    )
  )
);