<?php
/**
 * Create a taxonomy
 *
 * @uses  Inserts new taxonomy object into the list
 * @uses  Adds query vars
 *
 * @param string  Name of taxonomy object
 * @param array|string  Name of the object type for the taxonomy object.
 * @param array|string  Taxonomy arguments
 * @return null|WP_Error WP_Error if errors, otherwise null.
 * @link https://codex.wordpress.org/Function_Reference/register_taxonomy
 *
 * @package Sky Game
 */


/* -------------------------------------------------------
 * Create functions register_taxonomy_sky_game_category
 * ------------------------------------------------------- */

if ( ! function_exists( 'register_taxonomy_sky_game_category' ) ) :
	function register_taxonomy_sky_game_category() {
		$cat_slug = sky_get_option_setting( 'sky_general', 'category_game_slug', 'sky-game-cat' );
		$labels = array(
			'name'					=> esc_html__( 'Category Game', 'sky-game' ),
			'singular_name'			=> esc_html__( 'Category Game', 'sky-game' ),
			'search_items'			=> esc_html__( 'Search Category Game', 'sky-game' ),
			'popular_items'			=> esc_html__( 'Popular Category Game', 'sky-game' ),
			'all_items'				=> esc_html__( 'All Category Game', 'sky-game' ),
			'parent_item'			=> esc_html__( 'Parent Category Game', 'sky-game' ),
			'parent_item_colon'		=> esc_html__( 'Parent Category Game', 'sky-game' ),
			'edit_item'				=> esc_html__( 'Edit Category Game', 'sky-game' ),
			'update_item'			=> esc_html__( 'Update Category Game', 'sky-game' ),
			'add_new_item'			=> esc_html__( 'Add Category Game', 'sky-game' ),
			'new_item_name'			=> esc_html__( 'New Game', 'sky-game' ),
			'add_or_remove_items'	=> esc_html__( 'Add or remove Category Game', 'sky-game' ),
			'choose_from_most_used'	=> esc_html__( 'Choose from most used sky-game', 'sky-game' ),
			'menu_name'				=> esc_html__( 'Category Game', 'sky-game' ),
		);

		$args = array(
			'labels'            => $labels,
			'public'            => true,
			'show_in_nav_menus' => true,
			'show_admin_column' => false,
			'hierarchical'      => true,
			'show_tagcloud'     => false,
			'show_ui'           => true,
			'query_var'         => true,
			'rewrite'           => true,
			'query_var'         => true,
			'capabilities'      => array(),
		);

		register_taxonomy( $cat_slug, array( 'sky-game' ), $args );
	}

	add_action( 'init', 'register_taxonomy_sky_game_category' );
endif;

/** ====== END register_taxonomy_sky_game_category ====== **/


/* -------------------------------------------------------
 * Create functions register_taxonomy_sky_game_offered
 * ------------------------------------------------------- */

if ( ! function_exists( 'register_taxonomy_sky_game_offered' ) ) :
	function register_taxonomy_sky_game_offered() {
		$offered_slug = sky_get_option_setting( 'sky_general', 'offered_game_slug', 'sky-game-offered' );
		$labels = array(
			'name'					=> esc_html__( 'Offered Game', 'sky-game' ),
			'singular_name'			=> esc_html__( 'Offered Game', 'sky-game' ),
			'search_items'			=> esc_html__( 'Search Offered Game', 'sky-game' ),
			'popular_items'			=> esc_html__( 'Popular Offered Game', 'sky-game' ),
			'all_items'				=> esc_html__( 'All Offered Game', 'sky-game' ),
			'parent_item'			=> esc_html__( 'Parent Offered Game', 'sky-game' ),
			'parent_item_colon'		=> esc_html__( 'Parent Offered Game', 'sky-game' ),
			'edit_item'				=> esc_html__( 'Edit Offered Game', 'sky-game' ),
			'update_item'			=> esc_html__( 'Update Offered Game', 'sky-game' ),
			'add_new_item'			=> esc_html__( 'Add Offered Game', 'sky-game' ),
			'new_item_name'			=> esc_html__( 'New Game', 'sky-game' ),
			'add_or_remove_items'	=> esc_html__( 'Add or remove Offered Game', 'sky-game' ),
			'choose_from_most_used'	=> esc_html__( 'Choose from most used', 'sky-game' ),
			'menu_name'				=> esc_html__( 'Offered Game', 'sky-game' ),
		);

		$args = array(
			'labels'            => $labels,
			'public'            => true,
			'show_in_nav_menus' => true,
			'show_admin_column' => false,
			'hierarchical'      => true,
			'show_tagcloud'     => false,
			'show_ui'           => true,
			'query_var'         => true,
			'rewrite'           => true,
			'query_var'         => true,
			'capabilities'      => array(),
		);

		register_taxonomy( $offered_slug, array( 'sky-game' ), $args );
	}

	add_action( 'init', 'register_taxonomy_sky_game_offered' );
endif;

/** ====== END register_taxonomy_sky_game_offered ====== **/


/* -------------------------------------------------------
 * Create functions register_taxonomy_sky_game_support_os
 * ------------------------------------------------------- */

if ( ! function_exists( 'register_taxonomy_sky_game_support_os' ) ) :
	function register_taxonomy_sky_game_support_os() {
		$os_slug = sky_get_option_setting( 'sky_general', 'support_os_slug', 'sky-game-support-os' );
		$labels = array(
			'name'					=> esc_html__( 'Support OS', 'sky-game' ),
			'singular_name'			=> esc_html__( 'Support OS', 'sky-game' ),
			'search_items'			=> esc_html__( 'Search OS', 'sky-game' ),
			'popular_items'			=> esc_html__( 'Popular OS', 'sky-game' ),
			'all_items'				=> esc_html__( 'All OS', 'sky-game' ),
			'parent_item'			=> esc_html__( 'Parent OS', 'sky-game' ),
			'parent_item_colon'		=> esc_html__( 'Parent OS', 'sky-game' ),
			'edit_item'				=> esc_html__( 'Edit OS', 'sky-game' ),
			'update_item'			=> esc_html__( 'Update OS', 'sky-game' ),
			'add_new_item'			=> esc_html__( 'Add OS New', 'sky-game' ),
			'new_item_name'			=> esc_html__( 'New OS', 'sky-game' ),
			'add_or_remove_items'	=> esc_html__( 'Add or remove OS', 'sky-game' ),
			'choose_from_most_used'	=> esc_html__( 'Choose from most used sky-game', 'sky-game' ),
			'menu_name'				=> esc_html__( 'Support OS', 'sky-game' ),
		);

		$args = array(
			'labels'            => $labels,
			'public'            => true,
			'show_in_nav_menus' => true,
			'show_admin_column' => false,
			'hierarchical'      => true,
			'show_tagcloud'     => false,
			'show_ui'           => true,
			'query_var'         => true,
			'rewrite'           => true,
			'query_var'         => true,
			'capabilities'      => array(),
		);

		register_taxonomy( $os_slug, array( 'sky-game' ), $args );
	}

	add_action( 'init', 'register_taxonomy_sky_game_support_os' );
endif;

/** ====== END register_taxonomy_sky_game_support_os ====== **/

/* -------------------------------------------------------
 * Create functions register_taxonomy_sky_game_support_screen
 * ------------------------------------------------------- */

if ( ! function_exists( 'register_taxonomy_sky_game_support_screen' ) ) :
	function register_taxonomy_sky_game_support_screen() {
		$screen_slug = sky_get_option_setting( 'sky_general', 'support_screen_slug', 'sky-game-support-screen' );
		$labels = array(
			'name'					=> esc_html__( 'Support Screen', 'sky-game' ),
			'singular_name'			=> esc_html__( 'Support Screen', 'sky-game' ),
			'search_items'			=> esc_html__( 'Search Screen', 'sky-game' ),
			'popular_items'			=> esc_html__( 'Popular Screen', 'sky-game' ),
			'all_items'				=> esc_html__( 'All Screen', 'sky-game' ),
			'parent_item'			=> esc_html__( 'Parent Screen', 'sky-game' ),
			'parent_item_colon'		=> esc_html__( 'Parent Screen', 'sky-game' ),
			'edit_item'				=> esc_html__( 'Edit Screen', 'sky-game' ),
			'update_item'			=> esc_html__( 'Update Screen', 'sky-game' ),
			'add_new_item'			=> esc_html__( 'Add Screen New', 'sky-game' ),
			'new_item_name'			=> esc_html__( 'New Screen', 'sky-game' ),
			'add_or_remove_items'	=> esc_html__( 'Add or remove Screen', 'sky-game' ),
			'choose_from_most_used'	=> esc_html__( 'Choose from most used sky-game', 'sky-game' ),
			'menu_name'				=> esc_html__( 'Support Screen', 'sky-game' ),
		);

		$args = array(
			'labels'            => $labels,
			'public'            => true,
			'show_in_nav_menus' => true,
			'show_admin_column' => false,
			'hierarchical'      => true,
			'show_tagcloud'     => false,
			'show_ui'           => true,
			'query_var'         => true,
			'rewrite'           => true,
			'query_var'         => true,
			'capabilities'      => array(),
		);

		register_taxonomy( $screen_slug, array( 'sky-game' ), $args );
	}

	add_action( 'init', 'register_taxonomy_sky_game_support_screen' );
endif;

/** ====== END register_taxonomy_sky_game_support_screen ====== **/

/* -------------------------------------------------------
 * Create functions register_taxonomy_sky_game_tags
 * ------------------------------------------------------- */

if ( ! function_exists( 'register_taxonomy_sky_game_tags' ) ) :
	function register_taxonomy_sky_game_tags() {
		$tag_slug = sky_get_option_setting( 'sky_general', 'tag_slug', 'sky-game-tags' );
		$labels = array(
			'name'					=> esc_html__( 'Tags Game', 'sky-game' ),
			'singular_name'			=> esc_html__( 'Tags Game', 'sky-game' ),
			'search_items'			=> esc_html__( 'Search Tags Game', 'sky-game' ),
			'popular_items'			=> esc_html__( 'Popular Tags Game', 'sky-game' ),
			'all_items'				=> esc_html__( 'All Tags Game', 'sky-game' ),
			'parent_item'			=> esc_html__( 'Parent Tags Game', 'sky-game' ),
			'parent_item_colon'		=> esc_html__( 'Parent Tags Game', 'sky-game' ),
			'edit_item'				=> esc_html__( 'Edit Tags Game', 'sky-game' ),
			'update_item'			=> esc_html__( 'Update Tags Game', 'sky-game' ),
			'add_new_item'			=> esc_html__( 'Add Tags Game New', 'sky-game' ),
			'new_item_name'			=> esc_html__( 'New Tags Game', 'sky-game' ),
			'add_or_remove_items'	=> esc_html__( 'Add or remove Tags Game', 'sky-game' ),
			'choose_from_most_used'	=> esc_html__( 'Choose from most used sky-game', 'sky-game' ),
			'menu_name'				=> esc_html__( 'Tags Game', 'sky-game' ),
		);

		$args = array(
			'labels'            => $labels,
			'public'            => true,
			'show_in_nav_menus' => true,
			'show_admin_column' => false,
			'hierarchical'      => false,
			'show_tagcloud'     => true,
			'show_ui'           => true,
			'query_var'         => true,
			'rewrite'           => true,
			'query_var'         => true,
			'capabilities'      => array(),
		);

		register_taxonomy( $tag_slug, array( 'sky-game' ), $args );
	}

	add_action( 'init', 'register_taxonomy_sky_game_tags' );
endif;

/** ====== END register_taxonomy_sky_game_tags ====== **/

/* -------------------------------------------------------
 * Create functions sky_add_thumbnail_offered_game
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_add_thumbnail_offered_game' ) ) :
	
	function sky_add_thumbnail_offered_game() {

		if(function_exists( 'wp_enqueue_media' )){
			wp_enqueue_media();
		}else{
			wp_enqueue_style('thickbox');
			wp_enqueue_script('media-upload');
			wp_enqueue_script('thickbox');
		}
		?>
		<div class="form-field">
			<label><?php esc_html_e( 'Thumbnail', 'sky-game' ); ?></label>
			<div id="offered_thumnail" style="float:left;margin-right:10px;">
				<img src="" width="60px" height="60px" />
			</div>
			<div style="line-height:60px;">
				<input type="hidden" id="offered_thumbnail_id" name="offered_thumbnail_id" />
				<button type="button" class="upload_image_button button"><?php esc_html_e('Upload/Add image', 'sky-game'); ?></button>
				<button type="button" class="remove_image_button button"><?php esc_html_e('Remove image', 'sky-game'); ?></button>
			</div>
			<script type="text/javascript">
				
				 // Only show the "remove image" button when needed
				 if ( ! jQuery('#offered_thumbnail_id').val() )
					 jQuery('.remove_image_button').hide();
		
				// Uploading files
				var file_frame;
		
				jQuery(document).on( 'click', '.upload_image_button', function( event ){
		
					event.preventDefault();
		
					// If the media frame already exists, reopen it.
					if ( file_frame ) {
						file_frame.open();
						return;
					}
		
					// Create the media frame.
					file_frame = wp.media.frames.downloadable_file = wp.media({
						title: "<?php esc_html_e( 'Choose an image', 'sky-game' ); ?>",
						button: {
							text: "<?php esc_html_e( 'Use image', 'sky-game' ); ?>",
						},
						multiple: false
					});
		
					// When an image is selected, run a callback.
					file_frame.on( 'select', function() {
						attachment = file_frame.state().get('selection').first().toJSON();
		
						jQuery('#offered_thumbnail_id').val( attachment.id );
						jQuery('#offered_thumnail img').attr('src', attachment.url );
						jQuery('.remove_image_button').show();
					});
		
					// Finally, open the modal.
					file_frame.open();
				});
		
				jQuery(document).on( 'click', '.remove_image_button', function( event ){
					jQuery('#offered_thumnail img').attr('src', '<?php echo SKY_SET_THUMBNAIL_DEFAULT; ?>');
					jQuery('#offered_thumbnail_id').val('');
					jQuery('.remove_image_button').hide();
					return false;
				});
		
			</script>
			<div class="clear"></div>
		</div>
		<?php

	}

	add_action( sky_get_option_setting( 'sky_general', 'offered_game_slug', 'sky-game-offered' ) .'_add_form_fields', 'sky_add_thumbnail_offered_game' );

endif;

/** ====== END sky_add_thumbnail_offered_game ====== **/

/* -------------------------------------------------------
 * Create functions sky_edit_thumbnail_offered_game
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_edit_thumbnail_offered_game' ) ) :
	
	function sky_edit_thumbnail_offered_game( $term, $taxonomy ) {

		if(function_exists( 'wp_enqueue_media' )){
			wp_enqueue_media();
		}else{
			wp_enqueue_style('thickbox');
			wp_enqueue_script('media-upload');
			wp_enqueue_script('thickbox');
		}
		$sky_cat = get_option( 'sky_offered_game_thumnail' );
		$image 			= '';
		$offered_thumbnail_id 	= isset($sky_cat[$term->term_id]) ? $sky_cat[$term->term_id] : '';
		if ($offered_thumbnail_id) :
			$image = wp_get_attachment_url( $offered_thumbnail_id );
		else :
			$image = SKY_SET_THUMBNAIL_DEFAULT;
		endif;
		?>
		<tr class="form-field">
			<th scope="row" valign="top"><label><?php esc_html_e('Thumbnail', 'sky-game'); ?></label></th>
			<td>
				<div id="offered_thumnail" style="float:left;margin-right:10px;"><img src="<?php echo $image; ?>" width="60px" height="60px" /></div>
				<div style="line-height:60px;">
					<input type="hidden" id="offered_thumbnail_id" name="offered_thumbnail_id" value="<?php echo $offered_thumbnail_id; ?>" />
					<button type="button" class="upload_image_button button"><?php esc_html_e('Upload/Add image', 'sky-game'); ?></button>
					<button type="button" class="remove_image_button button"><?php esc_html_e('Remove image', 'sky-game'); ?></button>
				</div>
				<script type="text/javascript">
	
					jQuery(function(){
	
						 // Only show the "remove image" button when needed
						 if ( ! jQuery('#offered_thumbnail_id').val() )
							 jQuery('.remove_image_button').hide();
	
						// Uploading files
						var file_frame;
	
						jQuery(document).on( 'click', '.upload_image_button', function( event ){
	
							event.preventDefault();
	
							// If the media frame already exists, reopen it.
							if ( file_frame ) {
								file_frame.open();
								return;
							}
	
							// Create the media frame.
							file_frame = wp.media.frames.downloadable_file = wp.media({
								title: "<?php esc_html_e( 'Choose an image', 'sky-game' ); ?>",
								button: {
									text: "<?php esc_html_e( 'Use image', 'sky-game' ); ?>",
								},
								multiple: false
							});
	
							// When an image is selected, run a callback.
							file_frame.on( 'select', function() {
								attachment = file_frame.state().get('selection').first().toJSON();
	
								jQuery('#offered_thumbnail_id').val( attachment.id );
								jQuery('#offered_thumnail img').attr('src', attachment.url );
								jQuery('.remove_image_button').show();
							});
	
							// Finally, open the modal.
							file_frame.open();
						});
	
						jQuery(document).on( 'click', '.remove_image_button', function( event ){
							jQuery('#offered_thumnail img').attr('src', '<?php echo SKY_SET_THUMBNAIL_DEFAULT; ?>');
							jQuery('#offered_thumbnail_id').val('');
							jQuery('.remove_image_button').hide();
							return false;
						});
					});
	
				</script>
				<div class="clear"></div>
			</td>
		</tr>
		<?php

	}

	add_action( sky_get_option_setting( 'sky_general', 'offered_game_slug', 'sky-game-offered' ) .'_edit_form_fields' , 'sky_edit_thumbnail_offered_game', 10, 3 );

endif;

/** ====== END sky_edit_thumbnail_offered_game ====== **/

/* -------------------------------------------------------
 * Create functions sky_save_thumbnail_offered_game
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_save_thumbnail_offered_game' ) ) :
	
	function sky_save_thumbnail_offered_game( $term_id, $tt_id, $taxonomy ) {

		if ( isset( $_POST['offered_thumbnail_id'] ) ){
			$sky_cat = get_option( 'sky_offered_game_thumnail' );
			if ( ! $sky_cat )
				$sky_cat = array();
			$sky_cat[$term_id] = absint($_POST['offered_thumbnail_id']);
			update_option('sky_offered_game_thumnail', $sky_cat);
		}	

	}

	add_action( 'created_term', 'sky_save_thumbnail_offered_game', 10, 3 );
	add_action( 'edit_term', 'sky_save_thumbnail_offered_game', 10, 3 );

endif;

/** ====== END sky_save_thumbnail_offered_game ====== **/