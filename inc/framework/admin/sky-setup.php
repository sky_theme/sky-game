<?php
// ===== <<< [ Checking ABSPATH root ] >>> ===== //
	defined('ABSPATH') || ( header( "HTTP/1.1 403 Forbidden" ) & die( "403.14 - Directory listing denied." ) );

/**
 * Display notices in admin.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/* -------------------------------------------------------
 * Create functions sky_allowed_html
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_allowed_html' ) ) :
	
	function sky_allowed_html( $content = '' ) {

		$allowed_html = array(
			'a' => array(
				'href' => array(),
				'target' => array(),
				'title' => array(),
				'rel' => array(),
				'class' => array(),
				'style' => array(),
			),
			'img' => array(
				'src' => array(),
				'class' => array(),
				'style' => array(),
			),
			'h1' => array(),
			'h2' => array(),
			'h3' => array(),
			'h4' => array(),
			'h5' => array(),
			'p' => array(
				'class' => array(),
				'style' => array()
			),
			'br' => array(
				'class' => array(),
				'style' => array()
			),
			'hr' => array(
				'class' => array(),
				'style' => array()
			),
			'span' => array(
				'class' => array(),
				'style' => array()
			),
			'em' => array(
				'class' => array(),
				'style' => array()
			),
			'strong' => array(
				'class' => array(),
				'style' => array()
			),
			'small' => array(
				'class' => array(),
				'style' => array()
			),
			'b' => array(
				'class' => array(),
				'style' => array()
			),
			'i' => array(
				'class' => array(),
				'style' => array()
			),
			'u' => array(
				'class' => array(),
				'style' => array()
			),
			'ul' => array(
				'class' => array(),
				'style' => array()
			),
			'ol' => array(
				'class' => array(),
				'style' => array()
			),
			'li' => array(
				'class' => array(),
				'style' => array()
			),
			'blockquote' => array(
				'class' => array(),
				'style' => array()
			),
		);

		return apply_filters( 'sky_allowed_html', $allowed_html );

	}

endif;

/** ====== END sky_allowed_html ====== **/

/* -------------------------------------------------------
 * Create functions sky_html_content_filter
 * Allow only unharmed HTML tag.
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_html_content_filter' ) ) :
	
	function sky_html_content_filter( $content = '' ) {

		return wp_kses( $content, sky_allowed_html() );

	}

endif;

/** ====== END sky_html_content_filter ====== **/

/* -------------------------------------------------------
 * Create functions notice_html_install
 * ------------------------------------------------------- */
class Sky_Notice_Install {

	private $product_id = 'sky-game';

	private $install_option_group = 'sky_option_install_group';

	private $install_option_name = 'sky_option_install_name';

	private $install_section_id = 'sky_option_section_id';

	private $option_metabox = array();

	public function __construct() {
		if ( current_user_can( 'manage_options' ) ) {

			add_action( 'admin_notices', array( $this, 'notice_html_install' ) );
			add_action( 'admin_menu', array( $this, 'admin_menus' ) );

			add_action('sky_setting_general', array(&$this,'sky_general_options'));
			// add_action('sky_setting_import_demo', array(&$this,'sky_import_demo_options'));

			// -- Call setting fields
				add_action('admin_init', array( $this, 'sky_settings_fields'));
			// -- set ajax
				add_action( 'wp_ajax_sky_setup', array( $this, 'sky_setup' ) );

			require SKY_THEME_INCLUDES_FRAMEWORK . '/admin/sky-import.php';

		}
	}
	
	// -- Notice html
	
	public function notice_html_install() {
		if ( !$this->sky_get_option('disable_notice_install') && !isset($_GET['page']) == 'sky-setup' ) :

		?>
		<div id="message" class="updated notice is-dismissible">
			<p><strong><?php echo sprintf( esc_html__('Welcome to %s,', 'sky-game'), wp_get_theme($this->product_id)->get('Name') ); ?></strong></p>
			<p><?php esc_html_e('If it is the first time you install this theme, you should go and check the basic setting.', 'sky-game'); ?></p>
			<p class="submit">
				<a href="<?php echo esc_url( admin_url( 'admin.php?page=sky-setup' ) ); ?>" class="button-primary">
					<?php esc_html_e( 'Go to Quick Setup', 'sky-game' ); ?>
				</a>
				<a href="<?php echo esc_url( admin_url( 'admin.php?page=sky-setup&action=skip' ) ); ?>" class="button">
					<?php esc_html_e( 'Skip Setup', 'sky-game' ); ?>
				</a>
			</p>
		</div>
		<?php

		endif;
	}

	// -- Create page
		public function admin_menus() {
			add_menu_page ( 
				esc_html__( 'Sky Settings', 'sky-game' ), 
				esc_html__( 'Sky Settings', 'sky-game' ), 
				'manage_options', 
				'sky-setup', 
				array( $this, 'sky_page_setup' ),
				SKY_THEME_ASSETS_URI . '/images/icon-sky-16x16.png'
			);
		}

	// -- Processsing setup
		public function sky_setup() {
			global $wpdb;

			// ----
			
				$page['title'] = $_POST['title'];
				$page['content'] = isset( $_POST['content'] ) && !empty( $_POST['content'] ) ? $_POST['content'] : '';
				$page['page_template'] = isset( $_POST['page_template'] ) && !empty( $_POST['page_template'] ) ? $_POST['page_template'] : 'default';
				$page['setting_group'] = isset( $_POST['setting_group'] ) && !empty( $_POST['setting_group'] ) ? $_POST['setting_group'] : '';
				$page['setting_key'] = isset( $_POST['setting_key'] ) && !empty( $_POST['setting_key'] ) ? $_POST['setting_key'] : '';

			// -----

				$post_data = array(
					'post_title' => $page['title'],
					'post_content'  => $page['content'],
					'post_status' => 'publish',
					'post_type'   => 'page',
					'post_author' => get_current_user_id()
				);

				$id_page = wp_insert_post( $post_data ); // -- Insert post
			
				if( !is_wp_error( $id_page ) ) {
					update_post_meta($id_page, '_wp_page_template',  $page['page_template']); //- Set page template
					if( !empty( $page['setting_key'] ) ) {
						if( !empty( $page['setting_group'] ) ) {
							$setting_group = get_option( $page['setting_group'] );
							$setting_group[ $page['setting_key'] ] = $id_page;
							update_option( $page['setting_group'], $setting_group );
						} else {
							$setting_value = get_option( $page['setting_key'] );
							update_option( $page['setting_key'], $id_page );
						}
					}

					$post = get_post($id_page);
					echo json_encode( array( 'id' => esc_html__('Done', 'sky-game'), 'slug' => $post->post_name ) );
				}
				exit;
		}

	// -- Add option
		public function sky_add_option( $name ) {
			$options = array_merge( get_option( 'sky_setup', array() ), $name );
			update_option( 'sky_setup', $options );
		}

	// -- Get option
		public function sky_get_option( $name ) {
			$options = get_option( 'sky_setup' );
			return $options[$name];
		}

	// -- Tab menu
		public function sky_page_setup() {
			$current_tab = empty( $_GET['tab'] ) ? 'general' : sanitize_title( $_GET['tab'] );

			$tabs        = apply_filters( 'sky_settings_tabs_array', array(
				'general'     => esc_html__("Quick Setup", 'sky-game'), 
				// 'import_demo' => esc_html__("Import Demo", 'sky-game')
			));
			echo '<div class="wrap sky-settings">';
				if ( $current_tab != 'general' ) : echo '<form action="options.php" method="post">'; endif;
				    echo '<h2 class="nav-tab-wrapper">';
				    foreach( $tabs as $tab => $name ) :
				        
				        $class = ($tab == $current_tab) ? 'nav-tab-active' : '';
				        echo '<a class="nav-tab ' . $class . ' ' . $tab . '" href="?page=sky-setup&tab=' . $tab . '">' . $name . '</a>';
				    
				    endforeach;
				    echo '</h2>';

				    do_action( 'sky_setting_' . $current_tab );

				    if ( $current_tab != 'general' && $current_tab != 'sky_optimize_database_settings' ) :
					
						submit_button(esc_html__('Save Changes','sky-game'));

					endif;

				if ( $current_tab != 'general' ) : echo '</form>'; endif;
		    echo '</div>';

		} 

	// -- OPTION
	
		// -- General options
		public function sky_general_options() {
			if ( isset( $_GET['action'] ) == 'skip' ) :
				$this->sky_add_option( array( 'disable_notice_install' => true ) );
				wp_redirect( admin_url() );
				die;
			endif;
			if ( isset( $_POST['license_key'] ) ) :
				$value_license = array(
					'license_key' => $_POST['license_key'],
					'email' => $_POST['email'],
				);
				update_option( $this->product_id . '-license-settings', $value_license );

				echo '
					<div id="setting-error-settings_updated" class="updated settings-error notice is-dismissible"> 
						<p><strong>Settings saved.</strong></p>
						<button type="button" class="notice-dismiss">
							<span class="screen-reader-text">Dismiss this notice.</span>
						</button>
					</div>';

				$this->sky_add_option( array( 'disable_notice_install' => true ) );
				
				// ----
					if ( isset( $_POST['sky_general_settings'] ) && !empty( $_POST['sky_general_settings'] ) ) :

						update_option( 'sky_general_settings', $_POST['sky_general_settings'] );

					else :

						update_option( 'sky_general_settings', array() );

					endif;

			endif;
			echo '<div class="wrap">';
	        echo '<form method="post">';

			$this->sky_option_client();
			$this->sky_option_page();
			$this->sky_option_general();

			echo '<p>';
			submit_button('', 'primary', '', false);
			echo '&nbsp;&nbsp;';
			echo '<a href="' . esc_url( admin_url( 'admin.php?page=sky-setup&action=skip' ) ) . '" class="button">';
			esc_html_e( 'Cancel', 'sky-game' );
			echo '</a>';
			echo '</p>';

			echo '</form>';
			echo '</div>';

		}

			// -- Option client
			public function sky_option_client() {
				$settings_field_name = $this->get_product_field_name();
				$settings_group_id = $this->product_id . '-license-settings-group';
            	$options = get_option( $settings_field_name );
				?>
					<table class="widefat" cellspacing="0" id="client">
						<thead>
							<tr>
								<th colspan="3" data-export-label="<?php esc_html_e( 'License', 'sky-game' ); ?>">
									<label>
										<?php esc_html_e( 'License', 'sky-game' ); ?>
									</label>
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td data-export-label="<?php esc_html_e( 'License key', 'sky-game' ); ?>">
									<?php esc_html_e( 'License key:', 'sky-game' ); ?>
								</td>
								<td class="help">
									<a href="#" title="" class="help_tip">[?]</a>
								</td>
								<td>
									<input type='text' name='license_key' value='<?php echo $options['license_key']; ?>' class='regular-text'>
									<input type='hidden' name='email' value='<?php echo str_replace( 'http://', '', home_url( )); ?>' class='regular-text'>
								</td>
							</tr>
						</tbody>
					</table>
				<?php
			}

			// -- Option page
			public function sky_option_page() {
				
				?>
					<table class="widefat" cellspacing="0">
						<thead>
							<tr>
								<th colspan="5" data-export-label="<?php esc_html_e( 'Required Pages', 'sky-game' ); ?>"><?php esc_html_e( 'Required Pages', 'sky-game' ); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php
								global $wpdb;

								$list_pages = array(
									array(
										'title'         => esc_html__('Download' , 'sky-game'),
										'content'       => '[sky_download_page]1',
										'page_template' => 'page-download.php',
										'help'          => esc_html__('The page for download file', 'sky-game'),
										'setting'       => array(),
									)
								);

								foreach ($list_pages as $list_page => $page) :
									echo '<tr>';

									$get_id = null;
									$get_page = null;

									// Check the setting first
									$setting = $page['setting'];
									$setting_value = null;
									$setting_result = '';
									if( isset( $setting ) && !empty( $setting ) && is_array($setting) && !empty( $setting['key'] ) ) :
										if( !empty( $setting['group'] ) ) {
											$setting_group = get_option( $setting['group'] );
											$setting_value = is_array( $setting_group ) && isset( $setting_group[ $setting['key'] ] ) ? $setting_group[ $setting['key'] ] : null;
										} else {
											$setting_value = get_option( $setting['key'] );
										}

										if( $setting_value == null ) :
											$setting_result = 'missing';
										else :
											$setting_result = 'false';
											if( isset( $setting['value'] ) && $setting['value'] == $setting_value ) {
												$setting_result = 'true';
											} elseif( is_numeric( $setting_value ) ) {
												$get_id = absint( $setting_value );
												$get_page = get_post( $get_id );
												if( !empty( $get_page ) && $get_page->post_type == 'page' ) {

													// There's setting, check if the setting satisfy the other condition: page template and page content.
													if( ( isset( $page['page_template'] ) && !empty( $page['page_template'] ) && $page['page_template'] == sky_get_post_meta( $get_id, '_wp_page_template' ) ) 
														|| ( isset( $page['content'] ) && !empty( $page['content'] ) && strpos( $get_page->post_content, $page['content'] ) !== false  ) ) {
														$setting_result = 'true';
													}
												}
											}
										endif;


										if( !empty( $get_id ) ) {
											echo "<td data-export-label='{$page['title']}'>";
											echo "<a href='" . get_edit_post_link( $get_id ) . "' title='{$page['title']}' target='_blank'>{$page['title']}</a>";
											echo "</td>";
										} else {
											echo "<td data-export-label='{$page['title']}'>{$page['title']}</td>";
										}
										if( isset( $page['help'] ) && !empty( $page['help'] ) ) {
											echo '<td class="help">';
											echo '	<a href="#" class="help_tip" title="' . $page['help'] . '">[?]</a>';
											echo '</td>';
										}

										echo '<td>';
										if( $setting_result == 'missing' ) {
											echo "	<mark class='error'>" . esc_html__( 'Missing setting', 'sky-game' ) . "</mark>";
										} elseif( $setting_result == 'false' ) {
											echo "<mark class='error'>" . esc_html__( 'Wrong', 'sky-game' );
											if( !empty( $page['content'] ) ) {
												echo ' - ' . sprintf( esc_html__( 'Page should contains %s', 'sky-game' ), $page['content'] );
											} elseif ( !empty( $page['page_template'] ) ) {
												echo ' - ' . sprintf( esc_html__( 'Page should have template %s', 'sky-game' ), $page['page_template'] );
											}
											echo "</mark>";
										} else {
											echo "<mark class='yes'>" . esc_html__('Done', 'sky-game');
											if( !empty( $get_id ) && !empty( $get_page ) ) {
												echo " - /{$get_page->post_name}/";
											}
											echo '</mark>';
										}
										echo '</td>';

										echo '<td>';
										if( $setting_result !== 'true' && ( !empty( $page['content'] ) || !empty( $page['page_template'] ) ) ) {
											echo '	<div class="button button-primary">';
											echo '		<span class="correct-setting" data-title="' . $page['title'] . '" data-content="' . $page['content'] . '" data-page-template="' . $page['page_template'] . '" data-setting-group="' . $setting['group'] . '" data-setting-key="' . $setting['key'] . '">' . esc_html__('Correct now', 'sky-game') . '</span>';
											echo '	</div>';
										}
										echo '</td>';
										echo '<td>';
										if( $setting_result != 'true' && isset( $setting['url']) && !empty( $setting['url'] ) ) {
											echo '	<div class="button">';
											echo "		<a href='{$setting['url']}' title='{$page['title']}' target='_blank'>" . esc_html__("Edit setting", "sky-game") . "</a>";
											echo '	</div>';
										}
										echo '</td>';

									else :
										if ( !empty($page['page_template']) ) :
											$get_id = sky_get_page_id_by_template($page['page_template']);
											$get_page = !empty( $get_id ) ? get_post( $get_id ) : null;
											
											if( !empty( $get_id ) && !empty( $get_page ) ) {

												echo "<td data-export-label='{$page['title']}'>";
												echo "<a href='" . get_edit_post_link( $get_id ) . "' title='{$page['title']}' target='_blank'>{$page['title']}</a>";
												echo "</td>";
												if( isset( $page['help'] ) && !empty( $page['help'] ) ) {
													echo '<td class="help">';
													echo '	<a href="#" class="help_tip" title="' . $page['help'] . '">[?]</a>';
													echo '</td>';
												}
												echo '<td>';
												echo "	<mark class='yes'>" . esc_html__('Done', 'sky-game') . " - /{$get_page->post_name}/</mark>";
												echo '</td>';
											} else {
												echo "<td data-export-label='{$page['title']}'>{$page['title']}</td>";
												if( isset( $page['help'] ) && !empty( $page['help'] ) ) {
													echo '<td class="help">';
													echo '	<a href="#" class="help_tip" title="' . $page['help'] . '">[?]</a>';
													echo '</td>';
												}
												echo '<td>';
												echo "	<mark class='error'>" . sprintf( esc_html__( 'You need to create a page with template %s', 'sky-game' ), $page['page_template'] ) . "</mark>";
												echo '</td>';
												echo '<td>';
												echo '	<div class="button button-primary">';
												echo '		<span class="correct-setting" data-title="' . $page['title'] . '" data-content="' . $page['content'] . '" data-page-template="' . $page['page_template'] . '">' . esc_html__('Correct now', 'sky-game') . '</span>';
												echo '	</div>';
												echo '</td>';
												echo '<td></td>';
											}
										else :
											echo '<td colspan="4"></td>';
										endif;
									endif;

									echo '</tr>';
								endforeach;
								
							?>
						</tbody>
					</table>
				<?php
			}

		// ===== <<<< general option
		public function sky_option_general() {
			$option = get_option( 'sky_general_settings' );
			?>
				<table class="widefat" cellspacing="0">
					<thead>
						<tr>
							<th colspan="5" data-export-label="<?php esc_html_e( 'General Settings', 'sky-game' ); ?>"><?php esc_html_e( 'General Settings', 'sky-game' ); ?></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td width="40%"><?php esc_html_e( 'Disable Header', 'sky-game' ); ?></td>
							<td width="60%"><input type="checkbox" name="sky_general_settings[disable_header_clear]" value="1" <?php ( isset($option['disable_header_clear']) ? checked( $option['disable_header_clear'], 1 ) : '' ); ?>></td>
						</tr>
						<tr>
							<td width="40%"><?php esc_html_e( 'Disable Auto Ping', 'sky-game' ); ?></td>
							<td width="60%"><input type="checkbox" name="sky_general_settings[disable_auto_ping]" value="1" <?php ( isset($option['disable_auto_ping']) ? checked( $option['disable_auto_ping'], 1 ) : '' ); ?>></td>
						</tr>
						<tr>
							<td width="40%"><?php esc_html_e( 'Disable Compressed Website', 'sky-game' ); ?></td>
							<td width="60%"><input type="checkbox" name="sky_general_settings[disable_compressed]" value="1" <?php ( isset($option['disable_compressed']) ? checked( $option['disable_compressed'], 1 ) : '' ); ?>></td>
						</tr>
						<tr>
							<td width="40%"><?php esc_html_e( 'Disable Remove Version', 'sky-game' ); ?></td>
							<td width="60%">
								<input type="checkbox" name="sky_general_settings[disable_remove_version]" value="1" <?php ( isset($option['disable_remove_version']) ? checked( $option['disable_remove_version'], 1 ) : '' ); ?>>
								<small><?php esc_html_e( 'If you choose, every link js, css will have tails ?ver=, it will cause delays pages.', 'sky-game' ); ?></small>
							</td>
						</tr>
						<tr>
							<td width="40%"><?php esc_html_e( 'Disable Nofollow Out Link', 'sky-game' ); ?></td>
							<td width="60%">
								<input type="checkbox" name="sky_general_settings[disable_nofollow_out_link]" value="1" <?php ( isset($option['disable_nofollow_out_link']) ? checked( $option['disable_nofollow_out_link'], 1 ) : '' ); ?>>
								<small><?php esc_html_e( 'If you choose, all external links within the article content will dofollow, not good for seo.', 'sky-game' ); ?></small>
							</td>
						</tr>
						<tr>
							<td width="40%"><?php esc_html_e( 'Disable Seo Friendly Images', 'sky-game' ); ?></td>
							<td width="60%">
								<input type="checkbox" name="sky_general_settings[disable_seo_friendly_images]" value="1" <?php ( isset($option['disable_seo_friendly_images']) ? checked( $option['disable_seo_friendly_images'], 1 ) : '' ); ?>>
								<small><?php esc_html_e( 'If you choose, will not auto add tag title, alt in image, not good for seo.', 'sky-game' ); ?></small>
							</td>
						</tr>
					</tbody>
				</table>
			<?php

		}

		// -- Tools options
		public function sky_import_demo_options() {

			$list_demo = array(
				array(
					'name' => esc_html__( 'Demo 1', 'sky-game'),
					'img'  => SKY_THEME_INCLUDES_FRAMEWORK_URI . "/admin/data/demo-1/screenshot.jpg",
					'file' => 'demo-1'
				),
				array(
					'name' => esc_html__( 'Demo 2', 'sky-game'),
					'img'  => SKY_THEME_INCLUDES_FRAMEWORK_URI . "/admin/data/demo-2/screenshot.jpg",
					'file' => 'demo-2'
				),
				array(
					'name' => esc_html__( 'Demo 3', 'sky-game'),
					'img'  => SKY_THEME_INCLUDES_FRAMEWORK_URI . "/admin/data/demo-3/screenshot.jpg",
					'file' => 'demo-3'
				),
				array(
					'name' => esc_html__( 'Demo 4', 'sky-game'),
					'img'  => SKY_THEME_INCLUDES_FRAMEWORK_URI . "/admin/data/demo-4/screenshot.jpg",
					'file' => 'demo-4'
				)
			);

			?>
				<table class="widefat" cellspacing="0" style="width: 99%;">
					<thead>
						<tr>
							<th colspan="1" data-export-label="<?php esc_html_e( 'Settings', 'sky-game' ); ?>">
								<label class="hide_main">
									<?php esc_html_e( 'Settings', 'sky-game' ); ?>
								</label>
							</th>
						</tr>
					</thead>
					<tbody id="sky_main_select">
						<p></p>
						<tr>
							<td>
								<input type='checkbox' data-id='import_post' id='import_post' value='1' checked /> <?php esc_html_e( 'Import Post', 'sky-game' ); ?>
							</td>
						</tr>
						
						<tr>
							<td>
								<input type='checkbox'data-id='import_nav' id='import_nav' value='1' checked /> <?php esc_html_e( 'Import Nav Menu', 'sky-game' ); ?>
							</td>
						</tr>
						<tr>
							<td>
								<input type='checkbox' data-id='import_comment' id='import_comment' value='1' checked /> <?php esc_html_e( 'Import Comment', 'sky-game' ); ?>
							</td>
						</tr>
					</tbody>
				</table>

				<div id="sky_tools">
					
					<!-- [ MAIN ] -->
					<div id="process_import"></div>
					<div class="theme-browser rendered" style="margin-top: 20px;">
						<div class="themes">
							<?php foreach ($list_demo as $id => $demo) : ?>
								<div class="theme" tabindex="0">
									<div class="theme-screenshot">
										<img src="<?php echo $demo['img']; ?>" alt="" />
									</div>
									<span class="more-details" id="install_<?php echo $demo['file']; ?>"><?php esc_html_e( 'Install ' .$demo['name'], 'sky-game' ); ?></span>
									<h3 class="theme-name" id="sky-<?php echo $demo['file']; ?>-name"><?php echo $demo['name']; ?></h3>
									<div class="theme-actions">
										<button class="install-demo button button-secondary activate" 
											data-name="<?php echo $demo['file']; ?>"
											data-import-post="true"
											data-import-nav="true"
											data-import-comment="true"
										><?php esc_html_e( 'Install Demo', 'noo' ); ?></button>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
						<br class="clear">
					</div>
				
				</div><!-- /#sky_tools -->

			<?php

		}

	// -- Setting fields
		
		public function get_product_field_name() {
            return $this->product_id . '-license-settings';
        }
		
		public function sky_settings_fields() {

			register_setting( $this->install_option_group, $this->install_option_name );
		    add_settings_section( 
		    	$this->install_section_id, 
		    	'Title Section', 
		    	'',
		    	$this->install_option_group 
		    );
		    add_settings_field( 
		    	$this->product_id . '-page',
		    	'Page', 
		    	array( $this, 'render_settings_section' ),
		    	$this->install_option_group,
		    	$this->install_section_id 
		    );


		}

		/**
         * Renders the description for the settings section.
         */
        public function render_settings_section() {
        }

	// -- Options fields
		public function sky_options_fields() {

			$this->option_metabox[] = array(

				'id' => 'general_options',
				'title' => esc_html_e( 'General Options', 'sky-game' )

			);

			return $this->option_metabox;			

		}

}
new Sky_Notice_Install;