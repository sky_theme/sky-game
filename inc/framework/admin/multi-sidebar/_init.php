<?php

// Do not allow direct access to this file.
if( ! function_exists('add_action') ) 
	die();

/**
 * Plugin version
 *
 * Get the current plugin version.
 * 
 * @return string 
 */
function sky_sidebar_version(){
	if( is_admin() ){
		$data = get_file_data( __FILE__, array( 'Version' ) );
		return empty( $data ) ? '' : $data[0];
	}
	else{
		return false;
	}
}

/**
 * All conditions
 *
 * All condtions will be accessible from this function
 *
 * @return array All conditions type => class_name 
 */
function sky_sidebar_conditions_filter(){
	return apply_filters( 'sky_sidebar_conditions_filter', array() );
}

/**
 * Register a condition
 *
 * Register a condition and inject it in the main array
 *
 * @param string $name Condition class name
 * @return void 
 */
class Sky_Multi_Sidebar_Generator_Register_Condition{
	public $name;
	public $allCond;

	public function __construct( $name ){
		$this->name = $name;
		$this->allCond = sky_sidebar_conditions_filter();
		add_filter( 'sky_sidebar_conditions_filter', array( $this, 'add') );
	}
	public function add(){
		if( class_exists( $this->name ) ){
			$class = new $this->name;
			if( ! array_key_exists($class->type, $this->allCond) ){
				return array( $class->type => $this->name );
			}
		}
	}
}

/**
 * Register condition helper
 *
 * @param string $name Condition class name
 * @use Sky_Multi_Sidebar_Generator_Register_Condition
 * @return void 
 */
function sky_register_condition( $name ){
	new Sky_Multi_Sidebar_Generator_Register_Condition( $name );
}

/*
-------------------------------------------------------------------------------
Smk Sidebar function
-------------------------------------------------------------------------------
*/
function sky_sidebar($id){
	if(function_exists('dynamic_sidebar') && dynamic_sidebar($id)) : 
	endif;
	return true;
}

/*
-------------------------------------------------------------------------------
Smk All Sidebars
-------------------------------------------------------------------------------
*/
if(! function_exists('sky_get_all_sidebars') ) {
	function sky_get_all_sidebars(){
		global $wp_registered_sidebars;
		$all_sidebars = array();
		if ( $wp_registered_sidebars && ! is_wp_error( $wp_registered_sidebars ) ) {
			
			foreach ( $wp_registered_sidebars as $sidebar ) {
				$all_sidebars[ $sidebar['id'] ] = $sidebar['name'];
			}
			
		}
		return $all_sidebars;
	}
}

/*
----------------------------------------------------------------------
Shortcode
----------------------------------------------------------------------
*/
// [sky_sidebar id="X"] //X is the sidebar ID
function sky_sidebar_shortcode( $atts ) {
	
	extract( shortcode_atts( array(
		'id' => null,
	), $atts ) );
	sky_sidebar($id);
}
add_shortcode( 'sky_sidebar', 'sky_sidebar_shortcode' );

/* Plugin path
------------------------------------------------*/
$path = plugin_dir_path( __FILE__ );

/* HTML helper
------------------------------------------------*/
require $path . 'html.php';

/* Conditions
------------------------------------------------*/
require $path . 'condition.php';
require $path . 'condition-cpt.php';

/* Init conditions
------------------------------------------------*/
sky_register_condition( 'Sky_Multi_Sidebar_Generator_Condition_Cpt' );

/* Plugin work
------------------------------------------------*/
require $path . 'abstract.php';
require $path . 'render.php';
require $path . 'apply.php';

/* Init plugin
------------------------------------------------*/
$sky_sidebar_generator = new Sky_Multi_Sidebar_Generator;
$sky_sidebar_generator->init();

/* Apply conditions
------------------------------------------------*/
$applySidebars = new Sky_Multi_Sidebar_Generator_Apply;