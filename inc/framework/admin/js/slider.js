jQuery( function( $ )
{
	'use strict';

	function sky_update_slider()
	{
		var $input = $( this ),
			$slider = $input.siblings( '.sky-slider' ),
			$valueLabel = $slider.siblings( '.sky-slider-value-label' ).find( 'span' ),
			value = $input.val(),
			options = $slider.data( 'options' );

		$slider.html( '' );

		if ( !value )
		{
			value = 0;
			$input.val( 0 );
			$valueLabel.text( '0' );
		}
		else
		{
			$valueLabel.text( value );
		}

		// Assign field value and callback function when slide
		options.value = value;
		options.slide = function( event, ui )
		{
			$input.val( ui.value );
			$valueLabel.text( ui.value );
		};

		$slider.slider( options );
	}

	$( ':input.sky-slider-value' ).each( sky_update_slider );
	$( '.sky-input' ).on( 'clone', ':input.sky-slider-value', sky_update_slider );
} );
