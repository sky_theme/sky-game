<?php
/**
* Registers a new post type
* @uses $wp_post_types Inserts new post type object into the list
*
* @param string  Post type key, must not exceed 20 characters
* @param array|string  See optional args description above.
* @return object|WP_Error the registered post type object, or an error object
*/
function register_sky_post_type() {

	$labels = array(
		'name'                => esc_html__( 'Sky Game', 'sky-game' ),
		'singular_name'       => esc_html__( 'Sky Game', 'sky-game' ),
		'add_new'             => esc_html__( 'Add New Game', 'sky-game' ),
		'add_new_item'        => esc_html__( 'Add New Game', 'sky-game' ),
		'edit_item'           => esc_html__( 'Edit Game', 'sky-game' ),
		'new_item'            => esc_html__( 'New Game', 'sky-game' ),
		'view_item'           => esc_html__( 'View Game', 'sky-game' ),
		'search_items'        => esc_html__( 'Search Game', 'sky-game' ),
		'not_found'           => esc_html__( 'No Game found', 'sky-game' ),
		'not_found_in_trash'  => esc_html__( 'No Game found in Trash', 'sky-game' ),
		'parent_item_colon'   => esc_html__( 'Parent Game:', 'sky-game' ),
		'menu_name'           => esc_html__( 'Sky Game', 'sky-game' ),
	);

	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => 'description',
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 6,
		'menu_icon'           => 'dashicons-awards',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => array(
			'slug'	=> sky_get_option_setting( 'sky_general', 'game_slug', 'game' ),
        	'with_front'  => false,
	  	),
		'capability_type'     => 'post',
		'supports'            => array(
			'title', 'editor', 'thumbnail', 'comments'
		),
		'feed'=> true,
		'pages'=> true
	);

	register_post_type( 'sky-game', $args );
}


/* -------------------------------------------------------
 * Create functions sky_add_table_columns
 * Adds table columns to the Contacts WP_List_Table
 *
 * @param array $columns Existing Columns
 * @return array New Columns
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_add_table_columns' ) ) :
	
	function sky_add_table_columns( $columns ) {
		unset($columns['date']);
	   	unset($columns['title']);
	   	unset($columns['tags']);
	   	unset($columns['comments']);

		$columns['cb']        = esc_html__( '<input type="checkbox" />', 'sky-game' );
		$columns['thumbnail'] = esc_html__( 'Thumbnail', 'sky-game' );
		$columns['title']     = esc_html__( 'Game', 'sky-game' );
		$columns['posted']    = esc_html__( 'Posted', 'sky-game' );
		$columns['os']        = esc_html__( 'OS Support', 'sky-game' );
		$columns['cat']       = esc_html__( 'Category', 'sky-game' );
		$columns['view']      = esc_html__( 'Viewed', 'sky-game' );
	     
	    return $columns;

	}

endif;

/** ====== END sky_add_table_columns ====== **/

/* -------------------------------------------------------
 * Create functions sky_show_table_columns
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_show_table_columns' ) ) :
	
	function sky_show_table_columns( $column, $post_id ) {
		global $post;
		switch ( $column ) {

			case 'thumbnail':

				echo '<a href="' . get_permalink( $post_id ) . '" title="' . get_the_title( $post_id ) . '">';
				echo '<img width="80px" src="' . sky_get_thumb( $post_id ) . '" title="' . get_the_title( $post_id ) . '" />';
				echo '</a>';

			break;

			case 'os':

				$args = array('orderby' => 'ID', 'order' => 'ASC', 'fields' => 'all');
				$list_os = wp_get_post_terms( $post_id, sky_get_option_setting( 'sky_general', 'support_os_slug', 'sky-game-support-os' ), $args );
				foreach ($list_os as $os) :
					
					$os_key = $os->taxonomy . '_' . $os->term_id;
					$os_meta = sky_get_option_setting( $os_key );
					echo '<a target="_blank" href="' . get_term_link( $os->slug, $os->taxonomy ) . '" title="'. $os->name . "\n" . $os->description .'">';
					echo '<i style="color: ' .$os_meta['color_icon'] . '; font-size: 25px;" class="fa ' .$os_meta['os_icon'] . '"></i>&nbsp; ';
					echo '</a>';

				endforeach;

			break;

			case 'cat':

				$args = array('orderby' => 'ID', 'order' => 'ASC', 'fields' => 'all');
				$list_cat = wp_get_post_terms( $post_id, sky_get_option_setting( 'sky_general', 'category_game_slug', 'sky-game-cat' ), $args );
				$pam = 0;
				$total_cat = count($list_cat);
				foreach ($list_cat as $cat) :
					
					$cat_key = $cat->taxonomy . '_' . $cat->term_id;
					$cat_meta = sky_get_option_setting( $cat_key );
					echo '<a target="_blank" href="' . get_term_link( $cat->slug, $cat->taxonomy ) . '" title="'. $cat->name . "\n" . $cat->description .'">';
					echo $cat->name;
					echo '</a>';
					if ( $pam < ($total_cat -1) ) echo ',';
					$pam++;
				endforeach;

			break;

			case 'posted':

				echo '<span><strong>' . date_i18n( get_option('date_format'), strtotime( $post->post_date ) ) . '</strong><span><br>';
				echo ( empty( $post->post_author ) ? esc_html__( 'by a guest', 'sky-game' ) : sprintf( esc_html__( 'by %s', 'sky-game' ), '<a href="' . get_edit_user_link( $post->post_author ) . '">' . get_the_author() . '</a>' ) ) . '</span>';
				
			break;

			case 'view':
				echo '<span class="dashicons dashicons-visibility"></span> ' . sky_get_post_views( $post_id );
				break;
		}

	}

endif;

/** ====== END sky_show_table_columns ====== **/


add_action( 'init', 'register_sky_post_type' );
add_filter( 'manage_edit-sky-game_columns', 'sky_add_table_columns' );
add_action( 'manage_sky-game_posts_custom_column', 'sky_show_table_columns', 10, 2 );

/* -------------------------------------------------------
 * Create functions sky_init_testimonial
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_init_testimonial' ) ) :
	
	function sky_init_testimonial() {

		// Text for SKY testimonial.
        $testimonial_labels = array(
			'name'               => esc_html__('Testimonial', 'sky-game') ,
			'singular_name'      => esc_html__('Testimonial', 'sky-game') ,
			'menu_name'          => esc_html__('Testimonial', 'sky-game') ,
			'add_new'            => esc_html__('Add New', 'sky-game') ,
			'add_new_item'       => esc_html__('Add New testimonial Item', 'sky-game') ,
			'edit_item'          => esc_html__('Edit testimonial Item', 'sky-game') ,
			'new_item'           => esc_html__('Add New testimonial Item', 'sky-game') ,
			'view_item'          => esc_html__('View testimonial', 'sky-game') ,
			'search_items'       => esc_html__('Search testimonial', 'sky-game') ,
			'not_found'          => esc_html__('No testimonial items found', 'sky-game') ,
			'not_found_in_trash' => esc_html__('No testimonial items found in trash', 'sky-game') ,
			'parent_item_colon'  => ''
        );

        if ( floatval( get_bloginfo( 'version' ) ) >= 3.8 ) {
            $admin_icon = 'dashicons-testimonial';
        }

        // Options
        $testimonial_args = array(
            'labels' => $testimonial_labels,
            'public' => false,
            // 'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'menu_position' => 5,
            'menu_icon' => $admin_icon,
            'capability_type' => 'post',
            'hierarchical' => false,
            'supports' => array(
                'title',
                'editor',
               // 'excerpt',
               // 'thumbnail',
                // 'comments',
                // 'custom-fields',
                'revisions'
            ) ,
            'has_archive' => true,
            'rewrite' => array(
                'slug' => sky_get_option_setting( 'sky_general', 'testimonial_slug', 'testimonials' ),
                'with_front' => false
            )
        );

        register_post_type('testimonial', $testimonial_args);

        // Register a taxonomy for Project Categories.
        $category_labels = array(
			'name'                       => esc_html__('Testimonial Categories', 'sky-game') ,
			'singular_name'              => esc_html__('Testimonial Category', 'sky-game') ,
			'menu_name'                  => esc_html__('Testimonial Categories', 'sky-game') ,
			'all_items'                  => esc_html__('All Testimonial Categories', 'sky-game') ,
			'edit_item'                  => esc_html__('Edit Testimonial Category', 'sky-game') ,
			'view_item'                  => esc_html__('View Testimonial Category', 'sky-game') ,
			'update_item'                => esc_html__('Update Testimonial Category', 'sky-game') ,
			'add_new_item'               => esc_html__('Add New Testimonial Category', 'sky-game') ,
			'new_item_name'              => esc_html__('New Testimonial Category Name', 'sky-game') ,
			'parent_item'                => esc_html__('Parent Testimonial Category', 'sky-game') ,
			'parent_item_colon'          => esc_html__('Parent Testimonial Category:', 'sky-game') ,
			'search_items'               => esc_html__('Search Testimonial Categories', 'sky-game') ,
			'popular_items'              => esc_html__('Popular Testimonial Categories', 'sky-game') ,
			'separate_items_with_commas' => esc_html__('Separate Testimonial Categories with commas', 'sky-game') ,
			'add_or_remove_items'        => esc_html__('Add or remove Testimonial Categories', 'sky-game') ,
			'choose_from_most_used'      => esc_html__('Choose from the most used Testimonial Categories', 'sky-game') ,
			'not_found'                  => esc_html__('No Testimonial Categories found', 'sky-game') ,
        );

        $category_args = array(
			'labels'            => $category_labels,
			'public'            => false,
			'show_ui'           => true,
			'show_in_nav_menus' => false,
			'show_tagcloud'     => true,
			'show_admin_column' => true,
			'hierarchical'      => true,
			'query_var'         => true,
			'rewrite'           => array(
				'slug'       => 'testimonial_category',
				'with_front' => false
            ) ,
        );

        register_taxonomy('testimonial_category', array(
            'testimonial'
        ) , $category_args);

	}

	add_action('init', 'sky_init_testimonial');

endif;

/** ====== END sky_init_testimonial ====== **/




// -- [ Custom Fields Term ]
	/* -------------------------------------------------------
	 * Create functions sky_add_custom_fields_os
	 * ------------------------------------------------------- */

	if ( ! function_exists( 'sky_add_custom_fields_os' ) ) :

		function sky_add_custom_fields_os( ) {
			$os_id = uniqid();
			?>
				<div class="form-field">
					<label for="os_meta[os_icon]"><?php esc_html_e( 'Icon OS', 'sky-game' ); ?></label>
					<div class="t_select_icon">
						<select name="os_meta[os_icon]">
							<option value="icon-windows">Window Phone</option>
							<option value="icon-android">Android</option>
							<option value="icon-apple">Apple</option>
							<option value="icon-blackberry">Blackbery</option>
							<option value="icon-java-bold">Java</option>
						</select>
					</div>
				</div>
				<div class="form-field">
					<label for="os_meta[color_icon]"><?php esc_html_e( 'Color Icon', 'sky-game' ); ?></label>
					<script type='text/javascript'>
						jQuery(document).ready(function($) {
							$('.color-picker-<?php echo esc_attr($os_id); ?>').wpColorPicker();
						});
					</script>
					<input type="color" name="os_meta[color_icon]" id="os_meta[color_icon]" value="<?php echo esc_html( $os_meta['color_icon'] ) ? esc_html( $os_meta['color_icon'] ) : ''; ?>" class="color-picker-<?php echo esc_attr($os_id); ?>" data-default-color="#000" />
				</div>
			<?php

		}
		add_action( sky_get_option_setting( 'sky_general', 'support_os_slug', sky_get_option_setting( 'sky_general', 'support_os_slug', 'sky-game-support-os' ) ) . '_add_form_fields', 'sky_add_custom_fields_os', 10, 2 );

	endif;

	/** ====== END sky_add_custom_fields_os ====== **/

	/* -------------------------------------------------------
	 * Create functions sky_edit_custom_fields_os
	 * ------------------------------------------------------- */
	
	if ( ! function_exists( 'sky_edit_custom_fields_os' ) ) :
	
		function sky_edit_custom_fields_os( $term ) {
			
			// put the term ID into a variable
				$os_id = $term->term_id;
			
			// retrieve the existing value(s) for this meta field. This returns an array
				$os_meta = get_option( sky_get_option_setting( 'sky_general', 'support_os_slug', 'sky-game-support-os' ) . "_{$os_id}" );
			?>
				<tr class="form-field">
					<th scope="row" valign="top">
						<label for="os_meta[os_icon]">
							<?php esc_html_e( 'Icon OS', 'sky-game' ); ?>
						</label>
					</th>
					<td class="select_icon">
						<select name="os_meta[os_icon]">
							<option value="icon-windows" <?php selected( $os_meta['os_icon'], 'icon-windows' ); ?>>Window Phone</option>
							<option value="icon-android" <?php selected( $os_meta['os_icon'], 'icon-android' ); ?>>Android</option>
							<option value="icon-apple" <?php selected( $os_meta['os_icon'], 'icon-apple' ); ?>>Apple</option>
							<option value="icon-blackberry" <?php selected( $os_meta['os_icon'], 'icon-blackberry' ); ?>>Blackbery</option>
							<option value="icon-java-bold" <?php selected( $os_meta['os_icon'], 'icon-java-bold' ); ?>>Java</option>
						</select>
					</td>
				</tr>

				<tr class="form-field">
					<th scope="row" valign="top">
						<label for="os_meta[color_icon]">
							<?php esc_html_e( 'Color Icon', 'sky-game' ); ?>
						</label>
					</th>
					<script type='text/javascript'>
						jQuery(document).ready(function($) {
							$('.color-picker-<?php echo esc_attr($os_id); ?>').wpColorPicker();
						});
					</script>
					<td>
						<input type="color" name="os_meta[color_icon]" id="os_meta[color_icon]" value="<?php echo esc_html( $os_meta['color_icon'] ) ? esc_html( $os_meta['color_icon'] ) : ''; ?>" class="color-picker-<?php echo esc_attr($os_id); ?>" data-default-color="#000" />
						<!-- <input type="color" name="os_meta[color_icon]" id="os_meta[color_icon]" value="<?php echo esc_html( $os_meta['color_icon'] ) ? esc_html( $os_meta['color_icon'] ) : ''; ?>" /> -->
					</td>
				</tr>
			<?php
		}

		add_action( sky_get_option_setting( 'sky_general', 'support_os_slug', 'sky-game-support-os' ) . '_edit_form_fields', 'sky_edit_custom_fields_os', 10, 2 );
	
	endif;
	
	/** ====== END sky_edit_custom_fields_os ====== **/

	/* -------------------------------------------------------
	 * Create functions sky_save_custom_field_os
	 * ------------------------------------------------------- */
	
	if ( ! function_exists( 'sky_save_custom_field_os' ) ) :
	
		function sky_save_custom_field_os( $term_id ) {
			
			if ( isset( $_POST['os_meta'] ) ) {
				$os_id   = $term_id;
				$os_meta = sky_get_option( sky_get_option_setting( 'sky_general', 'support_os_slug', 'sky-game-support-os' ) . "_{$os_id}" );
				$os_key  = array_keys( $_POST['os_meta'] );
				foreach ( $os_key as $key ) {
					if ( isset ( $_POST['os_meta'][$key] ) ) {
						$os_meta[$key] = $_POST['os_meta'][$key];
					}
				}
				// Save the option array.
				update_option( sky_get_option_setting( 'sky_general', 'support_os_slug', 'sky-game-support-os' ) . "_{$os_id}", $os_meta );
			}
	
		}

		add_action( 'edited_'. sky_get_option_setting( 'sky_general', 'support_os_slug', 'sky-game-support-os' ), 'sky_save_custom_field_os', 10, 2 );  
		add_action( 'create_'. sky_get_option_setting( 'sky_general', 'support_os_slug', 'sky-game-support-os' ), 'sky_save_custom_field_os', 10, 2 );
	
	endif;
	
	/** ====== END sky_save_custom_field_os ====== **/

