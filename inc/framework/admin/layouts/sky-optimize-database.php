<?php 
// ===== <<< [ Checking ABSPATH root ] >>> ===== //
	defined('ABSPATH') || ( header( "HTTP/1.1 403 Forbidden" ) & die( "403.14 - Directory listing denied." ) );

if( !class_exists( 'Sky_Optimize_Database_Setting' ) ) :

	class Sky_Optimize_Database_Setting {

		public function __construct() {

			/*
			 * Load action, filter
			 */
				add_action('sky_setting_sky_optimize_database_settings', array( &$this, 'setting_page' ));
				add_filter('sky_settings_tabs_array', array( &$this, 'add_seting_optimize_database_tab' ));
				add_action( 'wp_ajax_sky_optimize', array( &$this, 'sky_optimize_ajax' ) );

		}

		public function add_seting_optimize_database_tab( $tabs ) {

			$tabs['sky_optimize_database_settings'] = esc_html__(  'Database Optimization', 'sky-game' );
			return $tabs;

		}

		public function sky_optimize_ajax() {

			// ===== <<< [ checking ajax ] >>> ===== //
				check_ajax_referer( 'sky-script-admin', 'security' );

			// =====
				$sky_optimize = new Sky_Database();

			// =====
				if ( $_POST['process'] == 'clean' ) : 

					foreach ( $_POST['database'] as $table ) :
						
						$sky_optimize->sky_database_clean( $table );

					endforeach;

					echo 'ok';

				elseif ( $_POST['process'] == 'optimize' ) :

					$sky_optimize->sky_cleaner_optimize();

					echo 'ok';

				endif;

			wp_die();

		}

		public function setting_page() {
			global $wpdb;
			// =====
		    	$sky_optimize = new Sky_Database();

		    // ===== set default
				$sky_count_unused         = $sky_optimize->sky_cleaner_count();
				$total_tables_to_optimize = 0;
				$total_lost               = 0;
				$prefix                   = str_replace( '_', '\_', $wpdb->prefix );
				$sky_sql                  = "SELECT table_name, data_free FROM information_schema.tables WHERE table_schema = '" . DB_NAME . "' and data_free > 0";

	    	?>
	    		<table class="widefat sky-optimize">
					<thead>
						<tr>
							<th colspan="3" data-export-label="<?php esc_html_e( 'Clear Database', 'sky-game' ); ?>">
								<label>
									<?php esc_html_e( 'Clear Database', 'sky-game' ); ?>
								</label>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr class="sky-menu">
							<td>
								<?php esc_html_e( 'Part to be cleaned', 'sky-game' ); ?>
							</td>
							<td class="help">
								<?php esc_html_e( 'Quantity', 'sky-game' ); ?>
							</td>
							<td>
								<?php esc_html_e( 'Select', 'sky-game' ); ?>
							</td>
						</tr>

						<?php 
							if( $sky_count_unused["total"] == 0 ) : ?>

								<tr class="">
									<td style="color: #999">
										<?php esc_html_e( 'Everything is good!' , 'sky-game' ); ?>
									</td>
									<td></td>
									<td></td>
								</tr>

							<?php else : ?>
						
							<?php if( $sky_count_unused["revision"] > 0 ) : ?>

									<tr>
										<td>
											<?php esc_html_e( 'Revision',   'sky-game' ); ?>
										</td>
										<td>
											<?php echo $sky_count_unused["revision"]; ?>
										</td>
										<td>
											<div class="sky-select-clean">
												<input type="checkbox" value="revision" />
											</div>
										</td>
									</tr>

							<?php endif; ?>
								
							<?php if( $sky_count_unused["draft"] > 0 ) : ?>
								
								<tr>
									<td>
										<?php esc_html_e( 'Draft' , 'sky-game' ); ?>
									</td>
									<td>
										<?php echo $sky_count_unused["draft"]; ?>
									</td>
									<td>
										<div class="sky-select-clean">
											<input type="checkbox" value="draft" />
										</div>
									</td>
								</tr>
							
							<?php endif; ?>
						
							<?php if( $sky_count_unused["autodraft"] > 0 ) : ?>

								<tr>
									<td>
										<?php esc_html_e( 'Auto Draft' , 'sky-game' ); ?>
									</td>
									<td>
										<?php echo $sky_count_unused["autodraft"]; ?>
									</td>
									<td>
										<div class="sky-select-clean">
											<input type="checkbox" value="autodraft" />
										</div>
									</td>
								</tr>

							<?php endif; ?>				
							
							<?php if( $sky_count_unused["moderated"] > 0 ) : ?>

								<tr>
									<td>
										<?php esc_html_e( 'Moderated Comments' , 'sky-game' ); ?>
									</td>
									<td>
										<?php echo $sky_count_unused["moderated"]; ?>
									</td>
									<td>
										<div class="sky-select-clean">
											<input type="checkbox" value="moderated" />
										</div>
									</td>
								</tr>

							<?php endif; ?>
						
							<?php if( $sky_count_unused["spam"] > 0 ) : ?>

								<tr>
									<td>
										<?php esc_html_e( 'Spam Comments' , 'sky-game' ); ?>
									</td>
									<td>
										<?php echo $sky_count_unused["spam"]; ?>
									</td>
									<td>
										<div class="sky-select-clean">
											<input type="checkbox" value="spam" />
										</div>
									</td>
								</tr>

							<?php endif; ?>
							
							<?php if( $sky_count_unused["trash"] > 0 ) : ?>

								<tr>
									<td>
										<?php esc_html_e( 'Trash Comments' , 'sky-game' ); ?>
									</td>
									<td>
										<?php echo $sky_count_unused["trash"]; ?>
									</td>
									<td>
										<div class="sky-select-clean">
											<input type="checkbox" value="trash" />
										</div>
									</td>
								</tr>

							<?php endif; ?>

							<?php if( $sky_count_unused["postmeta"] > 0 ) : ?>

								<tr>
									<td><?php esc_html_e( 'Orphan Postmeta' , 'sky-game' ); ?></td>
									<td><?php echo $sky_count_unused["postmeta"]; ?></td>
									<td>
										<div class="sky-select-clean">
											<input type="checkbox" value="postmeta" />
										</div>
									</td>
								</tr>

							<?php endif; ?>

							<?php if( $sky_count_unused["commentmeta"] > 0 ) : ?>

								<tr>
									<td><?php esc_html_e( 'Orphan Commentmeta' , 'sky-game' ); ?></td>
									<td><?php echo $sky_count_unused["commentmeta"]; ?></td>
									<td>
										<div class="sky-select-clean">
											<input type="checkbox" value="commentmeta" />
										</div>
									</td>
								</tr>

							<?php endif; ?>

							<?php if( $sky_count_unused["relationships"] > 0 ) : ?>
								
								<tr>
									<td><?php esc_html_e( 'Orphan Relationships' , 'sky-game' ); ?></td>
									<td><?php echo $sky_count_unused["relationships"]; ?></td>
									<td>
										<div class="sky-select-clean">
											<input type="checkbox" value="relationships" />
										</div>
									</td>
								</tr>

							<?php endif; ?>
							
							<?php if( $sky_count_unused["feed"] > 0 ) : ?>

								<tr>
									<td><?php esc_html_e( 'Dashboard Manager' , 'sky-game' ); ?></td>
									<td><?php echo $sky_count_unused["feed"]; ?></td>
									<td>
										<div class="sky-select-clean">
											<input type="checkbox" value="feed" />
										</div>
									</td>
								</tr>

							<?php endif; ?>
						
						<?php endif; ?>
						
					</tbody>

					<tfoot>
						<tr>
							<th>
								<?php esc_html_e( 'Total', 'sky-game' ); ?>
							</th>
							<th>
								<?php echo $sky_count_unused["total"]; ?>
							</th>
							<th>
								<button id="sky-clean-database" class="button">
									<?php esc_html_e( 'Clear', 'sky-game' ); ?>
									<i class="fa"></i>
								</button>
							</th>
						</tr>
					</tfoot>

				</table>


				<table class="widefat sky-optimize">
					<thead>
						<tr>
							<th colspan="3" data-export-label="<?php esc_html_e( 'Database Optimization', 'sky-game' ); ?>">
								<label>
									<?php esc_html_e( 'Database Optimization', 'sky-game' ); ?>
								</label>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr class='sky-menu'>
							<td>
								<?php esc_html_e( 'Optimize parts', 'sky-game' ); ?>
							</td>
							<td>
								<?php esc_html_e( 'Space used', 'sky-game' ); ?>
							</td>
						</tr>
						<?php 
							if( !is_main_site() ) :

								$sky_sql = $sky_sql . " and table_name LIKE '{$prefix}%'";

							endif;

							$result = $wpdb->get_results($sky_sql);

							foreach( $result as $row ) :

								$total_tables_to_optimize += 1;
								$total_lost               += $row->data_free;

								echo "
									<tr class=''>
									 	<td>
									 		". $row->table_name ."
									 	</td>
									 	<td>
									 		". $row->data_free . ' ‰' ."
									 	</td>
									</tr>\n";

							endforeach;
							if($total_lost == 0) :

								echo "
									<tr class=''>
									 	<td class='column-name' style='color: #999'>
									 		". esc_html__( 'All Database are optimized!', 'sky-game' ) ."
									 	</td>
									 	<td class='column-name'></td>
									</tr>
								";

							endif;
						?>
					</tbody>
					<tfoot>
						<tr>
							<th>
								<?php esc_html_e( 'Total', 'sky-game' ); ?>
							</th>
							<th>
								<?php echo $total_lost .'</b> o'; ?>
							</th>
						</tr>
						<tr>
							<th></th>
							<th>
								<button id="sky-optimize-database" class="button">
									<?php esc_html_e( 'Optimize', 'sky-game' ); ?>
									<i class="fa"></i>
								</button>
							</th>
						</tr>
					</tfoot>
				</table>
	    		
	    	<?php

	    }

	}

	new Sky_Optimize_Database_Setting();

endif;