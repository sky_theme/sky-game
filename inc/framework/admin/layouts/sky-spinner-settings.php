<?php 
// ===== <<< [ Checking ABSPATH root ] >>> ===== //
	defined('ABSPATH') || ( header( "HTTP/1.1 403 Forbidden" ) & die( "403.14 - Directory listing denied." ) );

if( !class_exists( 'Sky_Spinner_Setting' ) ) :

	class Sky_Spinner_Setting {

		public function __construct() {

			/*
			 * Load action, filter
			 */
				add_action('admin_init', array(&$this,'admin_init'));
				add_action('sky_setting_sky_spinner_settings', array( &$this, 'setting_page' ));
				add_filter('sky_settings_tabs_array', array( &$this, 'add_seting_spinner_tab' ));

				add_action( 'wp_ajax_sky_spinner', array( &$this, 'sky_spinner_ajax' ) );

				add_action( 'add_meta_boxes', array( &$this, 'sky_spinner_create_meta_box' ) );

				add_filter( 'mce_css', array( &$this, 'sky_spinner_mce_css' ) );

		}

		public function admin_init(){

			register_setting( 'sky_spinner_settings' , 'sky_spinner_settings' );

		}

		public function add_seting_spinner_tab( $tabs ) {

			$tabs['sky_spinner_settings'] = esc_html__(  'Spinner', 'sky-game' );
			return $tabs;

		}

		public static function get_setting($id = null, $default = null){

			$sky_spinner_settings = get_option('sky_spinner_settings');

			if(isset($sky_spinner_settings[$id]))
				return $sky_spinner_settings[$id];

			return $default;

		}

		public function sky_spinner_create_meta_box() {
			
			$screens = array( 'post', 'sky-game' );

			foreach ( $screens as $screen ) :
				
				add_meta_box( 'sky_spinner-meta-boxes', esc_html__(  'Sky Spinner', 'sky-game' ), array( &$this, 'sky_spinner_metabox' ), $screen , 'normal', 'high' );

			endforeach;
			
		}

		public function sky_spinner_mce_css( $mce_css ) {
			if ( ! empty( $mce_css ) )
				$mce_css .= ',';

			$mce_css .= SKY_THEME_ASSETS_URI . '/css/sky-editor.css';

			return $mce_css;
		}

		public function sky_spinner_ajax() {

			// ===== <<< [ checking ajax ] >>> ===== //
				check_ajax_referer( 'sky-script-admin', 'security' );

			// ===== <<< [ Get var ] >>> ===== //
				$id = $_POST['id'];


			if ( $_POST['process'] == 'delete' ) :

				$delete = get_option('sky_spinner' ,array() );
				unset($delete[$id]);
				update_option('sky_spinner' , $delete );
			
			elseif ( $_POST['process'] == 'edit' ) :

				$value     = $_POST['value'];
				$edit      = get_option('sky_spinner' ,array() );
				$edit[$id] = $value;
				update_option( 'sky_spinner', $edit );

			elseif ( $_POST['process'] == 'add' ) :

				$value    = $_POST['value'];
				$add      = get_option('sky_spinner' ,array() );
				$add[$id] = $value;
				update_option( 'sky_spinner', $add );			

			elseif ( $_POST['process'] == 'spin' ) :

				require_once(SKY_THEME_INCLUDES_FUNCTION . '/sky-spintax.php');
				
				$spin             = new Sky_Auto_Spin( $id, $_POST['title'], $_POST['post']);
				$return           = $spin->spin_wrap();
				$return['status'] = 'success';

				print_r(json_encode($return));			

			elseif ( $_POST['process'] == 're_generate' ) :

				require_once(SKY_THEME_INCLUDES_FUNCTION . '/sky-spintax.php');
	
				//generate new sky_spintax
					$content        = stripslashes( $_POST['title'].'**9999**'.$_POST['post']);
					$sky_spintax        = new Sky_Spinner_Spintax;
					$sky_spintaxed      = $sky_spintax->spin( $content );
					$sky_spintaxed_arr  = explode( '**9999**', $sky_spintaxed );
					$sky_spintaxed_ttl  = $sky_spintaxed_arr[0];
					$sky_spintaxed_cnt  = $sky_spintaxed_arr[1];
					$sky_spintaxed2     = $sky_spintax->editor_form;
					$sky_spintaxed2_arr = explode( '**9999**', $sky_spintaxed2 );
					$sky_spintaxed2_cnt = $sky_spintaxed2_arr[1];
				
				
				//update post meta
					update_post_meta($id, 'sky_spintaxed_cnt2', $sky_spintaxed2_cnt);
					update_post_meta($id, 'sky_spintaxed_ttl', $sky_spintaxed_ttl);
					update_post_meta($id, 'sky_spintaxed_cnt', $sky_spintaxed_cnt);
				
				$return['sky_spintaxed_ttl']  = $sky_spintaxed_arr[0];
				$return['sky_spintaxed_cnt2'] = $sky_spintaxed2_arr[1];
				$return['status']         = 'success';
				
				print_r(json_encode($return));		

			endif;

			wp_die();

		}

		public function sky_spinner_metabox( ) {
			$post_id = get_the_ID();
			?>
			<div class="post_rewrite">
				<a id="sky-spinner-post" title="<?php esc_html_e( 'Spin The Post', 'sky-game' ); ?>" class="button button-primary button-large" href="#">
					<?php esc_html_e( 'Spin The Post', 'sky-game' ); ?>
				</a>
				<img id="sky-ajax-loading1" class="ajax-loading" src="<?php echo site_url(); ?>/wp-admin/images/wpspin_light.gif" />
			</div>

			<?php 
				if( trim( get_post_meta($post_id, 'sky_spinner_cnt' , 1) ) !='' ) :

					$display = '';

				else :

					$display = 'display:none';

				endif;
			?>

			<div class="categorydiv sky-spinner-meta" id="taxonomy-category" style="<?php echo $display ?>">
	
				<ul class="category-tabs" id="category-tabs">
					<li id="sky-spin-head" class="tab_head tabs"><?php esc_html_e( 'Spun Article', 'sky-game' ); ?></li>
					<li class="tab_head hide-if-no-js"><?php esc_html_e( 'Spun Keyword', 'sky-game' ); ?></li>
					<li class="tab_head hide-if-no-js"><?php esc_html_e( 'Original Article', 'sky-game' ); ?></li>
				</ul>

				<div class="tabs-panel visible-overflow" style="display: block;">
					
					<p><i><?php esc_html_e( 'Sample of Spun article', 'sky-game' ); ?></i></p>
					
					<div class="sky-spin-form">
					
						<div id="field-sky-spin-title-container" class="field">
							<label for="field-sky-spin-title">
							    <?php esc_html_e( 'Spun title', 'sky-game' ); ?>
							</label>
							<input value="<?php echo get_post_meta($post_id, 'sky_spintaxed_ttl' , 1) ?>" name="sky-spin-title" id="field-sky-spin-title" type="text">
						</div>
						
						<div id="field-sky-spin-post-container" class="field" >
							<label for="field-sky-spin-post">
								<?php esc_html_e( 'Spun post', 'sky-game' ); ?>
							</label>
							
							<?php 
								
							$args = array(
								'textarea_rows' => 15,
								'teeny'         => true,
								'quicktags'     => false,
								'media_buttons' => false,
								'tinymce'       => array(
									'extended_valid_elements' => "span[id|class|title|style|synonyms]",
									'setup'     =>  "function (ed) {
												    	ed.onLoadContent.add(function(ed, o) {
												          	// Output the element name
															jQuery(\"#spinner-editor_ifr\").parent().append(
																'<ul id=\"spinner_dev\"><li>test</li></ul>'
															);
															var mouseX;
															var mouseY;
															jQuery(jQuery(\"#spinner-editor_ifr\").contents()[0], window).bind(\"mousemove\", function(e) {
																mouseX = e.pageX; 
																mouseY = e.pageY;
															});

															var currentSynonym ;
															  
															jQuery(\"#spinner-editor_ifr\").contents().find('.synonym').mouseover(function(){

																currentSynonym=this;

																//empty list
																jQuery('#spinner_dev li').remove();

																var synonyms = jQuery(this).attr('synonyms');
																synonyms_arr =synonyms.split('|') ;
																jQuery.each(synonyms_arr, function (index, value) {
																    if (value != '') {
																		jQuery('#spinner_dev').append('<li>' + value + '</li>');            
																    }
																});

	  															jQuery('#spinner_dev').css({
	  																'top' : mouseY - 13 +45 - jQuery(\"#spinner-editor_ifr\").contents().find('body').scrollTop(),
	  																'left' : mouseX -5
	  															}).fadeIn('slow');
	  															
	  															jQuery('#spinner_dev').focus();
	  
	  															jQuery('#spinner_dev li').click(function(){
	    
																    jQuery(currentSynonym).html(jQuery(this).html());
																    jQuery('#spinner_dev').hide();
																
																});
 
															});

															jQuery('#spinner_dev').mouseleave(function(){
															  	jQuery('#spinner_dev').hide();
															});


															jQuery('#spinner_dev li').click(function(event){
															    console.log(jQuery(this).html());
															     event.stopImmediatePropagation();
															    return false;
															});
								
														           
														});
																						
													},"  
								)
							);
				
							wp_editor( get_post_meta($post_id, 'sky_spintaxed_cnt2', 1) , 'spinner-editor', $args );
							?>
						</div>
			
						<input type="hidden" value="yes" name="wp_auto_spinner_manual_flag" />

						<div id="form-submit" class="field" style="padding:20px 0">
						    <a id="sky-spinner-stoeditor" title="Send to editor " class="button" href="#">Send to editor</a>
						    <a  id="sky-auto-spinner-regenerate-re" title="regenerate"class="button"> Generate new post</a>
						    <img id="sky-ajax-loading3" class="ajax-loading" src=" <?php echo site_url(); ?>/wp-admin/images/wpspin_light.gif">
						</div>
		
					</div>
		
					<div class="clear"></div>
		
				</div>
	
				<div class="tabs-panel visible-overflow" style="display: none;">
					<p>
						<i>
							<?php esc_html_e( 'The article contains spun keywords taken from the original one', 'sky-game' ); ?>
						</i>
					</p>
					
					<div class="sky-spin-form">
					
						<div id="field-sky-spin-title-container" class="field">
							<label for="field-sky-spin-title">
							    <?php esc_html_e( 'Spun title', 'sky-game' ); ?>
							</label>
							<input value="<?php echo  get_post_meta($post_id, 'sky_spinner_ttl' , 1) ?>" name="sky-spin-titlesyn" id="field-sky-spin-titlesyn"  type="text">
						</div>
						
						<div id="field-sky-spin-post-container" class="field" >
							<label for="field-sky-spin-post">
								<?php esc_html_e( 'Spun post', 'sky-game' ); ?>
							</label>
							<textarea  rows="5" cols="20" name="sky-spin-postsyn" id="field-sky-spin-postsyn"><?php echo get_post_meta($post_id, 'sky_spinner_cnt' , 1)  ?></textarea>
						</div>
						
						<div id="form-submit" class="field" style="padding:20px 0"> 
						    <a id="sky-auto-spinner-regenerate" title="<?php esc_html_e( 'Spin The Post', 'sky-game' ); ?>" class="button" href="<?php echo site_url('/?wp_auto_spinner=ajax&action=rewrite'); ?>"> Generate new post from above synonyms </a>
						    <img id="sky-ajax-loading2" class="ajax-loading" src="<?php echo site_url(); ?>/wp-admin/images/wpspin_light.gif" />
						</div>
					
					</div>
					<div class="clear"></div>
					
					
				</div>
	
				<div id="category-pop" class="tabs-panel  visible-overflow" style="display: none;">
					<p>
						<i>
							<?php esc_html_e( 'Original content of the article that will then be spun', 'sky-game' ); ?>
						</i>
					</p>
					
					<div class="sky-spin-form">
					
						<div id="field-sky-spin-title-container" class="field">
							<label for="field-sky-spin-title">
							    <?php esc_html_e( 'Original Title :', 'sky-game' ); ?>
							</label>
							<input value="<?php echo  get_post_meta($post_id, 'original_ttl' , 1)  ?>" name="sky-spin-titleori" id="field-sky-spin-titleori"  type="text">
						</div>
						
						<div id="field-sky-spin-post-container" class="field" >
							<label for="field-sky-spin-post">
								<?php esc_html_e( 'Original post :', 'sky-game' ); ?>
							</label>
							<textarea  rows="5" cols="20" name="sky-spin-postori" id="field-sky-spin-postori"><?php echo  get_post_meta($post_id, 'original_cnt' , 1)  ?></textarea>
						</div>
						
						<div id="form-submit" class="field" style="padding:20px 0">
						    <a id="sky-spin-restore" title="<?php esc_html_e( 'Spin The Post', 'sky-game' ); ?>" class="button" href="#"> Restore original post </a>
						</div>
					
					</div>
					<div class="clear"></div>
					
				</div>

			</div>

			<?php

		}

		public function setting_page() {
			// =====
		    	settings_fields('sky_spinner_settings');

		    // ===== set default

	    	?>
	    		<table class="widefat sky-spinner">
					<thead>
						<tr>
							<th colspan="3" data-export-label="<?php esc_html_e( 'Spinner Settings', 'sky-game' ); ?>">
								<label>
									<?php esc_html_e( 'Spinner Settings', 'sky-game' ); ?>
								</label>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<?php esc_html_e( 'Targeted keywords', 'sky-game' ); ?>
                                <select name="sometext" size="20" id="word_select">
                               
                                </select>
                                <button id="edit_synonyms" title="<?php esc_html_e( 'Edit', 'sky-game' ); ?>">
                                    <i class="fa fa-pencil-square-o"></i> <?php esc_html_e( 'Edit', 'sky-game' ); ?>
                                </button>
                                
                                <button  id="save_synonyms" disabled="disabled" title="<?php esc_html_e( 'Save', 'sky-game' ); ?>">
                                    <i class="fa fa-floppy-o"></i> <?php esc_html_e( 'Save', 'sky-game' ); ?>
                                </button>
                                
                                <button  id="cancel_synonyms" disabled="disabled" title="<?php esc_html_e( 'Cancel', 'sky-game' ); ?>">
                                    <i class="fa fa-ban"></i> <?php esc_html_e( 'Cancel', 'sky-game' ); ?>
                                </button>
                                
                                <button  id="delete_synonyms" title="<?php esc_html_e( 'Trash', 'sky-game' ); ?>">
                                    <i class="fa fa-trash-o"></i> <?php esc_html_e( 'Trash', 'sky-game' ); ?>
                                </button>
							</td>
							<td>
								<p>
									<?php esc_html_e( 'Current Word', 'sky-game' ); ?>
								</p>
                                <input disabled="disabled" type="text" id="synonym_word" />
                                <p>
                                	<?php esc_html_e( 'Synonyms Word', 'sky-game' ); ?>
                                </p>
                                <textarea disabled="disabled" rows="15" cols="30" name="name" id="word_synonyms"></textarea>
							</td>
						</tr>
					</tbody>
				</table>

				<table class="widefat sky-spinner">
					<thead>
						<tr>
							<th colspan="3" data-export-label="<?php esc_html_e( 'Add New Keyword To Spin', 'sky-game' ); ?>">
								<label>
									<?php esc_html_e( 'Add New Keyword To Spin', 'sky-game' ); ?>
								</label>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<p><?php esc_html_e( 'one word per line', 'sky-game' ); ?></p>
								<textarea style="width:100%" cols="15" rows="3" class="mceEditor" id="content" name="content"></textarea>
								<button id="add_synonyms" class="button"><?php esc_html_e( 'Add synonyms keyword', 'sky-game' ); ?></button>
							</td>
						</tr>
					</tbody>
				</table>

	    	<?php

	    }

	}

	new Sky_Spinner_Setting();

endif;