<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.

/* -------------------------------------------------------
 * Create functions register_sky_metabox
 * ------------------------------------------------------- */

if ( ! function_exists( 'register_sky_metabox' ) ) :

  function register_sky_metabox( $meta_boxes ) {
    
    $prefix = 'sky_';

    // ==== <<< Meta Sky-Game

      $meta_boxes[] = array(
        'id'       => 'box_game_settings',
        'title'    => esc_html__( 'Page Settings', 'sky-game' ),
        'pages'    => array( 'sky-game' ),
        'fields'   => array(
          array(
            'name'        => esc_html__( 'Body Custom CSS Class', 'sky-game' ),
            'id'          => $prefix . 'class',
            'type'        => 'checkbox',
          ),
          array(
            'name'        => esc_html__( 'Hide Game Title', 'sky-game' ),
            'id'          => $prefix . 'hide_title',
            'type'        => 'checkbox',
            'default'     => true
          ),
          array(
            'name'        => esc_html__( 'Hide Game Related', 'sky-game' ),
            'id'          => $prefix . 'hide_related',
            'type'        => 'checkbox',
          ),
          array(
            'name'        => esc_html__( 'Hide Facebook Comment', 'sky-game' ),
            'id'          => $prefix . 'hide_comment_facebook',
            'type'        => 'checkbox',
          ),
        )
      );

      $meta_boxes[] = array(
        'id'       => 'box_game_info',
        'title'    => esc_html__( 'Game Information', 'sky-game' ),
        'pages'    => array( 'sky-game' ),
        'fields'   => array(
          array(  
            'name'        => esc_html__( 'Thumbnail Url', 'sky-game' ),
            'id'          => $prefix . 'thumbnail_url',
            'type'        => 'text',
            'placeholder' => esc_html__( 'http://', 'sky-game' )
          ),
          array(  
            'name'        => esc_html__( 'Video Url', 'sky-game' ),
            'id'          => $prefix . 'video_url',
            'type'        => 'text',
            'placeholder' => esc_html__( '', 'sky-game' )
          ),
          array(
            'name'        => esc_html__( 'Short Descrtiption', 'sky-game' ),
            'id'          => $prefix . 'description',
            'type'        => 'textarea',
            'placeholder' => esc_html__( 'Enter a brief description of the article', 'sky-game' ),
          ),
          array(
            'name'        => esc_html__( 'Add link download', 'sky-game' ),
            'desc'        => esc_html__( 'Ex: http://linkdownload|Name link|Size|Android|icon(android|ios|java|wp)', 'sky-game' ),
            'id'          => $prefix . 'link_download',
            'type'        => 'text',
            'clone'       => true,
            'sort_clone'  => true,
            'placeholder' => esc_html__( 'Enter your link', 'sky-game' )
          ),
          array(
            'name'        => esc_html__( 'What\'s new', 'sky-game' ),
            'id'          => $prefix . 'new',
            'type'        => 'text',
            'clone'       => true,
            'sort_clone'  => true,
            'placeholder' => esc_html__( 'Enter game information in the new version', 'sky-game' )
          ),
          array(  
            'name'        => esc_html__( 'Version', 'sky-game' ),
            'id'          => $prefix . 'version',
            'type'        => 'text',
            'placeholder' => esc_html__( 'Ex: 1.0.0', 'sky-game' )
          ),
          array(  
            'name'        => esc_html__( 'Size', 'sky-game' ),
            'id'          => $prefix . 'size',
            'type'        => 'text',
            'placeholder' => esc_html__( 'Ex: 50MB', 'sky-game' )
          ),
          array(  
            'name'        => esc_html__( 'Required Android', 'sky-game' ),
            'id'          => $prefix . 'requires_android',
            'type'        => 'text',
            'placeholder' => esc_html__( 'Ex: 2.2 and above', 'sky-game' )
          ),
          array(  
            'name'        => esc_html__( 'Required IOS', 'sky-game' ),
            'id'          => $prefix . 'requires_ios',
            'type'        => 'text',
            'placeholder' => esc_html__( 'Ex: 2.2 and above', 'sky-game' )
          ),
          array(  
            'name'        => esc_html__( 'Add keywords to display related articles', 'sky-game' ),
            'id'          => $prefix . 'key_related',
            'type'        => 'text',
            'desc'        => esc_html__( 'Ex: Title Display|Number Show Post|Slug Keyword', 'sky-game' )
          ),
          array(
            'type'    => 'heading',
            'content' => 'Heading Field',
          ),
          array(  
            'name'        => esc_html__( 'Enable notification above the downloaded link', 'sky-game' ),
            'id'          => $prefix . 'before_link_download',
            'type'        => 'checkbox',
            'default'     => false,
            'desc'        => esc_html__( 'If you tick/check this box, there will be a section that allows you to type a paragraph. This paragraph will be shown above the downloaded link.', 'sky-game' )
          ),
          array(
            'name'        => '&nbsp;',
            'id'          => $prefix . 'before_link_download_content',
            'type'        => 'textarea',
            'placeholder' => esc_html__( 'Enter the paragraph/text here. It can be HTML or shorcode', 'sky-game' ),
            'shortcode'   => true
          ),
        )
      );
  
    // ==== <<< Meta Testimonial
      $meta_boxes[] = array(
        'id'       => 'box_testimonial_settings',
        'title'    => esc_html__( 'Testimonial Options', 'sky-game' ),
        'pages'    => array( 'testimonial' ),
        'fields'   => array(
          array(
            'id'               => $prefix . 'image',
            'name'             => esc_html__( 'Your Image', 'sky-game' ),
            'type'             => 'image_advanced',
            'force_delete'     => true,
            'max_file_uploads' => 1,
          ),
          array(  
            'name'        => esc_html__( 'Your Name', 'sky-game' ),
            'id'          => $prefix . 'name',
            'type'        => 'text'
          ),
          array(  
            'name'        => esc_html__( 'Your Position', 'sky-game' ),
            'id'          => $prefix . 'position',
            'type'        => 'text'
          ),
        )
      );

    // ===== <<< Meta Page
      $meta_boxes[] = array(
        'id'       => 'box_page_settings',
        'title'    => esc_html__( 'Page Settings', 'sky-game' ),
        'pages'    => array( 'page' ),
        'fields'   => array(
          array(
            'name'        => esc_html__( 'Body Custom CSS Class', 'sky-game' ),
            'id'          => $prefix . 'class',
            'type'        => 'checkbox',
          ),
          array(
            'name'        => esc_html__( 'Hide Title', 'sky-game' ),
            'id'          => $prefix . 'hide_title',
            'type'        => 'checkbox',
          )
        )
      );

    // ===== <<< Meta Post
      $meta_boxes[] = array(
        'id'       => 'box_post_settings',
        'title'    => esc_html__( 'Post Settings', 'sky-game' ),
        'pages'    => array( 'post' ),
        'fields'   => array(
          array(
            'name'        => esc_html__( 'Hide Title', 'sky-game' ),
            'id'          => $prefix . 'hide_title',
            'type'        => 'checkbox',
          )
        )
      );

    return apply_filters( 'sky_add_meta_box', $meta_boxes );

  }

  add_filter( 'sky_meta_boxes', 'register_sky_metabox' );

endif;

/** ====== END register_sky_metabox ====== **/