<?php
/**
 * Helper functions for Sky Framework.
 * Function for getting view files. There's two kind of view files,
 * one is default view from framework, the other is view from specific theme.
 * File from specific theme will override that from framework.
 *
 * @package Sky Game
 */

/*
 * Load Framework Helper
 */
// require SKY_THEME_INCLUDES_FRAMEWORK . '/admin/custom_controls.php';
// require SKY_THEME_INCLUDES_FRAMEWORK . '/admin/class-helper.php';
require SKY_THEME_INCLUDES_FRAMEWORK . '/admin/sky-check-version.php';
if ( is_admin() ) {     
    $license_manager = new Sky_Check_Version(
        'sky-game',
        'Sky Game',
        'http://update.skygame.mobi/api/license-manager/v1',
        'theme',
        '',
        false
    );
}

if ( ! isset( $content_width ) ) {
	$content_width = 900;
}
/* -------------------------------------------------------
 * Create functions sky_remove_default_image_sizes
 * Remove default image size
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_remove_default_image_sizes' ) ) :

	function sky_remove_default_image_sizes( $sizes) {
	    // unset( $sizes['thumbnail']);
	    unset( $sizes['medium']);
	    unset( $sizes['large']);
	     
	    return $sizes;
	}
	add_filter('intermediate_image_sizes_advanced', 'sky_remove_default_image_sizes');
endif;

/** ====== END sky_get_layout ====== **/


/* -------------------------------------------------------
 * Create functions sky_get_layout
 * Shorthand function get predefined layout
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_get_layout' ) ) :

	function sky_get_layout( $slug, $name = '' ) {
		
		get_template_part( 'layouts/' . $slug, $name );

	}

endif;

/** ====== END sky_get_layout ====== **/

/* -------------------------------------------------------
 * Create functions sky_get_option
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_get_option' ) ) :

	function sky_get_option_setting( $name, $value = null, $default = null ) {
		$option_name = get_option( $name );
		if ( empty( $value ) ) return $option_name;
		if ( empty( $option_name ) ) return $default;
		if ( empty( $option_name[$value] ) ) return $default;
		if (!$option_name[$value]) return;
		return $option_name[$value];

	}

endif;

/** ====== END sky_get_option ====== **/

/* -------------------------------------------------------
 * Create functions sky_get_post_meta
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_get_post_meta' ) ) :

	function sky_get_post_meta( $post_ID, $name, $value = null ) {

		$option_name = get_post_meta( $post_ID, $name, true );
		if ( is_array($name) ) return $option_name[$value];
		return $option_name;

	}

endif;

/** ====== END sky_get_post_meta ====== **/


/* -------------------------------------------------------
 * Create functions sky_query_vars
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_query_vars' ) ) :
	
	function sky_query_vars( $query_vars ) {

		$query_vars[] = 'slug_game';
	    $query_vars[] = 'id_game';
	    $query_vars[] = 'paged';
	    return $query_vars;

	}
	add_filter( 'query_vars', 'sky_query_vars' );

endif;

/** ====== END sky_query_vars ====== **/

/* -------------------------------------------------------
 * Create functions sky_custom_rewrite_basic
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_custom_rewrite_basic' ) ) :
	
	function sky_custom_rewrite_basic() {

		add_rewrite_rule(
        	'^download/file-([0-9]+)/?',
        	'index.php?pagename=download&id_game=$matches[1]',
        	'top'
    	);

	}
	add_action('init', 'sky_custom_rewrite_basic', 10, 0);

endif;

/** ====== END sky_custom_rewrite_basic ====== **/

require SKY_THEME_INCLUDES_FRAMEWORK . '/admin/sky-setup.php';
require SKY_THEME_INCLUDES_FRAMEWORK . '/star/sky-framework.php';
// require SKY_THEME_INCLUDES_FRAMEWORK . '/star1/cs-framework.php';
require SKY_THEME_INCLUDES_FRAMEWORK . '/admin/meta-box.php';
require SKY_THEME_INCLUDES_FRAMEWORK . '/admin/sky-post-type.php';
require SKY_THEME_INCLUDES_FRAMEWORK . '/admin/sky-taxonomy.php';


require SKY_THEME_INCLUDES_FUNCTION . '/class-tgm-plugin-activation.php';
require SKY_THEME_INCLUDES_FUNCTION . '/sky-optimization.php';
require SKY_THEME_INCLUDES_FUNCTION . '/sky-enqueue-style.php';
require SKY_THEME_INCLUDES_FUNCTION . '/sky-enqueue-script.php';
require SKY_THEME_INCLUDES_FUNCTION . '/sky-utilities.php';
require SKY_THEME_INCLUDES_FUNCTION . '/sky-shortcode.php';
require SKY_THEME_INCLUDES_FUNCTION . '/sky-manager.php';
require SKY_THEME_INCLUDES_FUNCTION . '/sky-layout.php';
require SKY_THEME_INCLUDES_FUNCTION . '/sky-html.php';
require SKY_THEME_INCLUDES_FUNCTION . '/sky-style.php';
require SKY_THEME_INCLUDES_FUNCTION . '/sky-mailchimp.php';
require SKY_THEME_INCLUDES_FUNCTION . '/sky-control.php';
require SKY_THEME_INCLUDES_FUNCTION . '/sky-seo.php';

require SKY_THEME_INCLUDES_FRAMEWORK. '/admin/mega-menu/sky_mega_menu.php';
require SKY_THEME_INCLUDES_FRAMEWORK. '/admin/multi-sidebar/_init.php';

// ==== Auto load Settings

sky_autoload( SKY_THEME_INCLUDES_FRAMEWORK . '/admin/layouts' );