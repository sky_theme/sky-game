<?php
if(! class_exists('Sky_Spinner_Spintax')){
	class Sky_Spinner_Spintax {
		
		public $editor_form;
	
	   	public function spin( $string, $view = false ) {
	   	
	   		// ===== <<< [ Set default ] >>> ===== //
				$opt   = get_option( 'sky_spinner' , array() );
				$input = $this->bracketArray( $string );
				$z     = -1;
				$res   = '';
				$res2  = '';

	      	// =====
		      	for ( $i = 0; $i < count( $input ); $i++ ) :

		         	for ( $x = 0; $x < count( $input[$i] ); $x++ ) :

		            	if ( !$input[$i][$x] == "" || "/n" ) :

		               		$z++;

		               		if ( strstr( $input[$i][$x], "*|*" ) ) :

		                  		$out = explode("*|*", $input[$i][$x] );
		                  		$output[$z] = $out[rand(1, count($out)-2)];
		                  
			                  	//invert synonyms
			                  		$synonyms = str_replace('*|*', '|', $input[$i][$x] );
			                  
			                  	//if content spinningactive

			                  	$randSyn = $out[rand(1, count($out)-1)];
			                  
			                  	$output2[$z] = '<span synonyms="' . $synonyms . '" class="synonym">' . $randSyn . '</span>';

			               	else :
			                  
			                  	$output[$z] = $input[$i][$x];
			                  	$output2[$z] = $input[$i][$x];
			               	
			               	endif;
		            	
		            	endif;

		         	endfor;

		      	endfor;

	      	
	      	for( $i = 0; $i < count( $output ); $i++ ) :
	        
				$res  .=  $output[$i];
				$res2 .= $output2[$i];
	      	
	      	endfor;
	      
	      	$this->editor_form = $res2;
	      
	      	return $res;
	      
	   	}
	   
	   
	   	public function bracketArray( $str, $view = false ) {
	   	
	   		preg_match_all ( '/{(.*?)}/s', $str, $matches );
	   		$sets = $matches [0];
	   		foreach ( $sets as $set ) :
	   			
	   			$str = str_replace( $set, str_replace('|','*|*', $set ), $str );

	   		endforeach;
	   	
	      	@$string = explode( "{", $str );
 
	      	for( $i = 0; $i < count( $string ); $i++ ) :

	         	@$_string[$i] = explode("}", $string[$i]);
	      	
	      	endfor;
	      	
	      	if($view) :

	         	$this->printArray($_string);

	      	endif;

	      	return $_string;
	   	}
	   
	   	public function cleanArray($array) {

	      	for( $i = 0; $i < count( $array ); $i++ ) :

	         	if( $array[$i] != "" ) :

	            	$cleanArray[$i] = $array[$i];
	         	
	         	endif;
	      	
	      	endfor;

	      	return $cleanArray;
	   	}
	   
	   	public function printArray( $array ) {
	      	echo '<pre>';
	      	print_r($array);
	      	echo '</pre>';
	   	}
	}
}

class Sky_Auto_Spin{
	
	public $id;
	public $title;
	public $post;
	
	public $article; // sky_spinner article
	
	public function Sky_Auto_Spin( $id, $title, $post ){
		$this->id    = $id;
		$this->title = $title;
		$this->post  = $post;
	}
	
	public function spin_wrap(){
		
		$opt = get_option( 'sky_spinner', array() );
		
		return $this->spin();
		
		
	}
	
	public function spin() {
 
 		// ===== <<< [ Set Default ] >>> ===== //
			$opt     = get_option( 'sky_spinner', array( ) );
			$article = stripslashes( $this->title ) . '**9999**' . stripslashes( $this->post );
		 	$htmlurls = array();
		
		// ===== <<< [ Processing ] >>> ===== //
		
			// ===== <<< [ Processing Link Tag ] >>> ===== //
				preg_match_all( "/<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*?)<\/a>/s" ,$article,$matches,PREG_PATTERN_ORDER);
				$htmlurls = $matches[0];

			// ===== <<< [ Processing HTML Tag ] >>> ===== //
				preg_match_all( "/<[^<>]+>/is", $article, $matches, PREG_PATTERN_ORDER );
				$htmlfounds = $matches[0];
		
		 
			// ===== <<< [ Processing Shortcode Tag ] >>> ===== //
				$pattern = "\[.*?\]";
				preg_match_all( "/" . $pattern . "/s", $article, $matches2, PREG_PATTERN_ORDER );
				$shortcodes = $matches2[0];
			
			// ===== <<< [ Processing JS Tag ] >>> ===== //
				preg_match_all( "/<script.*?<\/script>/is", $article, $matches3, PREG_PATTERN_ORDER );
				$js = $matches3[0];
			
			// ===== <<< [ Processing nospin Item ] >>> ===== //
				preg_match_all( '{\[nospin\].*?\[/nospin\]}s', $article, $matches_ns );
				$nospin = $matches_ns[0];

			// ===== <<< [ Processing not spin item ] >>> ===== //
				
				$htmlfounds = array_merge( $js, $htmlfounds, $nospin, $shortcodes );
				$i          = 1;
				foreach( $htmlfounds as $htmlfound ) :
					
					$article = str_replace( $htmlfound, '('.str_repeat('*', $i).')', $article );
					$i++;

				endforeach;
			
		
			// ===== <<< [ Processing get list synonym ] >>> ===== //
				
				$file = get_option( 'sky_spinner', array() );
		
			// ===== <<< [ Checking all words for existance  ] >>> ===== //
				foreach ( $file as $line ) :
		
					// === Each synonym word
						$synonyms  = explode( '|', $line );
						$synonyms  = array_map( 'trim', $synonyms );
						$synonyms2 = array( $synonyms[0] );
			
					// ===== <<< [ Process synonyms2 ] >>> ===== //

						foreach ( $synonyms2 as $word ) :
					
							$word = str_replace( '/', '\/', $word );
							
							if ( trim( $word ) != '' ) :
						
								if ( preg_match( '/\b' . $word . '\b/u', $article ) ) :
						 
									$restruct           = array( $word );
									$restruct           = array_merge( $restruct, $synonyms );
									$restruct           = array_unique( $restruct );
									$restruct           = implode( '|', $restruct );
									$founds[md5($word)] = str_replace( array( "\n", "\r" ), '', $restruct ) ;
									$article            = preg_replace( '/\b' . $word . '\b/u', md5( $word ), $article );
								
								endif;
						
						
								// === replacing upper case words
									$uword = $this->sky_spinner_mb_ucfirst( $word );
						
								if( preg_match( '/\b' . $uword . '\b/u', $article ) ) :

									$restruct              = array($word);
									$restruct              = array_merge($restruct,$synonyms);
									$restruct              = array_unique($restruct);
									$restruct              = implode('|',$restruct);
									$founds[md5( $uword )] =  $this->sky_spinner_upper_case( str_replace( array( "\n", "\r" ), '',$restruct ) ) ;
									$article               = preg_replace( '/\b' . $uword . '\b/u', md5( $uword ), $article );
										
								endif;
						
							endif;
				
						endforeach;
					// ===== <<< [ End Process synonyms2 ] >>> ===== //

				endforeach;
		
			// ===== <<< [ End Checking all words for existance  ] >>> ===== //
	 	
		 
		
			// ==== restore html tags
				$i = 1;
				foreach( $htmlfounds as $htmlfound ) :

					$article = str_replace( '(' . str_repeat( '*', $i ) . ')', $htmlfound, $article );
					$i++;

				endforeach;
		 
		
			// ==== replace hashes with synonyms
				if( count( $founds ) !=0 ) :
					
					foreach ( $founds as $key => $val ) :

						$article = str_replace( $key, '{' . $val . '}', $article );

					endforeach;

				endif;
		
	
			// ==== deleting spin and nospin shortcodes
				$article = str_replace( array( '[nospin]', '[/nospin]' ), '', $article );
				
				$this->article = $article;
				
			 
			// ==== now article contains the synonyms on the form {test|test2}
				return $this->update_post();

		// ===== <<< [ End Processing  ] >>> ===== //
		 
	}
	
	// sky_spintax post , update data , return array of data
	public function update_post() {
		
		$sky_spinner = $this->article;
		
		// === synonyms
		if ( stristr( $sky_spinner, '911911' ) ) :
			
			$sky_spinner = str_replace( '911911', '**9999**', $sky_spinner );

		endif;

		$sky_spinner_arr    = explode( '**9999**' , $sky_spinner );
		$sky_spinner_ttl    = $sky_spinner_arr[0];
		$sky_spinner_cnt    = $sky_spinner_arr[1];

		// ==== sky_spintaxed wrirretten instance	 
			$sky_spintax        = new Sky_Spinner_Spintax;
			$sky_spintaxed      = $sky_spintax->spin( $sky_spinner );
			$sky_spintaxed2     = $sky_spintax->editor_form;
			$sky_spintaxed_arr  = explode( '**9999**', $sky_spintaxed );
			$sky_spintaxed_arr2 = explode( '**9999**', $sky_spintaxed2 );
			$sky_spintaxed_ttl  = $sky_spintaxed_arr[0];
			$sky_spintaxed_cnt  = $sky_spintaxed_arr[1];
			$sky_spintaxed_cnt2 = $sky_spintaxed_arr2[1];
		
		
		// ==== update post meta
			update_post_meta( $this->id, 'sky_spinner_ttl', $sky_spinner_ttl );
			update_post_meta( $this->id, 'sky_spinner_cnt', $sky_spinner_cnt );
			update_post_meta( $this->id, 'sky_spintaxed_ttl', $sky_spintaxed_ttl );
			update_post_meta( $this->id, 'sky_spintaxed_cnt', $sky_spintaxed_cnt );
			update_post_meta( $this->id, 'sky_spintaxed_cnt2', $sky_spintaxed_cnt2 );
			update_post_meta( $this->id, 'original_ttl', stripslashes( $this->title ) );
			update_post_meta( $this->id, 'original_cnt', stripslashes( $this->post ) );
			
			$return = array();
			$return['sky_spinner_ttl']    =  $sky_spinner_ttl;
			$return['sky_spinner_cnt']    =  $sky_spinner_cnt ;
			$return['sky_spintaxed_ttl']  =  $sky_spintaxed_ttl ;
			$return['sky_spintaxed_cnt']  = $sky_spintaxed_cnt;
			$return['sky_spintaxed_cnt2'] = $sky_spintaxed_cnt2;
			$return['original_ttl']   = $this->title;
			$return['original_cnt']   = $this->post;
		
		return $return ;
		
	}
	
	// ==== convert to upercase compatible with unicode chars
	public function sky_spinner_mb_ucfirst( $string ) {
		
		if ( function_exists( 'mb_strtoupper' ) ) :

			$encoding  = "utf8";
			$firstChar = mb_substr( $string, 0, 1, $encoding );
			$then      = mb_substr( $string, 1, null, $encoding );

			return mb_strtoupper( $firstChar, $encoding ) . $then;

		else :

			return ucfirst( $string );

		endif;
	}
	
	
	// ==== check the first letter of the word and upercase words in the line
	public function sky_spinner_upper_case( $line ) {
	
		$w_arr = explode('|',$line);

		for( $i = 0; $i < count( $w_arr ); $i++ ) :

			$w_arr[$i] = $this->sky_spinner_mb_ucfirst($w_arr[$i]) ;

		endfor;

		$line = implode('|', $w_arr );

		return $line;
	}
	
}