<?php
/**
 * Sky Framework Site Package.
 *
 * Register Style
 * This file register & enqueue style used in Sky Theme.
 *
 * @package    Sky Game
 */

/* -------------------------------------------------------
 * Create functions sky_enqueue_site_style
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_enqueue_site_style' ) ) :
	
	function sky_enqueue_site_style() {

		wp_enqueue_style( 'sky-game-style', get_stylesheet_uri() );

		wp_register_style( 'boostrap-style', SKY_THEME_ASSETS_URI .'/css/boostrap/boostrap.min.css' );
		wp_enqueue_style( 'boostrap-style' );

		wp_register_style( 'style-font-awesome', SKY_THEME_VENDOR_URI .'/fontawesome/css/font-awesome.min.css' );
		wp_enqueue_style( 'style-font-awesome' );
		
		wp_register_style( 'style-font-mfizz', SKY_THEME_VENDOR_URI .'/mfizz/font-mfizz.min.css' );
		wp_enqueue_style( 'style-font-mfizz' );

		// Carousel Slider
			wp_register_style( 'carousel', SKY_THEME_VENDOR_URI . '/carousel/owl.carousel.css');
			wp_register_style( 'carousel-theme', SKY_THEME_VENDOR_URI . '/carousel/owl.theme.css');
			
			wp_enqueue_style( 'carousel' );
			wp_enqueue_style( 'carousel-theme' );
	}

	add_action( 'wp_enqueue_scripts', 'sky_enqueue_site_style' );

endif;

/** ====== END sky_enqueue_site_style ====== **/

/* -------------------------------------------------------
 * Create functions sky_enqueue_admin_style
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_enqueue_admin_style' ) ) :
	
	function sky_enqueue_admin_style( $hook ) {

		wp_register_style( 'style-font-awesome', SKY_THEME_VENDOR_URI .'/fontawesome/css/font-awesome.min.css' );
		wp_enqueue_style( 'style-font-awesome' );

		if ( $hook != 'nav-menus.php' ) {
			wp_register_style( 'style-font-mfizz', SKY_THEME_VENDOR_URI .'/mfizz/font-mfizz.min.css' );
			wp_enqueue_style( 'style-font-mfizz' );
		}

		wp_register_style( 'style-admin', SKY_THEME_ASSETS_URI .'/css/admin.css' );
		wp_enqueue_style( 'style-admin' );

		wp_enqueue_style( 'wp-color-picker' );        
		wp_enqueue_script( 'wp-color-picker' );   

	}

	add_action( 'admin_enqueue_scripts', 'sky_enqueue_admin_style' );

endif;

/** ====== END sky_enqueue_admin_style ====== **/

function sky_optimization_finish($html) {
	// ===== <<<< get option
		$option = get_option( 'sky_general_settings' );

	// =====
		if ( !isset( $option['disable_compressed'] ) ) :
	  		return new Sky_Optimization($html);
	  	else :
	  		return $html;
	  	endif;
}

function sky_optimization_start() {
  	ob_start('sky_optimization_finish');
}
add_action( 'get_header', 'sky_optimization_start' );

