<?php
/*
 * Sky Control
 * This is a file which combines functions to add or run hooks, actions and filter.
 */

class Sky_Control {

	public function __construct() {
		
		add_action( 'admin_menu', 'sky_add_customizer_menu' );

		add_action( 'tgmpa_register', array( &$this, 'sky_require_plugins' ) );

		add_shortcode( 'non_free', array( &$this, 'sky_shortcode_non_free' ) );

		add_action( 'wp_ajax_sky_search_ajax_game','sky_live_search' );

		add_action( 'wp_ajax_nopriv_sky_search_ajax_game','sky_live_search' );

		add_action( 'before_delete_post', 'sky_auto_remove_post_and_attachment' );

		if ( is_singular() && comments_open() ) :

			wp_enqueue_script( 'comment-reply' );

		endif;

	}

	public function sky_shortcode_non_free( $atts ) {

		return '<div class="sky-notice">
					<i class="fa fa-exclamation-triangle"></i>
					<strong>Lưu ý:</strong> 
					Bắt đầu từ ngày 24/03/2015 <span style="color:red; font-weight: bold">Admin</span> cung cấp 2 link download phục vụ như cầu các member các bạn tùy ý chọn nha.<br />
					- <b>Bản Hack 1:</b> Mất phí, tốc độ tải siêu nhanh, không có quảng cáo!<br />
					- <b>Bản Hack 2:</b> Miễn phí, tốc độ tải chậm, có quảng cáo!
				</div>';

	}

	public function sky_require_plugins() {

		$plugins = array(
			// ===== <<< [ Requiring a Plugin From WordPress.org ] >>> ===== //
				
				array(
				       'name'      => 'Yoast SEO',
				       'slug'      => 'wordpress-seo',
				       'required'  => true,
				   ),
			
			// ===== <<< [ Requiring a Plugin From the Theme Directory ] >>> ===== //
				array(
			        'name'               => esc_html__( 'Sky Keyword Suggestion Tool', 'sky-game' ),
			        'slug'               => 'sky-keyword-suggestion-tool',
			        'source'             => 'http://update.skygame.mobi/plugins/sky-keyword-suggestion-tool.zip',
			        'required'           => true,
			        'version'            => '1.0.0',
			        'force_activation'   => true,
			    ),
			    array(
			        'name'               => esc_html__( 'WPBakery Visual Composer', 'sky-game' ),
			        'slug'               => 'js_composer',
			        'source'             => 'http://update.skygame.mobi/plugins/js_composer.zip',
			        'required'           => true,
			        'version'            => '4.8.1',
			        'force_activation'   => true,
			    )
		);

	    $config = array(
			'domain'            => 'sky-game',              // Text domain - likely want to be the same as your theme.
			'default_path'      => '',                           // Default absolute path to pre-packaged plugins
			'parent_slug'   	=> 'themes.php',                 // Default parent URL slug
			'menu'              => 'install-required-plugins',   // Menu slug
			'has_notices'       => true,                         // Show admin notices or not
			'is_automatic'      => true,            // Automatically activate plugins after installation or not
			'message'           => '',               // Message to output right before the plugins table
			'strings'           => array(
			  	'page_title'                          => __( 'Install Required Plugins', 'sky-game' ),
			  	'menu_title'                          => __( 'Install Plugins', 'sky-game' ),
				'installing'                          => __( 'Installing Plugin: %s', 'sky-game' ), // %1$s = plugin name
				'oops'                                => __( 'Something went wrong with the plugin API.', 'sky-game' ),
				'notice_can_install_required'         => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'sky-game' ), // %1$s = plugin name(s)
				'notice_can_install_recommended'      => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'sky-game' ), // %1$s = plugin name(s)
				'notice_cannot_install'               => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'sky-game' ), // %1$s = plugin name(s)
				'notice_can_activate_required'        => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'sky-game' ), // %1$s = plugin name(s)
				'notice_can_activate_recommended'     => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'sky-game' ), // %1$s = plugin name(s)
				'notice_cannot_activate'              => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'sky-game' ), // %1$s = plugin name(s)
				'notice_ask_to_update'                => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'sky-game' ), // %1$s = plugin name(s)
				'notice_cannot_update'                => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'sky-game' ), // %1$s = plugin name(s)
				'install_link'                        => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'sky-game' ),
				'activate_link'                       => _n_noop( 'Activate installed plugin', 'Activate installed plugins' ),
				'return'                              => __( 'Return to Required Plugins Installer', 'sky-game' ),
				'plugin_activated'                    => __( 'Plugin activated successfully.', 'sky-game' ),
				'complete'                            => __( 'All plugins installed and activated successfully. %s', 'sky-game' ) // %1$s = dashboard link
			)
		);
	 
	    tgmpa( $plugins, $config );

	}

}

new Sky_Control();