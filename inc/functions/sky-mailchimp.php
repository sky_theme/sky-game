<?php
if ( ! class_exists( 'SkyMailChimp' ) ) :

	class SkyMailChimp {

		public static $instance;

		public static function getInstance() {
			if( empty( self::$instance ) ) {
				self::$instance = new self;
			}
			return self::$instance;
		}

		private function __construct() {
			// Ajax for mail list
			add_action( 'wp_ajax_sky_mail_list', array(&$this, 'ajax_mail_list') );

			add_action( 'wp_ajax_sky_mc_subscribe', array(&$this, 'ajax_mc_subscribe') );
			add_action( 'wp_ajax_nopriv_sky_mc_subscribe', array(&$this, 'ajax_mc_subscribe'));
		}

		public function ajax_mc_subscribe() {
			if ( ! check_ajax_referer( 'sky-subscribe', 'nonce', false ) ) {
				$this->_ajax_exit( esc_html__( 'Your session is expired. Please reload and retry.', 'sky-game' ) );
			}

			// setup the email and name varaibles
			$email = strip_tags($_POST['mc_email']);
			
			// check for a valid email
			if(!is_email($email)) {
				$this->_ajax_exit(esc_html__('Your email address is invalid. Click back and enter a valid email address.', 'sky-game'), esc_html__('Invalid Email', 'sky-game'));
			}

			$list_id = strip_tags($_POST['mc_list_id']);

			if(empty($list_id)) {
				$this->_ajax_exit(esc_html__( '1 There\'s unknown problem. Please reload and retry.', 'sky-game' ));
			}
			
			// send this email to campaign_monitor
			if( $this->_subscribe_email($email, $list_id) ) {
				setcookie('sky_subscribed', 1, time()*20, '/');
				$this->_ajax_exit( esc_html__( 'Thank you for your subscription.', 'sky-game' ), true );
			} else {
				$this->_ajax_exit($list_id. ' ' .$email. ' ' .esc_html__( '2 There\'s unknown problem. Please reload and retry.', 'sky-game' ));
			}
		}

		public function ajax_mail_list() {
			$api_key = isset( $_POST['api_key'] ) ? $_POST['api_key'] : '';
			if( empty( $api_key ) ) {
				exit();
			}

			$lists = $this->get_mail_lists( $api_key );
			if( empty( $lists ) ) {
				exit();
			}

			foreach($lists as $id => $list_name) {
				echo '<option value="' . $id . '" >' . $list_name . '</option>';
			}

			exit();
		}

		// get an array of all campaign monitor subscription lists
		public function get_mail_lists( $api_key = '' ) {
			$api_key = trim($api_key);
			$api_key = empty( $api_key ) ? sky_get_customize_option('mailchimp_api') : $api_key;

			if(strlen($api_key) > 0 ) {
				
				$lists = array();
				
				if( ! class_exists('MCAPI' ) ) {
					require('MCAPI.class.php');
				}		
				$api = new MCAPI($api_key);
				$list_data = $api->lists();
				if($list_data) :
					foreach($list_data['data'] as $key => $list) {
						$lists[$list['id']] = $list['name'];
					}
				endif;
				return $lists;
					
			}

			return false;
		}

		// adds an email to the campaign_monitor subscription list
		private function _subscribe_email($email, $list_id) {
			$api_key = trim(sky_get_customize_option('mailchimp_api'));
			if(strlen($api_key) > 0 ) {
				
				if( ! class_exists('MCAPI' ) ) {
					require('MCAPI.class.php');
				}
				$api = new MCAPI($api_key);
				// $opt_in = isset(self::$sky_mc_options['double_opt_in']) ? true : false;
				if($api->listSubscribe($list_id, $email, array(), 'html', true) === true) {
					return true;
				}
			
			}
			return false;
		}

		private function _ajax_exit( $data = '', $success = false, $redirect = '' ) {
			$response = array(
				'success' => $success,
				'data' => $data,
			);

			if( !empty( $redirect ) ) {
				$response['redirect'] = $redirect;
			}

			echo json_encode($response);
			exit();
		}
	}

endif;
	
global $sky_mailchimp;
$sky_mailchimp = SkyMailChimp::getInstance();