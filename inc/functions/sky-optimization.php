<?php
class Sky_Optimization {
    // Settings
    protected $compress_css = true;
    protected $compress_js = true;
    protected $info_comment = true;
    protected $remove_comments = true;

    // Variables
    protected $html;
    public function __construct($html){
   	  if (!empty($html)) {
     		$this->parseHTML($html);
   	  }
    }
    public function __toString(){
   	  return $this->html;
    }
    protected function bottomComment($raw, $compressed) {
   	  $raw = strlen($raw);
   	  $compressed = strlen($compressed);
   	  $savings = ($raw-$compressed) / $raw * 100;
   	  $savings = round($savings, 2);
   	  return '<!-- Sky Optimization, size saved '. $savings .'%. From '.$raw.' bytes, now '.$compressed.' bytes-->';
    }
    protected function minifyHTML($html){
   	  $pattern = '/<(?<script>script).*?<\/script\s*>|<(?<style>style).*?<\/style\s*>|<!(?<comment>--).*?-->|<(?<tag>[\/\w.:-]*)(?:".*?"|\'.*?\'|[^\'">]+)*>|(?<text>((<[^!\/\w.:-])?[^<]*)+)|/si';
   	  preg_match_all($pattern, $html, $matches, PREG_SET_ORDER);
   	  $overriding = false;
   	  $raw_tag = false;
   	  // Variable reused for output
   	  $html = '';
   	  foreach ($matches as $token) {
        $tag = (isset($token['tag'])) ? strtolower($token['tag']) : null;
        $content = $token[0];
   		 
        if (is_null($tag)) {

          if ( !empty($token['script']) ) {
            $strip = $this->compress_js;
          } else if ( !empty($token['style']) ) {
            $strip = $this->compress_css;
   			  } else if ($content == '<!--wp-html-compression no compression-->') {
            $overriding = !$overriding;
   				  // Don't print the comment
   				  continue;
   			  } else if ($this->remove_comments) {
   				  if (!$overriding && $raw_tag != 'textarea') {
   					  // Remove any HTML comments, except MSIE conditional comments
   					  $content = preg_replace('/<!--(?!\s*(?:\[if [^\]]+]|<!|>))(?:(?!-->).)*-->/s', '', $content);
   				  }
   			  }
   		  } else {
   			  if ($tag == 'pre' || $tag == 'textarea') {
   				  $raw_tag = $tag;
   			  } else if ($tag == '/pre' || $tag == '/textarea') {
   				  $raw_tag = false;
   			  } else {
   				  if ($raw_tag || $overriding) {
   					  $strip = false;
   				  } else {
   					  $strip = true;
   					  // Remove any empty attributes, except:
   					  // action, alt, content, src
   					  $content = preg_replace('/(\s+)(\w++(?<!\baction|\balt|\bcontent|\bsrc)="")/', '$1', $content);
   					 
   					  // Remove any space before the end of self-closing XHTML tags
   					  // JavaScript excluded
   					  $content = str_replace(' />', '/>', $content);
   				  }
   			  }
   		  }
   		 
   		  if ($strip) {
   			  $content = $this->removeWhiteSpace($content);
        }
   		 
   		 $html .= $content;
   	  }
   	  return $html;
    }
   	 
    public function parseHTML($html) {
   	  $this->html = $this->minifyHTML($html);
   	 
   	  if ($this->info_comment) :
        $this->html .= "\n" . $this->bottomComment($html, $this->html);
   	  endif;
    }
    
    protected function removeWhiteSpace($str) {
   	  $str = str_replace("\t", '', $str);
   	  $str = str_replace("\n", '', $str);
   	  // $str = str_replace("\r", '', $str);
   	 
   	  while (stristr($str, '  ')) :
        $str = str_replace('  ', ' ', $str);
   	  endwhile;
   	 
   	  return $str;
    }
}

/* -------------------------------------------------------
 * Create functions sky_auto_remove_meta
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_auto_remove_meta' ) ) :
  
  function sky_auto_remove_meta( $post_id ) {
    
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return;
    if (!current_user_can('edit_post', $post_id)) return;
    $custom_fields = get_post_custom($post_id);
    if(!$custom_fields) return;

    foreach($custom_fields as $key=>$custom_field) {

        $values = array_filter($custom_field);

        if(empty($values)):
            delete_post_meta($post_id, $key);
        endif;
    
    }
    return;

  }

  add_action('save_post','sky_auto_remove_meta');

endif;

/** ====== END sky_auto_remove_meta ====== **/


/* -------------------------------------------------------
 * Create functions sky_remove_version
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_remove_version' ) ) :
  
  function sky_remove_version( $src ) {
    
    $rqs = explode( '?ver', $src );
    return $rqs[0];

  }

endif;

/** ====== END sky_remove_version ====== **/

/* -------------------------------------------------------
 * Create functions sky_auto_remove_post_and_attachment
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_auto_remove_post_and_attachment' ) ) :
  
  function sky_auto_remove_post_and_attachment( $post_id ) {

    global $wpdb;
    // ===== <<< [ Find id file attachment ] >>> ===== //
      $thumbnails = $wpdb->get_results( "SELECT * FROM $wpdb->postmeta WHERE meta_key = '_thumbnail_id' AND post_id = $post_ID" );
      foreach ( $thumbnails as $thumbnail ) :
        
        wp_delete_attachment( $thumbnail->meta_value, true );
      
      endforeach;
    // ===== <<< [ Remove file attachment ] >>> ===== //
    
      $attachments = $wpdb->get_results( "SELECT * FROM $wpdb->posts WHERE post_parent = $post_ID AND post_type = 'attachment'" );
      foreach ( $attachments as $attachment ) :
        wp_delete_attachment( $attachment->ID, true );
      endforeach;
      $wpdb->query( "DELETE FROM $wpdb->postmeta WHERE meta_key = '_thumbnail_id' AND post_id = $post_ID" );
  }

endif;

/** ====== END sky_auto_remove_post_and_attachment ====== **/

// ===== Sky_Database

if ( !class_exists( 'Sky_Database' ) ) :

   class Sky_Database {

      public function __construct() {

      }

      // ==== sky_database_clean
         public function sky_database_clean( $type ) {
       
            global $wpdb;
            // If we are in multi-site installation and in main site then we should process all elements in all tables
            if(function_exists('is_multisite') && is_multisite() && is_main_site()){
               $blogs_ids = $wpdb->get_col("SELECT blog_id FROM $wpdb->blogs");
               foreach($blogs_ids as $blog_id){
                  
                  switch_to_blog($blog_id);
                  switch($type){
                     case "revision":
                        $wpdb->query("DELETE FROM $wpdb->posts WHERE post_type = 'revision'");
                        $wpdb->query("DELETE pm FROM $wpdb->postmeta pm LEFT JOIN $wpdb->posts wp ON wp.ID = pm.post_id WHERE wp.ID IS NULL");
                        $wpdb->query("DELETE FROM $wpdb->term_relationships WHERE term_taxonomy_id=1 AND object_id NOT IN (SELECT id FROM $wpdb->posts)");
                        break;
                     case "draft":
                        $wpdb->query("DELETE FROM $wpdb->posts WHERE post_status = 'draft'");
                        $wpdb->query("DELETE pm FROM $wpdb->postmeta pm LEFT JOIN $wpdb->posts wp ON wp.ID = pm.post_id WHERE wp.ID IS NULL");
                        $wpdb->query("DELETE FROM $wpdb->term_relationships WHERE term_taxonomy_id=1 AND object_id NOT IN (SELECT id FROM $wpdb->posts)");
                        break;
                     case "autodraft":
                        $wpdb->query("DELETE FROM $wpdb->posts WHERE post_status = 'auto-draft'");
                        $wpdb->query("DELETE pm FROM $wpdb->postmeta pm LEFT JOIN $wpdb->posts wp ON wp.ID = pm.post_id WHERE wp.ID IS NULL");
                        $wpdb->query("DELETE FROM $wpdb->term_relationships WHERE term_taxonomy_id=1 AND object_id NOT IN (SELECT id FROM $wpdb->posts)");
                        break;
                     case "moderated":
                        $wpdb->query("DELETE FROM $wpdb->comments WHERE comment_approved = '0'");
                        $wpdb->query("DELETE FROM $wpdb->commentmeta WHERE comment_id NOT IN (SELECT comment_id FROM $wpdb->comments)");
                        break;
                     case "spam":
                        $wpdb->query("DELETE FROM $wpdb->comments WHERE comment_approved = 'spam'");
                        $wpdb->query("DELETE FROM $wpdb->commentmeta WHERE comment_id NOT IN (SELECT comment_id FROM $wpdb->comments)");
                        break;
                     case "trash":
                        $wpdb->query("DELETE FROM $wpdb->comments WHERE comment_approved = 'trash'");
                        $wpdb->query("DELETE FROM $wpdb->commentmeta WHERE comment_id NOT IN (SELECT comment_id FROM $wpdb->comments)");
                        break;
                     case "postmeta":
                        //"DELETE FROM $wpdb->postmeta WHERE NOT EXISTS ( SELECT * FROM $wpdb->posts WHERE $wpdb->postmeta.post_id = $wpdb->posts.ID )";
                        $wpdb->query("DELETE pm FROM $wpdb->postmeta pm LEFT JOIN $wpdb->posts wp ON wp.ID = pm.post_id WHERE wp.ID IS NULL");
                        break;
                     case "commentmeta":
                        $wpdb->query("DELETE FROM $wpdb->commentmeta WHERE comment_id NOT IN (SELECT comment_id FROM $wpdb->comments)");
                        break;
                     case "relationships":
                        $wpdb->query("DELETE FROM $wpdb->term_relationships WHERE term_taxonomy_id=1 AND object_id NOT IN (SELECT id FROM $wpdb->posts)");
                        break;
                     case "feed":
                        $wpdb->query("DELETE FROM $wpdb->options WHERE option_name LIKE '_site_transient_browser_%' OR option_name LIKE '_site_transient_timeout_browser_%' OR option_name LIKE '_transient_feed_%' OR option_name LIKE '_transient_timeout_feed_%'");
                        break;
                  }
                  restore_current_blog();
               }
            }else{
               // If we are in single site installation or in multi-site but not in main site, then we should process only current tables
               switch($type){
                  case "revision":
                     $wpdb->query("DELETE FROM $wpdb->posts WHERE post_type = 'revision'");
                     $wpdb->query("DELETE pm FROM $wpdb->postmeta pm LEFT JOIN $wpdb->posts wp ON wp.ID = pm.post_id WHERE wp.ID IS NULL");
                     $wpdb->query("DELETE FROM $wpdb->term_relationships WHERE term_taxonomy_id=1 AND object_id NOT IN (SELECT id FROM $wpdb->posts)");
                     break;
                  case "draft":
                     $wpdb->query("DELETE FROM $wpdb->posts WHERE post_status = 'draft'");
                     $wpdb->query("DELETE pm FROM $wpdb->postmeta pm LEFT JOIN $wpdb->posts wp ON wp.ID = pm.post_id WHERE wp.ID IS NULL");
                     $wpdb->query("DELETE FROM $wpdb->term_relationships WHERE term_taxonomy_id=1 AND object_id NOT IN (SELECT id FROM $wpdb->posts)");
                     break;
                  case "autodraft":
                     $wpdb->query("DELETE FROM $wpdb->posts WHERE post_status = 'auto-draft'");
                     $wpdb->query("DELETE pm FROM $wpdb->postmeta pm LEFT JOIN $wpdb->posts wp ON wp.ID = pm.post_id WHERE wp.ID IS NULL");
                     $wpdb->query("DELETE FROM $wpdb->term_relationships WHERE term_taxonomy_id=1 AND object_id NOT IN (SELECT id FROM $wpdb->posts)");
                     break;
                  case "moderated":
                     $wpdb->query("DELETE FROM $wpdb->comments WHERE comment_approved = '0'");
                     $wpdb->query("DELETE FROM $wpdb->commentmeta WHERE comment_id NOT IN (SELECT comment_id FROM $wpdb->comments)");
                     break;
                  case "spam":
                     $wpdb->query("DELETE FROM $wpdb->comments WHERE comment_approved = 'spam'");
                     $wpdb->query("DELETE FROM $wpdb->commentmeta WHERE comment_id NOT IN (SELECT comment_id FROM $wpdb->comments)");
                     break;
                  case "trash":
                     $wpdb->query("DELETE FROM $wpdb->comments WHERE comment_approved = 'trash'");
                     $wpdb->query("DELETE FROM $wpdb->commentmeta WHERE comment_id NOT IN (SELECT comment_id FROM $wpdb->comments)");
                     break;
                  case "postmeta":
                     //"DELETE FROM $wpdb->postmeta WHERE NOT EXISTS ( SELECT * FROM $wpdb->posts WHERE $wpdb->postmeta.post_id = $wpdb->posts.ID )";
                     $wpdb->query("DELETE pm FROM $wpdb->postmeta pm LEFT JOIN $wpdb->posts wp ON wp.ID = pm.post_id WHERE wp.ID IS NULL");
                     break;
                  case "commentmeta":
                     $wpdb->query("DELETE FROM $wpdb->commentmeta WHERE comment_id NOT IN (SELECT comment_id FROM $wpdb->comments)");
                     break;
                  case "relationships":
                     $wpdb->query("DELETE FROM $wpdb->term_relationships WHERE term_taxonomy_id=1 AND object_id NOT IN (SELECT id FROM $wpdb->posts)");
                     break;
                  case "feed":
                     $wpdb->query("DELETE FROM $wpdb->options WHERE option_name LIKE '_site_transient_browser_%' OR option_name LIKE '_site_transient_timeout_browser_%' OR option_name LIKE '_transient_feed_%' OR option_name LIKE '_transient_timeout_feed_%'");
                     break;
               }
            }
         }
      // ==== End sky_database_clean
      
      // ==== sky_cleaner_count
         public function sky_cleaner_count() {
            global $wpdb;
            $sky_count_total_unused = 0;
            // If we are in multi-site installation and in main site then we should process all elements in all tables
            if( function_exists('is_multisite') && is_multisite() && is_main_site() ){
               $blogs_ids = $wpdb->get_col("SELECT blog_id FROM $wpdb->blogs");
               foreach($blogs_ids as $blog_id){
                  switch_to_blog($blog_id);
                  $sky_count_unused["revision"] += $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->posts WHERE post_type = 'revision'");
                  $sky_count_unused["draft"] += $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->posts WHERE post_status = 'draft'");
                  $sky_count_unused["autodraft"] += $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->posts WHERE post_status = 'auto-draft'");
                  $sky_count_unused["moderated"] += $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->comments WHERE comment_approved = '0'");
                  $sky_count_unused["spam"] += $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->comments WHERE comment_approved = 'spam'");
                  $sky_count_unused["trash"] += $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->comments WHERE comment_approved = 'trash'");
                  //"SELECT COUNT(*) FROM $wpdb->postmeta WHERE NOT EXISTS ( SELECT * FROM $wpdb->posts WHERE $wpdb->postmeta.post_id = $wpdb->posts.ID )";
                  $sky_count_unused["postmeta"] += $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->postmeta pm LEFT JOIN $wpdb->posts wp ON wp.ID = pm.post_id WHERE wp.ID IS NULL");
                  $sky_count_unused["commentmeta"] += $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->commentmeta WHERE comment_id NOT IN (SELECT comment_id FROM $wpdb->comments)");
                  $sky_count_unused["relationships"] += $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->term_relationships WHERE term_taxonomy_id=1 AND object_id NOT IN (SELECT id FROM $wpdb->posts)");
                  $sky_count_unused["feed"] += $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->options WHERE option_name LIKE '_site_transient_browser_%' OR option_name LIKE '_site_transient_timeout_browser_%' OR option_name LIKE '_transient_feed_%' OR option_name LIKE '_transient_timeout_feed_%'");   
                  restore_current_blog();
               }
               $sky_count_unused["total"] = $sky_count_unused["revision"] + $sky_count_unused["draft"] + $sky_count_unused["autodraft"] + $sky_count_unused["moderated"] + $sky_count_unused["spam"] + $sky_count_unused["trash"] + $sky_count_unused["postmeta"] + $sky_count_unused["commentmeta"] + $sky_count_unused["relationships"] + $sky_count_unused["feed"];
            }else{
               // If we are in single site installation or in multi-site but not in main site, then we should process only current tables
               $sky_count_unused["revision"] = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->posts WHERE post_type = 'revision'");
               $sky_count_unused["draft"] = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->posts WHERE post_status = 'draft'");
               $sky_count_unused["autodraft"] = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->posts WHERE post_status = 'auto-draft'");
               $sky_count_unused["moderated"] = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->comments WHERE comment_approved = '0'");
               $sky_count_unused["spam"] = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->comments WHERE comment_approved = 'spam'");
               $sky_count_unused["trash"] = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->comments WHERE comment_approved = 'trash'");
               //"SELECT COUNT(*) FROM $wpdb->postmeta WHERE NOT EXISTS ( SELECT * FROM $wpdb->posts WHERE $wpdb->postmeta.post_id = $wpdb->posts.ID )";
               $sky_count_unused["postmeta"] = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->postmeta pm LEFT JOIN $wpdb->posts wp ON wp.ID = pm.post_id WHERE wp.ID IS NULL");
               $sky_count_unused["commentmeta"] = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->commentmeta WHERE comment_id NOT IN (SELECT comment_id FROM $wpdb->comments)");
               $sky_count_unused["relationships"] = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->term_relationships WHERE term_taxonomy_id=1 AND object_id NOT IN (SELECT id FROM $wpdb->posts)");
               $sky_count_unused["feed"] = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->options WHERE option_name LIKE '_site_transient_browser_%' OR option_name LIKE '_site_transient_timeout_browser_%' OR option_name LIKE '_transient_feed_%' OR option_name LIKE '_transient_timeout_feed_%'");
               $sky_count_unused["total"] = $sky_count_unused["revision"] + $sky_count_unused["draft"] + $sky_count_unused["autodraft"] + $sky_count_unused["moderated"] + $sky_count_unused["spam"] + $sky_count_unused["trash"] + $sky_count_unused["postmeta"] + $sky_count_unused["commentmeta"] + $sky_count_unused["relationships"] + $sky_count_unused["feed"];
            }
            return $sky_count_unused;
         }
      // ==== End sky_cleaner_count

      // ==== sky_cleaner_optimize
         public function sky_cleaner_optimize() {
            global $wpdb;
           
            $prefix  = str_replace( '_', '\_', $wpdb->prefix );
            $sky_sql = "SELECT table_name, data_free FROM information_schema.tables WHERE table_schema = '".DB_NAME."' and data_free > 0";
            
            if( !is_main_site() ) :

               $sky_sql = $sky_sql . " and table_name LIKE '{$prefix}%'";

            endif;

            $result = $wpdb->get_results($sky_sql);
            
            foreach( $result as $row ) :

               $sky_sql = 'OPTIMIZE TABLE '.$row->table_name;
               $wpdb->query( $sky_sql );
            
            endforeach;

         }
      // ==== End sky_cleaner_optimize

      // ==== sky_cleaner_reset
         public function sky_cleaner_reset() {

            require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
            global $current_user, $wpdb;

            $blogname    = get_option( 'blogname' );
            $admin_email = get_option( 'admin_email' );
            $blog_public = get_option( 'blog_public' );

            if ( $current_user->user_login != 'admin' ) :

               $user = get_user_by( 'login', 'admin' );

            endif;

            if ( empty( $user->user_level ) || $user->user_level < 10 ) :

               $user = $current_user;

            endif;

            $prefix = str_replace( '_', '\_', $wpdb->prefix );
            $tables = $wpdb->get_col( "SHOW TABLES LIKE '{$prefix}%'" );
            
            foreach ( $tables as $table ) :

               $wpdb->query( "DROP TABLE $table" );

            endforeach;

            // === Install wordpress
               $result = wp_install( $blogname, $user->user_login, $user->user_email, $blog_public);
               extract( $result, EXTR_SKIP );

            // === Set user password
               $query = $wpdb->prepare( "UPDATE $wpdb->users SET user_pass = %s, user_activation_key = '' WHERE ID = %d", $user->user_pass, $user_id );
               $wpdb->query( $query );

            // ==== Test for functions
               $get_user_meta = function_exists( 'get_user_meta' ) ? 'get_user_meta' : 'get_usermeta';
               $update_user_meta = function_exists( 'update_user_meta' ) ? 'update_user_meta' : 'update_usermeta';
            
            // === Say to wordpress that we will not use generated password
               if ( $get_user_meta( $user_id, 'default_password_nag' ) ) :

                  $update_user_meta( $user_id, 'default_password_nag', false );

               endif;

               if ( $get_user_meta( $user_id, $wpdb->prefix . 'default_password_nag' ) ) :

                  $update_user_meta( $user_id, $wpdb->prefix . 'default_password_nag', false );

               endif;

            // === Clear all cookies associated with authentication
               wp_clear_auth_cookie();

            // === Set the authentication cookies based User ID
               wp_set_auth_cookie( $user_id );

            // === Redirect user to admin pannel
               wp_redirect( admin_url());

         } 
      // ==== End sky_cleaner_reset

   }

endif;

// ===== End Sky_Database