<?php
/*
 * Library Sky Shortcode
 * 
 * @package Sky Game
 * 
 */

/* -------------------------------------------------------
 * Create functions sky_download_page
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_download_page' ) ) :
	
	function sky_download_page( $atts ) {
		ob_start();
			
			sky_get_list_download( get_query_var( 'id_game' ), 'box' );

		return ob_get_clean();
	}

	add_shortcode( 'sky_download_page', 'sky_download_page' );

endif;

/** ====== END sky_download_page ====== **/

/* -------------------------------------------------------
 * Create functions sky_recent_game
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_recent_game' ) ) :
	
	function sky_recent_game( $atts, $content = null ) {

		extract( shortcode_atts( array(
			'title'             => '',
			'type'				=> 'list',
			'game_category'		=> 'all',
			'number'            => '6',
			'show'				=> '',
			'style'				=> 'list',
			'show_thumb'		=> 'yes',
			'show_paged'		=> 'no',
			'show_readmore'		=> 'no',
			'orderby'			=> 'latest',
			'visibility'        => '',
			'class'             => '',
			'custom_style'      => ''
		), $atts ) );

		$visibility       = ( $visibility      != ''     ) && ( $visibility != 'all' ) ? esc_attr( $visibility ) : '';
		$class            = ( $class           != ''     ) ? 'recent-game ' . esc_attr( $class ) : 'recent-game';
		switch ($visibility) {
			case 'hidden-phone':
				$class .= ' hidden-xs';
				break;
			case 'hidden-tablet':
				$class .= ' hidden-sm hidden-md';
				break;
			case 'hidden-pc':
				$class .= ' hidden-lg';
				break;
			case 'visible-phone':
				$class .= ' visible-xs-block visible-xs-inline visible-xs-inline-block';
				break;
			case 'visible-tablet':
				$class .= ' visible-sm-block visible-sm-inline visible-sm-inline-block visible-md-block visible-md-inline visible-md-inline-block';
				break;
			case 'visible-phone':
				$class .= ' visible-lg-block visible-lg-inline visible-lg-inline-block';
				break;
		}
		
		$class = ( $class != '' ) ? ' class="' . esc_attr( $class ) . '"' : '';
		$custom_style = ( $custom_style != '' ) ? ' style="' . $custom_style . '"' : '';
		if( is_front_page() || is_home()) {
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : ( ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1 );
		} else {
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		}
		$sky_args = array(
			'paged'			  	  => $paged,
			'posts_per_page'      => $number,
			'post_status'         => 'publish',
			'post_type'			  => 'sky-game',
		);

		if ( $game_category != 'all' ) :

			$args['tax_query'] = array('relation' => 'AND');
			if( !empty($game_category) ) :
				$sky_args['tax_query'][] = array(
					'taxonomy'     => sky_get_option_setting( 'sky_general', 'category_game_slug', 'sky-game-cat' ),
					'field'        => 'slug',
					'terms'        => $game_category
				);
			endif; // === << empty($game_category) >>

		endif; // === << game_category >>

		if( $orderby == 'latest' ) :

			$sky_args['orderby'] = 'modified';
			$sky_args['order'] = 'DESC';

		elseif( $orderby == 'oldest' ) :

			$sky_args['orderby'] = 'modified';
			$sky_args['order'] = 'ASC';

		elseif( $orderby == 'alphabet' ) :

			$sky_args['orderby'] = 'title';
			$sky_args['order'] = 'ASC';	

		elseif( $orderby == 'ralphabet' ) :

			$sky_args['orderby'] = 'title';
			$sky_args['order'] = 'DESC';

		elseif( $orderby == 'random' ) :

			$sky_args['orderby'] = 'rand';

		endif; // --- /order

		$wp_query             = NULL;
		$wp_query             = new WP_Query( $sky_args );

		switch ($style) {
			case 'list':
				$class_parent = ' class="sky-list-single"';
				break;
			case 'grid':
				$class_parent = ' class="sky-list-grid"';
				break;
			case 'slider':
				$class_parent = ' class="sky-list-slider"';
				break;
			case 'featured':
				$class_parent = ' class="sky-list-featured"';
				break;
		}

        if ( $show_thumb == 'yes' ) echo "<ul{$class_parent}>";

	        while ( $wp_query->have_posts() ) :
	        	$wp_query->the_post();

	        	// --
		        	$url = get_permalink( );
		        	$title = get_the_title();

				if ( $style == 'list' ) :
					if ( $show_thumb ) :
						
						?>
							<div class="sky-item">
								
								<div class="sky-thumb">
									
									<a href="<?php the_permalink(); ?>" title="<?php the_title( ); ?>">

										<img src="<?php echo sky_get_thumb( ); ?>" alt="<?php the_title( ); ?>" />

									</a>

								</div>

								<div class="sky-entry">
									
									<a href="<?php the_permalink(); ?>" title="<?php the_title( ); ?>">
										<h4>
											<?php the_title( ); ?>
										</h4>
									</a>

									<p>
										<?php esc_html_e( 'Hỗ trợ', 'sky-game' ); ?>&nbsp;
										<?php sky_get_list_support_os(); ?>
									</p>

									<a target="_blank" href="<?php echo sky_get_url_download() ?>" title="<?php esc_html_e( 'Tải về', 'sky-game' ); ?>">
										<span class="sky-btn-download">
											<i class="fa fa-download"></i>&nbsp; <?php esc_html_e( 'Tải về', 'sky-game' ); ?>
										</span>
									</a>

								</div>

							</div>
						<?php
					
					else :

						echo "<li>";
						echo "<a href='{$url}' title='{$title}'>{$title}</a>";
						echo "</li>";

					endif;

				endif; // --- $style == 'list'

				if ( $style == 'grid' ) :

					?>

						<li class="col-xs-6 col-sm-3 col-md-2">
		                    <span class="sky-grid-item">
		                      	<a href="<?php echo $url ?>" title="<?php echo $title ?>">
		                        	<span class="sky-icon">
		                          		<img src="<?php echo sky_get_thumb( ); ?>" alt="<?php the_title( ); ?>" />
		                       		</span>
		                      	</a>
		                      	<span class="sky-info">
		                        	<span class="title">
		                          		<a href="<?php echo $url ?>" title="<?php echo $title ?>"><?php echo $title ?></a>
		                        	</span>
		                      	</span>
		                      	<span class="sky-bottom">
		                       		<span class="view">
		                       			<i class="fa fa-eye"></i>&nbsp; 
		                       			<?php echo sky_get_post_views(); ?>
		                       		</span>
		                      </span>
		                    </span>
		                </li>

					<?php

				endif; // --- $style == 'grid'
			
			endwhile;
			if ( $show_thumb == 'yes' ) echo "</ul>";

			if ( $show_paged == 'yes' ) sky_pagination($wp_query->max_num_pages);

			if ( $show_readmore == 'yes' ) :

				$tax_cat = get_terms( sky_get_option_setting( 'sky_general', 'category_game_slug', 'sky-game-cat' ) );
				foreach ( $tax_cat as $cat ) {

					if ( $cat->slug == $game_category ) :
						// The $term is an object, so we don't need to specify the $taxonomy.
						    $cat_link = get_term_link( $cat );
						   
						// If there was an error, continue to the next term.
						    if ( is_wp_error( $cat_link ) ) {
						        continue;
						    }

						echo "<div class=\"sky-readmore\">";
						echo "	<a target='_blank' href='{$cat_link}' title='{$cat->name}'>" . esc_html__( 'Read more', 'sky-game' ) . "</a>";
						echo "</div>";
					endif;
				}
				

			endif;

		// -- Restore original Post Data
			wp_reset_postdata();

	}

	add_shortcode('sky_recent_game', 'sky_recent_game');

endif;

/** ====== END sky_recent_game ====== **/


/* -------------------------------------------------------
 * Create functions sky_shortcode_testimonial
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_shortcode_testimonial' ) ) :
	
	function sky_shortcode_testimonial() {

		// wp_enqueue_script( 'vendor-carouFredSel' );
        extract(shortcode_atts(array(
            // 'title'          => '',
            'image_per_page' =>  '3',
            'style'          => 1,
            'autoplay'       =>  'true',
            'hidden_pagination' => 'true'
        ), $atts));

        ob_start();
            $args = array(
                'post_type'         =>  'testimonial',
                'posts_per_page'    =>   '-1',
            );
           
            $query = new WP_Query( $args );
            if ( $style == 2 ) $image_per_page = 1;
            $options = array(
                'id'                => 'testimonial',
                'show'              => 'testimonial',
                'style'             => $style,
                'max'               => $image_per_page,
                'autoplay'          => $autoplay,
                'hidden_pagination' => $hidden_pagination
            );
            noo_caroufredsel_slider( $query, $options );

        $testimonial = ob_get_contents();
        ob_end_clean();
        return $testimonial;

	}

	add_shortcode('sky_testimonial','sky_shortcode_testimonial');

endif;

/** ====== END sky_shortcode_testimonial ====== **/

/* -------------------------------------------------------
 * Create functions sky_shortcode_box_like_facebook
 * ------------------------------------------------------- */

if ( ! function_exists( 'sky_shortcode_box_like_facebook' ) ) :
	
	function sky_shortcode_box_like_facebook( $atts ) {
		
		extract( shortcode_atts(
			array(
				'url' 			=> 'https://www.facebook.com/hackgame.mienphi.no1',
				'width' 		=> '380',
				'height' 		=> '500',
				'show_faces' 	=> 'true',
				'show_header' 	=> 'true',
				'show_border' 	=> 'false'
			), $atts )
		);
		
		$html = '<div class="fb-like-box" data-href="' . $url . '" data-colorscheme="light" data-width="'.$width.'" data-height="'.$height.'"  data-show-faces="'.$show_faces.'" data-header="'.$show_Header.'" data-stream="false" data-show-border="'.$show_border.'"></div>';
		return $html;

	}

	add_shortcode( 'sky-boxlike', 'sky_shortcode_box_like_facebook' );

endif;

/** ====== END sky_shortcode_box_like_facebook ====== **/