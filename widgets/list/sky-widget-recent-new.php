<?php 

class Sky_Recent_News extends WP_Widget {

	public function __construct() {
		$widget_ops = array('classname' => 'sky_recent_news_widget', 'description' => esc_html__( "Your site&#8217;s most recent Posts.",'sky-game') );
		parent::__construct('recent-news', esc_html__('Sky Recent News','sky-game'), $widget_ops);
		$this->alt_option_name = 'sky_recent_news_widget';

		add_action( 'save_post', array($this, 'flush_widget_cache') );
		add_action( 'deleted_post', array($this, 'flush_widget_cache') );
		add_action( 'switch_theme', array($this, 'flush_widget_cache') );
	}

	public function widget($args, $instance) {
		$cache = array();
		if ( ! $this->is_preview() ) {
			$cache = wp_cache_get( 'sky_recent_news_widget', 'widget' );
		}

		if ( ! is_array( $cache ) ) {
			$cache = array();
		}

		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : esc_html__( 'Recent News','sky-game' );

		/** This filter is documented in wp-includes/default-widgets.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
		if ( ! $number )
			$number = 5;
		$show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;

		/**
		 * Filter the arguments for the Recent Posts widget.
		 *
		 * @since 3.4.0
		 *
		 * @see WP_Query::get_posts()
		 *
		 * @param array $args An array of arguments used to retrieve the recent posts.
		 */
		$r = new WP_Query( apply_filters( 'widget_news_args', array(
			'posts_per_page'      => $number,
			'no_found_rows'       => true,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => true,
		) ) );

		if ($r->have_posts()) :
			echo $args['before_widget'];
			if ( $title ) :
				echo $args['before_title'] . $title . $args['after_title'];
			endif;
			if ( $instance['sub_title'] ) : ?>

				<div class="sub_title"><?php echo $instance['sub_title']; ?></div>

			<?php endif; ?>
			
			<div class="recent_new_widget">
				
				<?php while ( $r->have_posts() ) : $r->the_post(); ?>

					<div class="item_recent_new">
						
						<div class="item_thumb">
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<img src="<?php echo sky_get_thumb(); ?>" alt="<?php the_title(); ?>">
								<?php if ( $show_date ) : ?>
									<div class="item_date">
										<?php the_date( 'd M' ); ?>
									</div>
								<?php endif; ?>
							</a>
						</div><!-- /.item_thumb -->

						<div class="item_info">
							<div class="item_title">
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
									<?php the_title( ); ?>
								</a>
							</div>
							<div class="item_excerpt">
								<?php
									echo sky_substr( get_the_excerpt( ), 7 );
								?>
							</div>
						</div><!-- /.item_info -->

					</div><!-- /.item_recent_new -->

				<?php endwhile; ?>
			</div><!-- /.recent_new_widget -->
			<?php echo $args['after_widget'];
			// Reset the global $the_post as this query will have stomped on it
			wp_reset_postdata();

		endif;

		if ( ! $this->is_preview() ) {
			$cache[ $args['widget_id'] ] = ob_get_flush();
			wp_cache_set( 'sky_recent_news_widget', $cache, 'widget' );
		} else {
			ob_end_flush();
		}
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['sub_title'] = strip_tags($new_instance['sub_title']);
		$instance['number'] = (int) $new_instance['number'];
		$instance['show_date'] = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['widget_recent_entries']) )
			delete_option('widget_recent_entries');

		return $instance;
	}

	public function flush_widget_cache() {
		wp_cache_delete('sky_recent_news_widget', 'widget');
	}

	public function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$sub_title     = isset( $instance['sub_title'] ) ? esc_attr( $instance['sub_title'] ) : '';
		$number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
		?>
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>">
					<?php esc_html_e( 'Title', 'sky-game' ); ?>
				</label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
			</p>

			<p>
				<label for="<?php echo $this->get_field_id( 'sub_title' ); ?>">
					<?php esc_html_e( 'Sub Title', 'sky-game' ); ?>
				</label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'sub_title' ); ?>" name="<?php echo $this->get_field_name( 'sub_title' ); ?>" type="text" value="<?php echo $sub_title; ?>" />
			</p>

			<p>
				<label for="<?php echo $this->get_field_id( 'number' ); ?>">
					<?php esc_html_e( 'Number of posts to show:', 'sky-game' ); ?>
				</label>
				<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" />
			</p>

			<p>
				<input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
				<label for="<?php echo $this->get_field_id( 'show_date' ); ?>">
					<?php esc_html_e( 'Display post date?', 'sky-game' ); ?>
				</label>
			</p>
		<?php
	}
}

register_widget('Sky_Recent_News');
