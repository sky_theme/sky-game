<?php                       
     
class Sky_Rank_Alexa_Widget extends WP_Widget {
	     
	public function __construct() {     
		    
		parent::__construct(
			'alexa_traffic_rank_widget',
			esc_html__( 'Sky Alexa Traffic Rank', 'sky-game' ),
			array( 'description' => esc_html__( 'Monitor your alexa ranking!', 'sky-game' ), )
		);
		
	}          
	 
	public function form( $instance ) {  

		$defaults = array( 
			'title' => esc_html__('Alexa Traffic Rank', 'sky-game'), 
			'website_url' => 'http://skygame.mobi/', 
			'layout' => 'horizontal' 
		);
		
        $instance = wp_parse_args( (array) $instance, $defaults ); 

        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title','sky-game' ); ?>:</label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
                   name="<?php echo $this->get_field_name( 'title' ); ?>" type="text"
                   value="<?php echo esc_attr( $instance['title'] ); ?>">
        </p>
        
        <p>
            <label
                for="<?php echo $this->get_field_id( 'website_url' ); ?>"><?php esc_html_e( 'Website/Blog URL','sky-game'); ?>:</label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'website_url' ); ?>"
                   name="<?php echo $this->get_field_name( 'website_url' ); ?>" type="text"
                   value="<?php echo esc_attr( $instance['website_url'] ); ?>">
        </p>
		
		<p>
			<label for="<?php echo $this->get_field_id('layout'); ?>"><?php esc_html_e('Layout','sky-game'); ?>:</label> 
			<select id="<?php echo $this->get_field_id('layout'); ?>" name="<?php echo $this->get_field_name('layout'); ?>" style="width:100%;">
				<option <?php if ('horizontal' == $instance['layout']) echo 'selected="selected"'; ?>>Horizontal</option>
				<option <?php if ('vertical' == $instance['layout']) echo 'selected="selected"'; ?>>Vertical</option>
			</select>
		</p>			

    <?php
    }



public function update( $new_instance, $old_instance ) {
		$instance                = $old_instance;
		$instance['title']       = strip_tags( $new_instance['title'] );
		$instance['layout']      = strip_tags( strtolower($new_instance['layout']));
		$instance['website_url'] = strip_tags( $new_instance['website_url'] );
        return $instance;
    }


public function widget( $args, $instance ) {
		$title       = apply_filters( 'widget_title', $instance['title'] );
		$layout      = $instance['layout'];
		$website_url = $instance['website_url'];
		
        echo $args['before_widget'];
		
        if ( ! empty( $title ) ) {
        	
            echo $args['before_title'] . $title . $args['after_title'];
			
        }
		
		$html='';
		
		if($layout=="horizontal"):	
		    
		    $html .= '<a href="http://www.alexa.com/siteinfo/'.$website_url.'"><script src="http://xslt.alexa.com/site_stats/js/s/a?url='.$website_url.'" type="text/javascript" charset="utf-8" async defer></script></a>';
			
			echo $html;
		
		endif;
		
		if($layout=="vertical"):
		
			$html .= '<a href="http://www.alexa.com/siteinfo/'.$website_url.'"><script src="http://xslt.alexa.com/site_stats/js/s/b?url='.$website_url.'" type="text/javascript" charset="utf-8" async defer></script></a>';
			
			echo $html;
		
		endif;
		
        echo $args['after_widget'];
    }

}

register_widget( 'Sky_Rank_Alexa_Widget' );
