<?php 

class Sky_MailChimp extends WP_Widget {

	/**
	 * Constructor
	 */
	
	public function __construct() {
		parent::__construct (
			'sky_mailchimp',
			esc_html__('Sky MailChimp','sky-game'),
			array ( 'classname' => 'mailchimp-widget', 'description' => __ ( 'Display simple MailChimp subscribe form.', 'sky-game' ) )
		);

		// if ( $this->cached ) {
			add_action( 'save_post', array( $this, 'flush_widget_cache' ) );
			add_action( 'deleted_post', array( $this, 'flush_widget_cache' ) );
			add_action( 'switch_theme', array( $this, 'flush_widget_cache' ) );
		// }
	}

	public function widget($args, $instance) {
		extract( $args );
		$title = apply_filters( 
			'widget_title', 
			empty( $instance['title'] ) ? '' : $instance['title'], 
			$instance, $this->id_base 
		);

		echo $args['before_widget'];
		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
		
		if ( $instance['sub_title'] ) : ?>

			<div class="sub_title"><?php echo $instance['sub_title']; ?></div>

		<?php endif;

		$mail_list
		?>
		
		<form class="mc-subscribe-form<?php echo (isset($_COOKIE['sky_subscribed']) ? ' submited':'')?>">
			<?php if( isset($_COOKIE['sky_subscribed']) ) : ?>
			<label class="sky-message alert" role="alert"><?php _e( 'You\'ve already subscribed.', 'sky-game' ); ?></label>
			<?php else: ?> 
			<label for="email"><?php echo esc_attr( $instance['subscribe_text'] ); ?></label>
			
			<div class="main_mail">
				<input type="email" id="email" name="mc_email" class="mc-email" value="" placeholder="<?php _e( 'Email', 'sky-game' ); ?>" />
				<button><i class="fa fa-long-arrow-right">&nbsp;</i></button>
			</div>
			<?php if ( isset($instance['enable_social']) ) : ?>
				<div class="main_social">
					<?php if ( !empty( $instance['facebook'] ) ) : ?>
						<a href="<?php echo $instance['facebook']; ?>" class="tooltip" title="<?php esc_html_e( 'Facebook', 'sky-game' ); ?>">
							<i class="fa fa-facebook"></i>
						</a>
					<?php endif; ?>
					<?php if ( !empty( $instance['twitter'] ) ) : ?>
						<a href="<?php echo $instance['twitter']; ?>" class="tooltip" title="<?php esc_html_e( 'Twitter', 'sky-game' ); ?>">
							<i class="fa fa-twitter"></i>
						</a>
					<?php endif; ?>
					<?php if ( !empty( $instance['linkedin'] ) ) : ?>
						<a href="<?php echo $instance['linkedin']; ?>" class="tooltip" title="<?php esc_html_e( 'Linkedin', 'sky-game' ); ?>">
							<i class="fa fa-linkedin"></i>
						</a>
					<?php endif; ?>
					<?php if ( !empty( $instance['youtube'] ) ) : ?>
						<a href="<?php echo $instance['youtube']; ?>" class="tooltip" title="<?php esc_html_e( 'Youtube', 'sky-game' ); ?>">
							<i class="fa fa-youtube"></i>
						</a>
					<?php endif; ?>
				</div>
			<?php endif; ?>

			<input type="hidden" name="mc_list_id" value="<?php echo esc_attr( $instance['mail_list'] ); ?>" />
			<input type="hidden" name="action" value="sky_mc_subscribe" />
			<?php wp_nonce_field('sky-subscribe','nonce'); ?>
			<?php endif; ?>
		</form>
		<?php
		

		echo $args['after_widget'];
	}

	/**
	 * form function.
	 *
	 * @see WP_Widget->form
	 * @param array $instance
	 */

	public function form( $instance ) {
		$defaults = array( 
			'title'          => esc_html__( 'Newsletter', 'sky-game' ),
			'sub_title'      => esc_html__( 'Sign up to our mailing list', 'sky-game' ),
			'subscribe_text' => esc_html__( 'Get notified when new themes or new versions released', 'sky-game' ),
			'mail_list'      => '',
			'enable_social'  => false,
			'facebook'       => '',
			'twitter'        => '',
			'linkedin'       => '',
			'youtube'        => '',
		);
		$instance = wp_parse_args( (array) $instance, $defaults );
		global $sky_mailchimp;
		$api_key = sky_get_customize_option( 'mailchimp_api' );
		$mail_list = !empty( $api_key ) ? $sky_mailchimp->get_mail_lists( $api_key ) : '';

		?>
		<p>
			<label><?php esc_html_e( 'Title', 'sky-game' ) ?></label>
			<input type="text" name="<?php echo $this->get_field_name( 'title' ) ?>" id="<?php echo $this->get_field_id( 'title' ) ?>" value="<?php
			 echo esc_attr( $instance['title'] ) ?>" class="widefat" />
		</p>
		<p>
			<label><?php esc_html_e( 'Sub Title', 'sky-game' ) ?></label>
			<input type="text" name="<?php echo $this->get_field_name( 'sub_title' ) ?>" id="<?php echo $this->get_field_id( 'sub_title' ) ?>" value="<?php
			 echo esc_attr( $instance['sub_title'] ) ?>" class="widefat" />
		</p>
		<p>
			<label><?php esc_html_e( 'Subscribe Text', 'sky-game' ) ?></label>
			<input type="text" name="<?php echo $this->get_field_name( 'subscribe_text' ) ?>" id="<?php echo $this->get_field_id( 'subscribe_text' ) ?>" value="<?php
			 echo esc_attr( $instance['subscribe_text'] ) ?>" class="widefat" />
		</p>
		<?php if(!empty($mail_list)) : ?>
		<p>
			<label><?php esc_html_e( 'Subscribe Mail List', 'sky-game' ) ?></label>
			<select name="<?php echo $this->get_field_name( 'mail_list' ) ?>" id="<?php echo $this->get_field_id( 'mail_list' ) ?>" class="widefat" >';
			<?php foreach($mail_list as $id => $list_name) : ?>
				<option value="<?php echo $id ?>" <?php selected( $instance['mail_list'], $id ) ?>><?php echo $list_name ?></option>
			<?php endforeach; ?>
			</select>
		</p>
		<?php else : ?>
			<p><?php esc_html_e( 'There\'s problem get your mail list, please check your MailChimp API Key settings in Customizer', 'sky-game' ) ?></p>
		<?php endif; ?>
		<script type="text/javascript">
		jQuery(document).ready(function($) {
			<?php $social_id = uniqid(); ?>
			$('#<?php echo $this->get_field_id( 'enable_social' ) ?>_<?php echo $social_id; ?>').change(function(event) {
				if($(this).prop('checked')) {
					$('.enable_social_<?php echo $social_id; ?>').show();
				} else {
					$('.enable_social_<?php echo $social_id; ?>').hide();
				}
			}).change();
		});
		</script>
		<p>
			<input type="checkbox" <?php checked( $instance['enable_social'], true); ?>  name="<?php echo $this->get_field_name( 'enable_social' ) ?>" id="<?php echo $this->get_field_id( 'enable_social' ) ?>_<?php echo $social_id; ?>" value="1" />
			<label for="<?php echo $this->get_field_id( 'enable_social' ) ?>_<?php echo $social_id; ?>"><?php esc_html_e( 'Enable Social', 'sky-game' ) ?></label>
		</p>
		<p class="enable_social_<?php echo $social_id; ?>">
			<label for="<?php echo $this->get_field_id( 'facebook' ) ?>"><?php esc_html_e( 'Facebook', 'sky-game' ) ?></label>
			<input type="text" name="<?php echo $this->get_field_name( 'facebook' ) ?>" id="<?php echo $this->get_field_id( 'facebook' ) ?>" value="<?php
			 echo $instance['facebook'] ?>" class="widefat" />
		</p>
		<p class="enable_social_<?php echo $social_id; ?>">
			<label for="<?php echo $this->get_field_id( 'twitter' ) ?>"><?php esc_html_e( 'Twitter', 'sky-game' ) ?></label>
			<input type="text" name="<?php echo $this->get_field_name( 'twitter' ) ?>" id="<?php echo $this->get_field_id( 'twitter' ) ?>" value="<?php
			 echo esc_attr( $instance['twitter'] ) ?>" class="widefat" />
		</p>
		<p class="enable_social_<?php echo $social_id; ?>">
			<label for="<?php echo $this->get_field_id( 'linkedin' ) ?>"><?php esc_html_e( 'Linkedin', 'sky-game' ) ?></label>
			<input type="text" name="<?php echo $this->get_field_name( 'linkedin' ) ?>" id="<?php echo $this->get_field_id( 'linkedin' ) ?>" value="<?php
			 echo esc_attr( $instance['linkedin'] ) ?>" class="widefat" />
		</p>
		<p class="enable_social_<?php echo $social_id; ?>">
			<label for="<?php echo $this->get_field_id( 'youtube' ) ?>"><?php esc_html_e( 'Youtube', 'sky-game' ) ?></label>
			<input type="text" name="<?php echo $this->get_field_name( 'youtube' ) ?>" id="<?php echo $this->get_field_id( 'youtube' ) ?>" value="<?php
			 echo esc_attr( $instance['youtube'] ) ?>" class="widefat" />
		</p>
		<?php
	}

	/**
	 * update function.
	 *
	 * @see WP_Widget->update
	 * @param array $new_instance
	 * @param array $old_instance
	 * @return array
	 */

	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;

		$instance['title']          = strip_tags($new_instance['title']);
		$instance['sub_title']      = strip_tags($new_instance['sub_title']);
		$instance['subscribe_text'] = strip_tags($new_instance['subscribe_text']);
		$instance['mail_list']      = strip_tags($new_instance['mail_list']);
		$instance['enable_social']  = strip_tags($new_instance['enable_social']);
		$instance['facebook']       = strip_tags($new_instance['facebook']);
		$instance['twitter']        = strip_tags($new_instance['twitter']);
		$instance['linkedin']       = strip_tags($new_instance['linkedin']);
		$instance['youtube']        = strip_tags($new_instance['youtube']);

		return $instance;
	}

	/**
	 * Cache the widget
	 * @param string $content
	 */
	public function cache_widget( $args, $content ) {
		$cache[$args['widget_id']] = $content;
		
		wp_cache_set( apply_filters( 'sky_mailchimp', $this->widget_id ), $cache, 'widget' );
	}

	/**
	 * Flush the cache
	 *
	 * @return void
	 */
	public function flush_widget_cache() {
		//wp_cache_delete( apply_filters( 'sky_mailchimp', $this->widget_id ), 'widget' );
	}
}

function sky_register_widget() {
	if( sky_get_customize_option('enable_mailchimp') ) {
		$subscribe_mail_list = sky_get_customize_option( 'mailchimp_api' );
		if( ! empty( $subscribe_mail_list ) ) {
			register_widget( 'Sky_MailChimp' );
		}
	}
}
add_action( 'widgets_init', 'sky_register_widget' );
