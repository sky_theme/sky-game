<?php

class Sky_Box_Like_Facebook extends WP_Widget {
	public function __construct() {
		parent::__construct (
			'sky_box_like_facebook',
			esc_html__('Sky Box Like Facebook','sky-game')
		);
	}
	public function widget($args, $instance) {
		$title = apply_filters( 
			'widget_title', 
			empty( $instance['title'] ) ? '' : $instance['title'], 
			$instance, $this->id_base 
		);
		if ($instance['hide_mobile']) echo '<div class="visible-lg visible-md visible-sm">';
		echo $args['before_widget'];
		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
		
		// === Add code
		?>
			<div class="sky-box-like">

				<div 
					class="fb-page<?php if ($instance['hide_mobile']) echo ' visible-lg visible-md'; ?>" 
					data-width="500"
					data-href="https://www.facebook.com/<?php echo $instance['url'] ?>" 
					data-small-header="<?php echo ($instance['small_header'] ? 'true' : 'false') ?>" 
					data-adapt-container-width="true" 
					data-hide-cover="<?php echo ($instance['hide_conver_photo'] ? 'true' : 'false') ?>" 
					data-show-facepile="<?php echo ($instance['friend_face'] ? 'true' : 'false') ?>" 
					data-show-posts="<?php echo ($instance['show_page_post'] ? 'true' : 'false') ?>">
					<div class="fb-xfbml-parse-ignore">
						<blockquote cite="https://www.facebook.com/<?php echo $instance['url'] ?>">
							<a href="https://www.facebook.com/<?php echo $instance['url'] ?>">
								<?php echo $instance['title_fanpage'] ?>
							</a>
						</blockquote>
					</div>
				</div>

			</div>

		<?php

		echo $args['after_widget'];
		if ($instance['hide_mobile']) echo '</div>';
	}

	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 
			'title'             => '',
			'title_fanpage'     => '',
			'url'               => '',
			'friend_face'       => '',
			'show_page_post'    => '',
			'small_header'      => '',
			'hide_conver_photo' => '',
			'hide_mobile' 		=> '',
		) );
		$title             = $instance['title'];
		$title_fanpage     = $instance['title_fanpage'];
		$url               = $instance['url'];
		$friend_face       = $instance['friend_face'];
		$show_page_post    = $instance['show_page_post'];
		$small_header      = $instance['small_header'];
		$hide_conver_photo = $instance['hide_conver_photo'];
		$hide_mobile 	   = $instance['hide_mobile'];
		?>
			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>">
					<?php esc_html_e('Title','sky-game'); ?> 
					<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
				</label>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('title_fanpage'); ?>">
					<?php esc_html_e('Title Fanpage','sky-game'); ?> 
					<input class="widefat" id="<?php echo $this->get_field_id('title_fanpage'); ?>" name="<?php echo $this->get_field_name('title_fanpage'); ?>" type="text" value="<?php echo esc_attr($title_fanpage); ?>" placeholder="<?php esc_html_e( 'Hack Game No.1', 'sky-game' ); ?>" />
				</label>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('url'); ?>">
					<?php esc_html_e('Url Fanpage','sky-game'); ?> 
					<input class="widefat" id="<?php echo $this->get_field_id('url'); ?>" name="<?php echo $this->get_field_name('url'); ?>" type="text" value="<?php echo esc_attr($url); ?>"  placeholder="<?php esc_html_e( 'hackgame.mienphi.no1', 'sky-game' ); ?>" />
				</label>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('friend_face'); ?>">
					<input class="widefat" id="<?php echo $this->get_field_id('friend_face'); ?>" name="<?php echo $this->get_field_name('friend_face'); ?>" type="checkbox" value="1" <?php checked( $friend_face, 1 ); ?> />
					<?php esc_html_e('Show Friend\'s Faces','sky-game'); ?> 
				</label>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('show_page_post'); ?>">
					<input class="widefat" id="<?php echo $this->get_field_id('show_page_post'); ?>" name="<?php echo $this->get_field_name('show_page_post'); ?>" type="checkbox" value="1" <?php checked( $show_page_post, 1 ); ?> />
					<?php esc_html_e('Show Page Posts','sky-game'); ?> 
				</label>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('small_header'); ?>">
					<input class="widefat" id="<?php echo $this->get_field_id('small_header'); ?>" name="<?php echo $this->get_field_name('small_header'); ?>" type="checkbox" value="1" <?php checked( $small_header, 1 ); ?> />
					<?php esc_html_e('Use Small Header','sky-game'); ?> 
				</label>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('hide_conver_photo'); ?>">
					<input class="widefat" id="<?php echo $this->get_field_id('hide_conver_photo'); ?>" name="<?php echo $this->get_field_name('hide_conver_photo'); ?>" type="checkbox" value="1" <?php checked( $hide_conver_photo, 1 ); ?> />
					<?php esc_html_e('Hide Cover Photo','sky-game'); ?> 
				</label>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('hide_mobile'); ?>">
					<input class="widefat" id="<?php echo $this->get_field_id('hide_mobile'); ?>" name="<?php echo $this->get_field_name('hide_mobile'); ?>" type="checkbox" value="1" <?php checked( $hide_mobile, 1 ); ?> />
					<?php esc_html_e('Hide Mobile','sky-game'); ?> 
				</label>
			</p>
			<p>
        	 	<label><?php esc_html_e( 'Shortcode', 'sky-game' ); ?>:</label><br />
 			 	<code>
 			 		[sky-boxlike url=" https://www.facebook.com/hackgame.mienphi.no1" width="340" height="500" show_faces="true" show_header="true" show_border="false"]
 			 	</code>
		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$new_instance = wp_parse_args((array) $new_instance, array( 
			'title'             => '',
			'title_fanpage'     => '',
			'url'               => '',
			'friend_face'       => '',
			'show_page_post'    => '',
			'small_header'      => '',
			'hide_conver_photo' => '',
			'hide_mobile' 		=> '',
		));
		$instance['title']             = strip_tags($new_instance['title']);
		$instance['title_fanpage']     = strip_tags($new_instance['title_fanpage']);
		$instance['url']               = strip_tags($new_instance['url']);
		$instance['friend_face']       = strip_tags($new_instance['friend_face']);
		$instance['show_page_post']    = strip_tags($new_instance['show_page_post']);
		$instance['small_header']      = strip_tags($new_instance['small_header']);
		$instance['hide_conver_photo'] = strip_tags($new_instance['hide_conver_photo']);
		$instance['hide_mobile'] 	   = strip_tags($new_instance['hide_mobile']);
		return $instance;
	}
}
register_widget( 'Sky_Box_Like_Facebook' );
