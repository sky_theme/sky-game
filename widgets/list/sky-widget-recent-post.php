<?php
class Sky_Widget_Recent_Posts extends WP_Widget {

	public function __construct() {
		$widget_ops = array( 'classname' => 'widget_sky_recent_post', 'description' => esc_html__( "Display list post.",'sky-game' ) );
		parent::__construct('sky_recent_posts', esc_html__('Sky Recent Post','sky-game'), $widget_ops);
	}

	public function widget( $args, $instance ) {
		global $wp_query;

		$title = apply_filters( 
			'widget_title', 
			empty( $instance['title'] ) ? esc_html__( 'Title','sky-game' ) : $instance['title'], 
			$instance, 
			$this->id_base 
		);
		
		echo $args['before_widget'];
		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		// --
			$number = $instance['number'];
			$game_category = $instance['game_category'];
			$show_thumb = $instance['show_thumb'];
			$show_readmore = $instance['show_readmore'];
			$orderby = $instance['orderby'];

		// ===
			if( is_front_page() || is_home()) {
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : ( ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1 );
			} else {
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			}

		// -- array 
			$sky_args = array(
				'paged'			  	  => $paged,
				'posts_per_page'      => $number,
				'post_status'         => 'publish',
				'post_type'			  => 'sky-game',
			);

		if ( $game_category != 'all' ) :

			$args['tax_query'] = array('relation' => 'AND');
			if( !empty($game_category) ) :
				$sky_args['tax_query'][] = array(
					'taxonomy'     => sky_get_option_setting( 'sky_general', 'category_game_slug', 'sky-game-cat' ),
					'field'        => 'slug',
					'terms'        => $game_category
				);
			endif; // === << empty($game_category) >>

		endif; // === << game_category >>

		if( $orderby == 'latest' ) :

			$sky_args['orderby'] = 'modified';
			$sky_args['order'] = 'DESC';

		elseif( $orderby == 'oldest' ) :

			$sky_args['orderby'] = 'modified';
			$sky_args['order'] = 'ASC';

		elseif( $orderby == 'alphabet' ) :

			$sky_args['orderby'] = 'title';
			$sky_args['order'] = 'ASC';	

		elseif( $orderby == 'ralphabet' ) :

			$sky_args['orderby'] = 'title';
			$sky_args['order'] = 'DESC';

		elseif( $orderby == 'random' ) :

			$sky_args['orderby'] = 'rand';

		endif; // --- /order

		$wp_query             = NULL;
		$wp_query             = new WP_Query( $sky_args );

        if ( !$show_thumb ) echo "<ul>";
        else echo '<div class="sky_recent_post_container">';
        
	        while ( $wp_query->have_posts() ) :
	        	$wp_query->the_post();

	        	// --
		        	$url = get_permalink( );
		        	$title = get_the_title();

				if ( $show_thumb ) :
					
					?>
						<div class="sky-item">
							
							<div class="sky-thumb">
								
								<a href="<?php the_permalink(); ?>" title="<?php the_title( ); ?>">

									<img src="<?php echo sky_get_thumb( ); ?>" alt="<?php the_title( ); ?>" />

								</a>

							</div>

							<div class="sky-entry">
								
								<a href="<?php the_permalink(); ?>" title="<?php the_title( ); ?>">
									<h4>
										<?php the_title( ); ?>
									</h4>
								</a>

								<p>
									<?php esc_html_e( 'Hỗ trợ', 'sky-game' ); ?>&nbsp;
									<?php sky_get_list_support_os(); ?>
								</p>

								<a target="_blank" href="<?php echo sky_get_url_download() ?>" title="<?php esc_html_e( 'Tải về', 'sky-game' ); ?>">
									<span class="sky-btn-download">
										<i class="fa fa-download"></i>&nbsp; <?php esc_html_e( 'Tải về', 'sky-game' ); ?>
									</span>
								</a>

							</div>

						</div>
					<?php
				
				else :

					echo "<li>";
					echo "<a href='{$url}' title='{$title}'>{$title}</a>";
					echo "</li>";

				endif;
			
			endwhile;
			if ( !$show_thumb ) echo "</ul>";
			else echo '</div>';

			if ( $show_readmore ) :

				$tax_cat = get_terms( sky_get_option_setting( 'sky_general', 'category_game_slug', 'sky-game-cat' ) );
				foreach ( $tax_cat as $cat ) {

					if ( $cat->slug == $game_category ) :
						// The $term is an object, so we don't need to specify the $taxonomy.
						    $cat_link = get_term_link( $cat );
						   
						// If there was an error, continue to the next term.
						    if ( is_wp_error( $cat_link ) ) {
						        continue;
						    }

						echo "<div class=\"sky-readmore\">";
						echo "	<a target='_blank' href='{$cat_link}' title='{$cat->name}'>" . esc_html__( 'Read more', 'sky-game' ) . "</a>";
						echo "</div>";
					endif;
				}
				

			endif;

		// -- Restore original Post Data
			wp_reset_postdata();


		echo $args['after_widget'];
	}

	public function update( $new_instance, $old_instance ) {
		$instance                  = $old_instance;
		$instance['title']         = strip_tags($new_instance['title']);
		$instance['game_category'] = strip_tags($new_instance['game_category']);
		$instance['number']        = strip_tags($new_instance['number']);
		$instance['show_readmore'] = strip_tags($new_instance['show_readmore']);
		$instance['show_thumb']    = strip_tags($new_instance['show_thumb']);
		$instance['orderby']       = strip_tags($new_instance['orderby']);

		return $instance;
	}

	public function form( $instance ) {
		// -- Defaults
			$instance = wp_parse_args( 
				(array) $instance, 
				array( 
					'title'         => '',
					'game_category' => '',
					'number'        => '',
					'show_readmore' => '',
					'show_thumb'    => '',
					'orderby'       => '',
				) 
			);
		// -- Get var
			$title         = esc_attr( $instance['title'] );
			$game_category = esc_attr( $instance['game_category'] );
			$number        = esc_attr( $instance['number'] );
			$show_readmore = esc_attr( $instance['show_readmore'] );
			$show_thumb    = esc_attr( $instance['show_thumb'] );
			$orderby       = esc_attr( $instance['orderby'] );

		?>
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title:','sky-game' ); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
			</p>

			<!-- <p>
				<label for="<?php echo $this->get_field_id( 'game_category' ); ?>"><?php esc_html_e( 's:','sky-game' ); ?></label>
				<select id="<?php echo $this->get_field_id( 'game_category' ); ?>" name="<?php echo $this->get_field_name( 'game_category' ); ?>" class="widefat">
					
					<option value="list"<?php selected( $game_category, 'list' ); ?>><?php esc_html_e( 'List', 'sky-game' ); ?></option>
					<option value="sky-game"<?php selected( $game_category, 'sky-game' ); ?>><?php esc_html_e( 'Sky Game', 'sky-game' ); ?></option>
					
				</select>
			</p> -->

			<p>
				<label for="<?php echo $this->get_field_id( 'game_category' ); ?>"><?php esc_html_e( 'Category Game','sky-game' ); ?></label>
				<select id="<?php echo $this->get_field_id( 'game_category' ); ?>" name="<?php echo $this->get_field_name( 'game_category' ); ?>" class="widefat">
					<option value="all"><?php esc_html_e( 'All Category', 'sky-game' ); ?></option>
					<?php
						// === << get list category
						
						foreach ((array) get_terms(sky_get_option_setting( 'sky_general', 'category_game_slug', 'sky-game-cat' ), array('hide_empty'=>0)) as $category){
							?>
								<option value="<?php echo $category->slug ?>"<?php selected( $game_category,  $category->slug ); ?>><?php echo esc_html($category->name) ?></option>
							<?php
						}
					?>
				</select>
			</p>

			<p>
				<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php esc_html_e( 'Number of posts to show:','sky-game' ); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="number" value="<?php echo empty($number) ? 10 : $number; ?>" />
			</p>

			<p>
				<label for="<?php echo $this->get_field_id( 'orderby' ); ?>"><?php esc_html_e( 'Orderby','sky-game' ); ?></label>
				<select id="<?php echo $this->get_field_id( 'orderby' ); ?>" name="<?php echo $this->get_field_name( 'orderby' ); ?>" class="widefat">
					
					<option value="latest"<?php selected( $orderby, 'latest' ); ?>><?php esc_html_e( 'Recent First', 'sky-game' ); ?></option>
					<option value="oldest"<?php selected( $orderby, 'oldest' ); ?>><?php esc_html_e( 'Older First', 'sky-game' ); ?></option>
					<option value="alphabet"<?php selected( $orderby, 'alphabet' ); ?>><?php esc_html_e( 'Title Alphabet', 'sky-game' ); ?></option>
					<option value="ralphabet"<?php selected( $orderby, 'ralphabet' ); ?>><?php esc_html_e( 'Title Reversed Alphabet', 'sky-game' ); ?></option>
					<option value="random"<?php selected( $orderby, 'random' ); ?>><?php esc_html_e( 'Random', 'sky-game' ); ?></option>
					
				</select>
			</p>

			<p>
				<input id="<?php echo $this->get_field_id( 'show_thumb' ); ?>" name="<?php echo $this->get_field_name( 'show_thumb' ); ?>" type="checkbox" value="true" <?php checked( $show_thumb, 'true' ); ?> />
				<label for="<?php echo $this->get_field_id( 'show_thumb' ); ?>"><?php esc_html_e( 'Show thubmnail','sky-game' ); ?></label>
			</p>

			<p>
				<input id="<?php echo $this->get_field_id( 'show_readmore' ); ?>" name="<?php echo $this->get_field_name( 'show_readmore' ); ?>" type="checkbox" value="true" <?php checked( $show_readmore, 'true' ); ?> />
				<label for="<?php echo $this->get_field_id( 'show_readmore' ); ?>"><?php esc_html_e( 'Show Read More','sky-game' ); ?></label>
			</p>

		<?php
	}
}

register_widget('Sky_Widget_Recent_Posts');
