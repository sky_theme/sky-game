<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Sky Game
 */
?>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
	
</div>
		</div><!-- #sky-main-page -->
		<?php if( sky_get_customize_option('number_footer_column') != 'none' ) : ?>
			<div id="footer-widget" class="sky-container-fluid<?php if( sky_get_customize_option('background_column_footer_column_on_mobile') ) : ?> visible-lg visible-md visible-sm<?php endif; ?>">
				<div id="footer-widget-content" class="sky-container">
					<?php $num = sky_get_customize_option('number_footer_column'); ?>
					<div class="row">
						<?php

						$i = 0; while ( $i < $num ) : $i ++;
						switch ( $num ) {
							case 4 : $class = 'sky-md-3 sky-sm-6';  break;
							case 3 : $class = 'sky-md-4 sky-sm-6';  break;
							case 2 : $class = 'sky-md-6 sky-sm-12';  break;
							case 1 : $class = 'sky-md-12'; break;
						}
						echo '<div class="' . $class . '">';
						dynamic_sidebar( 'sky-footer-' . $i );
						echo '</div>';
						endwhile;

						?>
					</div> <!-- /.row -->
				</div> <!-- /#footer-widget-content -->
			</div> <!-- /#footer-widget -->
		<?php endif; ?>
		<footer id="sky-site-info">
			
			<div id="footer-content" class="sky-container">
				
				<div class="row">
					<?php if ( 
						sky_get_customize_option('sky_footer_social_fb') != '' ||
						sky_get_customize_option('sky_footer_social_gg') != '' ||
						sky_get_customize_option('sky_footer_social_tw') != '' ||
						sky_get_customize_option('sky_footer_social_in') != '' ||
						sky_get_customize_option('sky_footer_social_yt') != ''
					) : $social = true; ?>
						<div class="sky-xs-12 sky-sm-6 text-social">
							<ul>
								<?php if ( sky_get_customize_option('sky_footer_social_fb') != '' ) : ?>
									<li>
										<a class="tooltip" href="<?php echo sky_get_customize_option('sky_footer_social_fb'); ?>" title="<?php esc_html_e( 'Facebook', 'sky-game' ); ?>">
											<i class="fa fa-facebook"></i>
										</a>
									</li>
								<?php endif; ?>
								<?php if ( sky_get_customize_option('sky_footer_social_gg') != '' ) : ?>
									<li>
										<a class="tooltip" href="<?php echo sky_get_customize_option('sky_footer_social_gg'); ?>" title="<?php esc_html_e( 'Google', 'sky-game' ); ?>">
											<i class="fa fa-google-plus"></i>
										</a>
									</li>
								<?php endif; ?>
								<?php if ( sky_get_customize_option('sky_footer_social_tw') != '' ) : ?>
									<li>
										<a class="tooltip" href="<?php echo sky_get_customize_option('sky_footer_social_tw'); ?>" title="<?php esc_html_e( 'Twitter', 'sky-game' ); ?>">
											<i class="fa fa-twitter"></i>
										</a>
									</li>
								<?php endif; ?>
								<?php if ( sky_get_customize_option('sky_footer_social_in') != '' ) : ?>
									<li>
										<a class="tooltip" href="<?php echo sky_get_customize_option('sky_footer_social_in'); ?>" title="<?php esc_html_e( 'Linkedin', 'sky-game' ); ?>">
											<i class="fa fa-linkedin"></i>
										</a>
									</li>
								<?php endif; ?>
								<?php if ( sky_get_customize_option('sky_footer_social_yt') != '' ) : ?>
									<li>
										<a class="tooltip" href="<?php echo sky_get_customize_option('sky_footer_social_yt'); ?>" title="<?php esc_html_e( 'Youtube', 'sky-game' ); ?>">
											<i class="fa fa-youtube-play"></i>
										</a>
									</li>
								<?php endif; ?>
							</ul>
						</div>
					<?php endif; ?>

					<?php if ( sky_get_customize_option('sky_copyright') != '' ) : ?>
						<div class="sky-xs-12 sky-sm-<?php echo ( $social ? '6' : '12' ) ?> text-left text-copyright">
							<?php echo sky_get_customize_option('sky_copyright'); ?>
						</div>
					<?php endif; ?>
				</div> <!-- /.row -->

			</div> <!-- /#footer-content -->

		</footer> <!-- /#sky-site-info -->
		<?php wp_footer(); ?>

	</body>
</html>
