<?php
/*
 * Heading Layouts
 */
?>
<div class="sky-section">
	<div class="sky-navbar">
		<div class="navbar-container sky-container">
			<div id="sky-logo">
				<a class="logo" href="<?php echo bloginfo( 'url' ); ?>" title="<?php echo bloginfo( 'name' ); ?>">
					<?php if ( sky_get_customize_option( 'using_logo_image' ) ) : ?>
						<?php $image_logo = wp_get_attachment_image_src( sky_get_customize_option( 'image_logo' ), 'full' ); ?>
						<img src="<?php echo $image_logo[0]; ?>" alt="<?php echo bloginfo( 'name' ); ?>" />
					<?php else : ?>
						<?php echo sky_get_customize_option( 'text_logo' ); ?>
					<?php endif; ?>
				</a>
			</div><!-- /#sky-logo -->
			<div id="sky-search">
				<form method="get" id="form-header" class = "search-input" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <input type="search" class="search_ajax_value" value="" name="s"  autocomplete="off" placeholder="<?php esc_attr_e( 'Enter keyword to search...', 'sky-game' ); ?>">
                        <input type="submit" value="<?php esc_attr_e( 'Search', 'sky-game' ); ?>" /> 
                        <img class="ajax_load" src="<?php echo esc_url( SKY_THEME_ASSETS_URI . '/images/ajax-loader.gif' ); ?>" alt="spin" />
                        <div class="returs_ajax"></div>
                </form>
				<?php //get_search_form(); ?>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="sky-nav-line"></div>
	</div><!-- /.sky-nav -->
</div><!-- /.sky-section -->		
		<div class = "nav-button clearfix <?php echo sky_get_customize_option( 'nav_style', 'static' );?>">
			<a class="logo" href="<?php echo bloginfo( 'url' ); ?>" title="Demo">
				<?php if ( sky_get_customize_option( 'using_logo_image' ) ) : ?>
					<?php $image_logo_mobile = wp_get_attachment_image_src( sky_get_customize_option( 'image_logo_mobile' ), 'full' ); ?>
					<img src="<?php echo $image_logo_mobile[0]; ?>" alt="<?php echo bloginfo( 'name' ); ?>" />
				<?php else : ?>
					<?php echo sky_get_customize_option( 'text_logo' ); ?>
				<?php endif; ?>
			</a>
			<button type="button" class="navbar-toggle collapsed res-button" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
        </div>
        <nav id="site-navigation" class="sky-nav-menu <?php echo sky_get_customize_option( 'nav_style', 'static' );?>">   
			<div class="sky-container navbar-collapse collapse">
				<?php
					wp_nav_menu( array(

						'theme_location' => 'sky-nav-bar',
						'container'      => '',
						'menu_id' 		 => 'sky-nav-bar',
						'menu_class'	 => 'nav navbar-nav sf-menu',
						'before'         => '',
						'fallback_cb'    => 'sky_notice_set_menu',
						'walker'         => new sky_megamenu_walker
						)
					); 
				?>
			</div>
		</nav>
		<div style = "clear:both"></div>
		<div id="sky-search-mobile">
			<?php get_search_form(); ?>
		</div>

<div id="sky-main-page">