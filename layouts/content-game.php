<?php
/**
 * Template part for displaying game posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Sky Game
 */

?>

<article id="post-<?php the_ID(); ?>" class="single-game sky-xs-12 sky-md-8">
	<?php sky_breadcrumb(); ?>
	<header class="entry-header">

		<?php sky_single_box_info(); ?>
		
	</header><!-- .entry-header -->

	<?php if ( !sky_get_post_meta( get_the_ID(), 'sky_hide_title' ) ) : ?>
		
		<h1 id="entry-title">
			<?php the_title(); ?>
		</h1><!-- #entry-title -->

	<?php endif; ?>

	<div class="entry-content">

		<?php if ( !empty(sky_get_post_meta( get_the_ID(), 'sky_description' ) ) ) : ?>

			<?php echo "<h2 id=\"entry-description\">" . sky_get_post_meta( get_the_ID(), 'sky_description' ) . "</h2>"; ?>

		<?php endif; ?>

		<?php if ( !sky_get_post_meta( get_the_ID(), 'sky_hide_related' ) ) : ?>	
			
			<?php sky_get_related_posts(); ?>

		<?php endif; ?>

		<?php if ( sky_get_post_meta( get_the_ID(), 'sky_new' ) ) : ?>

			<div id="entry-new">
			
				<div id="entry-new-title"><?php esc_html_e( 'Có gì mới?', 'sky-game' ); ?></div>
				<ul class="item">
					<?php foreach ( sky_get_post_meta( get_the_ID(), 'sky_new' ) as $new ) : ?>

						<?php echo "<li class=\"item_new\">{$new}</li>"; ?>

					<?php endforeach; ?>
				</ul>
			</div>

		<?php endif; ?>

		<?php if ( sky_get_post_meta( get_the_ID(), 'sky_video_url' ) ) : ?>

			<?php sky_show_video(); ?>

		<?php endif; ?>
		
		<div class="content-detail">
			<?php the_content(); ?>
		</div>

		<?php sky_add_social(); ?>

	</div><!-- .entry-content -->

	<?php if ( current_user_can( 'manage_options' ) ) : ?>
		<footer class="entry-footer">
			<?php sky_game_entry_footer(); ?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>

	<div class="clearfix"></div>

	<?php sky_get_list_download( esc_html__( 'Tải về máy', 'sky-game' ) ); ?>
	
	<?php sky_get_related_tag(); ?>

	<?php sky_get_info_other( 'Thông tin khác' ); ?>

	<?php sky_get_post_nav( 'sky-game', sky_get_option_setting( 'sky_general', 'category_game_slug', 'sky-game-cat' ) ); ?>

	<?php sky_the_tag(); ?>

	<?php if ( !sky_get_post_meta( get_the_ID(), 'sky_hide_comment_facebook' ) ) : ?>

		<div class="sky-comment"><?php esc_html_e( 'Bình luận', 'sky-game' ); ?></div>
		<div class="fb-comments" data-href="<?php the_permalink(); ?>" data-numposts="5" data-width="100%"></div>

	<?php endif; ?>

</article><!-- #post-## -->
