jQuery(document).ready(function($) {
	var count = 1;
	$('.list-entry').hide();
	$('.select_icon').on('click', 'i', function(event) {
		event.preventDefault();

		var list = [
			"fa-adjust", "fa-anchor", "fa-archive", "fa-area-chart", "fa-arrows", "fa-arrows-h", "fa-arrows-v", "fa-asterisk", "fa-at", "fa-automobile", "fa-balance-scale", "fa-ban", "fa-bank", "fa-bar-chart", "fa-bar-chart-o", "fa-barcode", "fa-bars", "fa-battery-0", "fa-battery-1", "fa-battery-2", "fa-battery-3", "fa-battery-4", "fa-battery-empty", "fa-battery-full", "fa-battery-half", "fa-battery-quarter", "fa-battery-three-quarters", "fa-bed", "fa-beer", "fa-bell", "fa-bell-o", "fa-bell-slash", "fa-bell-slash-o", "fa-bicycle", "fa-binoculars", "fa-birthday-cake", "fa-bolt", "fa-bomb", "fa-book", "fa-bookmark", "fa-bookmark-o", "fa-briefcase", "fa-bug", "fa-building", "fa-building-o", "fa-bullhorn", "fa-bullseye", "fa-bus", "fa-cab", "fa-calculator", "fa-calendar", "fa-calendar-check-o", "fa-calendar-minus-o", "fa-calendar-o", "fa-calendar-plus-o", "fa-calendar-times-o", "fa-camera", "fa-camera-retro", "fa-car", "fa-caret-square-o-down", "fa-caret-square-o-left", "fa-caret-square-o-right", "fa-caret-square-o-up", "fa-cart-arrow-down", "fa-cart-plus", "fa-cc", "fa-certificate", "fa-check", "fa-check-circle", "fa-check-circle-o", "fa-check-square", "fa-check-square-o", "fa-child", "fa-circle", "fa-circle-o", "fa-circle-o-notch", "fa-circle-thin", "fa-clock-o", "fa-clone", "fa-close", "fa-cloud", "fa-cloud-download", "fa-cloud-upload", "fa-code", "fa-code-fork", "fa-coffee", "fa-cog", "fa-cogs", "fa-comment", "fa-comment-o", "fa-commenting", "fa-commenting-o", "fa-comments", "fa-comments-o", "fa-compass", "fa-copyright", "fa-creative-commons", "fa-credit-card", "fa-crop", "fa-crosshairs", "fa-cube", "fa-cubes", "fa-cutlery", "fa-dashboard", "fa-database", "fa-desktop", "fa-diamond", "fa-dot-circle-o", "fa-download", "fa-edit", "fa-ellipsis-h", "fa-ellipsis-v", "fa-envelope", "fa-envelope-o", "fa-envelope-square", "fa-eraser", "fa-exchange", "fa-exclamation", "fa-exclamation-circle", "fa-exclamation-triangle", "fa-external-link", "fa-external-link-square", "fa-eye", "fa-eye-slash", "fa-eyedropper", "fa-fax", "fa-feed", "fa-female", "fa-fighter-jet", "fa-file-archive-o", "fa-file-audio-o", "fa-file-code-o", "fa-file-excel-o", "fa-file-image-o", "fa-file-movie-o", "fa-file-pdf-o", "fa-file-photo-o", "fa-file-picture-o", "fa-file-powerpoint-o", "fa-file-sound-o", "fa-file-video-o", "fa-file-word-o", "fa-file-zip-o", "fa-film", "fa-filter", "fa-fire", "fa-fire-extinguisher", "fa-flag", "fa-flag-checkered", "fa-flag-o", "fa-flash", "fa-flask", "fa-folder", "fa-folder-o", "fa-folder-open", "fa-folder-open-o", "fa-frown-o", "fa-futbol-o", "fa-gamepad", "fa-gavel", "fa-gear", "fa-gears", "fa-gift", "fa-glass", "fa-globe", "fa-graduation-cap", "fa-group", "fa-hand-grab-o", "fa-hand-lizard-o", "fa-hand-paper-o", "fa-hand-peace-o", "fa-hand-pointer-o", "fa-hand-rock-o", "fa-hand-scissors-o", "fa-hand-spock-o", "fa-hand-stop-o", "fa-hdd-o", "fa-headphones", "fa-heart", "fa-heart-o", "fa-heartbeat", "fa-history", "fa-home", "fa-hotel", "fa-hourglass", "fa-hourglass-1", "fa-hourglass-2", "fa-hourglass-3", "fa-hourglass-end", "fa-hourglass-half", "fa-hourglass-o", "fa-hourglass-start", "fa-i-cursor", "fa-image", "fa-inbox", "fa-industry", "fa-info", "fa-info-circle", "fa-institution", "fa-key", "fa-keyboard-o", "fa-language", "fa-laptop", "fa-leaf", "fa-legal", "fa-lemon-o", "fa-level-down", "fa-level-up", "fa-life-bouy", "fa-life-buoy", "fa-life-ring", "fa-life-saver", "fa-lightbulb-o", "fa-line-chart", "fa-location-arrow", "fa-lock", "fa-magic", "fa-magnet", "fa-mail-forward", "fa-mail-reply", "fa-mail-reply-all", "fa-male", "fa-map", "fa-map-marker", "fa-map-o", "fa-map-pin", "fa-map-signs", "fa-meh-o", "fa-microphone", "fa-microphone-slash", "fa-minus", "fa-minus-circle", "fa-minus-square", "fa-minus-square-o", "fa-mobile", "fa-mobile-phone", "fa-money", "fa-moon-o", "fa-mortar-board", "fa-motorcycle", "fa-mouse-pointer", "fa-music", "fa-navicon", "fa-newspaper-o", "fa-object-group", "fa-object-ungroup", "fa-paint-brush", "fa-paper-plane", "fa-paper-plane-o", "fa-paw", "fa-pencil", "fa-pencil-square", "fa-pencil-square-o", "fa-phone", "fa-phone-square", "fa-photo", "fa-picture-o", "fa-pie-chart", "fa-plane", "fa-plug", "fa-plus", "fa-plus-circle", "fa-plus-square", "fa-plus-square-o", "fa-power-off", "fa-print", "fa-puzzle-piece", "fa-qrcode", "fa-question", "fa-question-circle", "fa-quote-left", "fa-quote-right", "fa-random", "fa-recycle", "fa-refresh", "fa-registered", "fa-remove", "fa-reorder", "fa-reply", "fa-reply-all", "fa-retweet", "fa-road", "fa-rocket", "fa-rss", "fa-rss-square", "fa-search", "fa-search-minus", "fa-search-plus", "fa-send", "fa-send-o", "fa-server", "fa-share", "fa-share-alt", "fa-share-alt-square", "fa-share-square", "fa-share-square-o", "fa-shield", "fa-ship", "fa-shopping-cart", "fa-sign-in", "fa-sign-out", "fa-signal", "fa-sitemap", "fa-sliders", "fa-smile-o", "fa-soccer-ball-o", "fa-sort", "fa-sort-alpha-asc", "fa-sort-alpha-desc", "fa-sort-amount-asc", "fa-sort-amount-desc", "fa-sort-asc", "fa-sort-desc", "fa-sort-down", "fa-sort-numeric-asc", "fa-sort-numeric-desc", "fa-sort-up", "fa-space-shuttle", "fa-spinner", "fa-spoon", "fa-square", "fa-square-o", "fa-star", "fa-star-half", "fa-star-half-empty", "fa-star-half-full", "fa-star-half-o", "fa-star-o", "fa-sticky-note", "fa-sticky-note-o", "fa-street-view", "fa-suitcase", "fa-sun-o", "fa-support", "fa-tablet", "fa-tachometer", "fa-tag", "fa-tags", "fa-tasks", "fa-taxi", "fa-television", "fa-terminal", "fa-thumb-tack", "fa-thumbs-down", "fa-thumbs-o-down", "fa-thumbs-o-up", "fa-thumbs-up", "fa-ticket", "fa-times", "fa-times-circle", "fa-times-circle-o", "fa-tint", "fa-toggle-down", "fa-toggle-left", "fa-toggle-off", "fa-toggle-on", "fa-toggle-right", "fa-toggle-up", "fa-trademark", "fa-trash", "fa-trash-o", "fa-tree", "fa-trophy", "fa-truck", "fa-tty", "fa-tv", "fa-umbrella", "fa-university", "fa-unlock", "fa-unlock-alt", "fa-unsorted", "fa-upload", "fa-user", "fa-user-plus", "fa-user-secret", "fa-user-times", "fa-users", "fa-video-camera", "fa-volume-down", "fa-volume-off", "fa-volume-up", "fa-warning", "fa-wheelchair", "fa-wifi", "fa-wrench", "fa-hand-grab-o", "fa-hand-lizard-o", "fa-hand-o-down", "fa-hand-o-left", "fa-hand-o-right", "fa-hand-o-up", "fa-hand-paper-o", "fa-hand-peace-o", "fa-hand-pointer-o", "fa-hand-rock-o", "fa-hand-scissors-o", "fa-hand-spock-o", "fa-hand-stop-o", "fa-thumbs-down", "fa-thumbs-o-down", "fa-thumbs-o-up", "fa-thumbs-up", "fa-ambulance", "fa-automobile", "fa-bicycle", "fa-bus", "fa-cab", "fa-car", "fa-fighter-jet", "fa-motorcycle", "fa-plane", "fa-rocket", "fa-ship", "fa-space-shuttle", "fa-subway", "fa-taxi", "fa-train", "fa-truck", "fa-wheelchair", "fa-genderless", "fa-intersex", "fa-mars", "fa-mars-double", "fa-mars-stroke", "fa-mars-stroke-h", "fa-mars-stroke-v", "fa-mercury", "fa-neuter", "fa-transgender", "fa-transgender-alt", "fa-venus", "fa-venus-double", "fa-venus-mars", "fa-file", "fa-file-archive-o", "fa-file-audio-o", "fa-file-code-o", "fa-file-excel-o", "fa-file-image-o", "fa-file-movie-o", "fa-file-o", "fa-file-pdf-o", "fa-file-photo-o", "fa-file-picture-o", "fa-file-powerpoint-o", "fa-file-sound-o", "fa-file-text", "fa-file-text-o", "fa-file-video-o", "fa-file-word-o", "fa-file-zip-o", "fa-circle-o-notch", "fa-cog", "fa-gear", "fa-refresh", "fa-spinner", "fa-check-square", "fa-check-square-o", "fa-circle", "fa-circle-o", "fa-dot-circle-o", "fa-minus-square", "fa-minus-square-o", "fa-plus-square", "fa-plus-square-o", "fa-square", "fa-square-o", "fa-cc-amex", "fa-cc-diners-club", "fa-cc-discover", "fa-cc-jcb", "fa-cc-mastercard", "fa-cc-paypal", "fa-cc-stripe", "fa-cc-visa", "fa-credit-card", "fa-google-wallet", "fa-paypal", "fa-area-chart", "fa-bar-chart", "fa-bar-chart-o", "fa-line-chart", "fa-pie-chart", "fa-bitcoin", "fa-btc", "fa-cny", "fa-dollar", "fa-eur", "fa-euro", "fa-gbp", "fa-gg", "fa-gg-circle", "fa-ils", "fa-inr", "fa-jpy", "fa-krw", "fa-money", "fa-rmb", "fa-rouble", "fa-rub", "fa-ruble", "fa-rupee", "fa-shekel", "fa-sheqel", "fa-try", "fa-turkish-lira", "fa-usd", "fa-won", "fa-yen", "fa-align-center", "fa-align-justify", "fa-align-left", "fa-align-right", "fa-bold", "fa-chain", "fa-chain-broken", "fa-clipboard", "fa-columns", "fa-copy", "fa-cut", "fa-dedent", "fa-eraser", "fa-file", "fa-file-o", "fa-file-text", "fa-file-text-o", "fa-files-o", "fa-floppy-o", "fa-font", "fa-header", "fa-indent", "fa-italic", "fa-link", "fa-list", "fa-list-alt", "fa-list-ol", "fa-list-ul", "fa-outdent", "fa-paperclip", "fa-paragraph", "fa-paste", "fa-repeat", "fa-rotate-left", "fa-rotate-right", "fa-save", "fa-scissors", "fa-strikethrough", "fa-subscript", "fa-superscript", "fa-table", "fa-text-height", "fa-text-width", "fa-th", "fa-th-large", "fa-th-list", "fa-underline", "fa-undo", "fa-unlink", "fa-angle-double-down", "fa-angle-double-left", "fa-angle-double-right", "fa-angle-double-up", "fa-angle-down", "fa-angle-left", "fa-angle-right", "fa-angle-up", "fa-arrow-circle-down", "fa-arrow-circle-left", "fa-arrow-circle-o-down", "fa-arrow-circle-o-left", "fa-arrow-circle-o-right", "fa-arrow-circle-o-up", "fa-arrow-circle-right", "fa-arrow-circle-up", "fa-arrow-down", "fa-arrow-left", "fa-arrow-right", "fa-arrow-up", "fa-arrows", "fa-arrows-alt", "fa-arrows-h", "fa-arrows-v", "fa-caret-down", "fa-caret-left", "fa-caret-right", "fa-caret-square-o-down", "fa-caret-square-o-left", "fa-caret-square-o-right", "fa-caret-square-o-up", "fa-caret-up", "fa-chevron-circle-down", "fa-chevron-circle-left", "fa-chevron-circle-right", "fa-chevron-circle-up", "fa-chevron-down", "fa-chevron-left", "fa-chevron-right", "fa-chevron-up", "fa-exchange", "fa-hand-o-down", "fa-hand-o-left", "fa-hand-o-right", "fa-hand-o-up", "fa-long-arrow-down", "fa-long-arrow-left", "fa-long-arrow-right", "fa-long-arrow-up", "fa-toggle-down", "fa-toggle-left", "fa-toggle-right", "fa-toggle-up", "fa-arrows-alt", "fa-backward", "fa-compress", "fa-eject", "fa-expand", "fa-fast-backward", "fa-fast-forward", "fa-forward", "fa-pause", "fa-play", "fa-play-circle", "fa-play-circle-o", "fa-random", "fa-step-backward", "fa-step-forward", "fa-stop", "fa-youtube-play", "fa-500px", "fa-adn", "fa-amazon", "fa-android", "fa-angellist", "fa-apple", "fa-behance", "fa-behance-square", "fa-bitbucket", "fa-bitbucket-square", "fa-bitcoin", "fa-black-tie", "fa-btc", "fa-buysellads", "fa-cc-amex", "fa-cc-diners-club", "fa-cc-discover", "fa-cc-jcb", "fa-cc-mastercard", "fa-cc-paypal", "fa-cc-stripe", "fa-cc-visa", "fa-chrome", "fa-codepen", "fa-connectdevelop", "fa-contao", "fa-css3", "fa-dashcube", "fa-delicious", "fa-deviantart", "fa-digg", "fa-dribbble", "fa-dropbox", "fa-drupal", "fa-empire", "fa-expeditedssl", "fa-facebook", "fa-facebook-f", "fa-facebook-official", "fa-facebook-square", "fa-firefox", "fa-flickr", "fa-fonticons", "fa-forumbee", "fa-foursquare", "fa-ge", "fa-get-pocket", "fa-gg", "fa-gg-circle", "fa-git", "fa-git-square", "fa-github", "fa-github-alt", "fa-github-square", "fa-gittip", "fa-google", "fa-google-plus", "fa-google-plus-square", "fa-google-wallet", "fa-gratipay", "fa-hacker-news", "fa-houzz", "fa-html5", "fa-instagram", "fa-internet-explorer", "fa-ioxhost", "fa-joomla", "fa-jsfiddle", "fa-lastfm", "fa-lastfm-square", "fa-leanpub", "fa-linkedin", "fa-linkedin-square", "fa-linux", "fa-maxcdn", "fa-meanpath", "fa-medium", "fa-odnoklassniki", "fa-odnoklassniki-square", "fa-opencart", "fa-openid", "fa-opera", "fa-optin-monster", "fa-pagelines", "fa-paypal", "fa-pied-piper", "fa-pied-piper-alt", "fa-pinterest", "fa-pinterest-p", "fa-pinterest-square", "fa-qq", "fa-ra", "fa-rebel", "fa-reddit", "fa-reddit-square", "fa-renren", "fa-safari", "fa-sellsy", "fa-share-alt", "fa-share-alt-square", "fa-shirtsinbulk", "fa-simplybuilt", "fa-skyatlas", "fa-skype", "fa-slack", "fa-slideshare", "fa-soundcloud", "fa-spotify", "fa-stack-exchange", "fa-stack-overflow", "fa-steam", "fa-steam-square", "fa-stumbleupon", "fa-stumbleupon-circle", "fa-tencent-weibo", "fa-trello", "fa-tripadvisor", "fa-tumblr", "fa-tumblr-square", "fa-twitch", "fa-twitter", "fa-twitter-square", "fa-viacoin", "fa-vimeo", "fa-vimeo-square", "fa-vine", "fa-vk", "fa-wechat", "fa-weibo", "fa-weixin", "fa-whatsapp", "fa-wikipedia-w", "fa-windows", "fa-wordpress", "fa-xing", "fa-xing-square", "fa-y-combinator", "fa-y-combinator-square", "fa-yahoo", "fa-yc", "fa-yc-square", "fa-yelp", "fa-youtube", "fa-youtube-play", "fa-youtube-square",
		];
		var content = '';
		$.each(list, function(i, icon) {
			content += "<span data-tooltip=\""+ icon +"\"><i data-icon=\"" + icon + "\" class=\"fa " + icon + "\"></i></span>";
		});
		if ( count % 2 === 0 ) {
			// --- 
				$('.list-entry').hide('slow');

		} else { 
			// ---
				$('.list-entry').show('slow');
				$('.list-entry' + ' .list-icon').html(content);

		}

		$('.list-entry').on('click', 'i', function(event) {
			event.preventDefault();
			var icon = $(this).data('icon');
			$('.list-entry i').css('background', 'transparent');
			$(this).css('background', '#E7DEDE');
			$('#os_icon').val( icon );
			$('.icon').html( "<i class=\"fa " + icon + "\"></i>" ).show('slow');
			// $('.search').val(icon);
		});

		// -- live search icon
			$('.search').keyup(function(event) {
				$('.list-entry .list-icon').html();
				var content_live = '';
				var search_icon = this.value;
				$.each(list, function(i, icon){
			        if (icon.indexOf(search_icon) !== -1) {
			        	content_live += "<span data-tooltip=\""+ icon +"\"><i data-icon=\"" + icon + "\" class=\"fa " + icon + "\"></i></span>";
			            // console.log('found in array '+i, icon);
			        }
			    });
			    $('.list-entry .list-icon').html(content_live);
			});

		// -- 
			count++;

	});

	var skySidebarGenerator = {

			// Sidebars accordion
			accordion: function(){
				jQuery("#sky-sidebars").on("click", "h3.accordion-section-title", function(){
					var current = $(this);
					
					if( current.parents("li.accordion-section").hasClass("open") ){
						$(this).parents("li.accordion-section").removeClass("open");
						$("#sky-sidebars .accordion-section-content").slideUp("fast");
					}
					else{
						$("#sky-sidebars .accordion-section-content").slideUp("fast");
						$(this).next().slideDown("fast");

						$("#sky-sidebars li.accordion-section").removeClass("open");
						$(this).parents("li.accordion-section").addClass("open");
					}
				});
			},

			// Close all accordion sections
			closeAllAccordionSections: function(){
				$("#sky-sidebars li.accordion-section").removeClass("open");
				$("#sky-sidebars .accordion-section-content").slideUp("fast");
			},

			// Make accordion sections sortable
			sortableAccordionSections: function(){
				var blocks = jQuery("#sky-sidebars ul.connected-sidebars-lists, #sky-removed-sidebars ul");
				blocks.sortable({
					items: "> li",
					axis: "y",
					tolerance: "pointer",
					connectWith: ".connected-sidebars-lists",
					handle: ".sky-sidebar-section-icon",
					// cancel: '.moderate-sidebar, .accordion-section-content',
					start: function( event, ui ) {
						skySidebarGenerator.closeAllAccordionSections();
					}
				});
				blocks.find('h3.accordion-section-title').disableSelection();
			},

			// Random ID
			randomID: function(_nr, mode){
				var text = "",
					nb = "0123456789",
					lt = "abcdefghijklmnopqrstuvwxyz",
					possible;
					if( mode == 'l' ){
						possible = lt;
					}
					else if( mode == 'n' ){
						possible = nb;
					}
					else{
						possible = nb + lt;
					}

				for( var i=0; i < _nr; i++ ){
					text += possible.charAt(Math.floor(Math.random() * possible.length));
				}

				return text;
			},

			// Add new sidebar
			addNew: function(){

				var counter = $('#sky-sidebar-generator-counter').val();
				counter = ( counter ) ? parseInt( counter, 10 ) : 0;

				jQuery(".add-new-sidebar").on("click", function(event){
					counter = counter + 1;
					var template       = $('.sidebar-template').clone(),
					    sidebar_prefix = $(this).data('sidebars-prefix'),
					    id             = sidebar_prefix + counter + skySidebarGenerator.randomID(2, 'n') + skySidebarGenerator.randomID(3, 'l'); 
					
					template.removeClass('sidebar-template');

					// Inputs
					template.find('input, select').each(function(){
						var name  = $(this).attr('name');
						var value = $(this).attr('value');
						$(this).attr( 'name', name.replace( '__id__', id ) );
						if( $(this).attr( 'value' ) ){
							$(this).attr( 'value', value.replace( '__id__', id ).replace( '__index__', counter ) );
						}
					});

					// Condition button
					var new_button_name = template.find('.condition-add').data( 'name' ).replace( '__id__', id );
					template.find('.condition-add').attr( 'data-name', new_button_name );
					template.find('.condition-add').attr( 'data-sidebar-id', id );

					// Index
					var h3 = template.find('h3.accordion-section-title span.name').html().replace( '__index__', counter );
					template.find('h3.accordion-section-title span.name').html( h3 );

					// Template ID
					var template_id = template.attr('id');
					template.attr('id', template_id.replace( '__id__', id ))

					// Close other accordion sections
					skySidebarGenerator.closeAllAccordionSections();

					// Append the new sidebar as a new accordion section and slide down it
					template.appendTo('#sky-sidebars ul.connected-sidebars-lists').addClass("open").hide();
					template.find(".accordion-section-content").show();
					template.slideDown('fast');

					$('#sky-sidebar-generator-counter').val( counter );

					event.stopImmediatePropagation();
				}).disableSelection();
			},

			// Live name and description update
			liveSet: function(){
				var container = jQuery('#sky-sidebars');

				container.on('change', '.sky-sidebar-name', function(){
					$(this).parents('li').find('h3.accordion-section-title span.name').html( $(this).val() );

				}).on('keyup', '.sky-sidebar-name', function(){
					$(this).parents('li').find('h3.accordion-section-title span.name').html( $(this).val() );

				});

				container.on('change', '.sky-sidebar-description', function(){
					$(this).parents('li').find('h3.accordion-section-title span.description').html( $(this).val() );

				}).on('keyup', '.sky-sidebar-description', function(){
					$(this).parents('li').find('h3.accordion-section-title span.description').html( $(this).val() );

				});
			},

			// Delete sidebar
			deleteSidebar: function(){
				jQuery("#sky-sidebars").on("click", ".sky-delete-sidebar", function(){

					$('.wrap').addClass('sbg-removed-active');// Show removed sidebars

					$(this).parents('li').slideUp('fast', function() {
						$(this).find('.accordion-section-content').hide(); 
						$(this).appendTo('#sky-removed-sidebars ul').slideDown('fast').removeClass('open'); 
					});
				});
			},
				
			// Restore sidebar
			restoreSidebar: function(){
				jQuery("#sky-removed-sidebars").on("click", ".sky-restore-sidebar", function(){
					$(this).parents('li').slideUp('fast', function() { 
						$(this).find('.accordion-section-content').hide(); 
						$(this).appendTo('#sky-sidebars ul.connected-sidebars-lists').slideDown('fast').removeClass('open'); 
					});
				});
			},

			// Get specific options for current condition choice via ajax
			targetIfCondition: function(){
				jQuery("#sky-sidebars").on("change", ".condition-if", function(){
					var condition_parent = $(this).parents('.condition-parent'),
					    selected = $(this).val(),
					    to_change = condition_parent.find('.condition-equalto');

					to_change.empty();

					jQuery.ajax({
						type: "POST",
						url: ajaxurl,
						dataType: "json",
						data: {
							'action': 'sky-game_load_equalto',
							'data':   { condition_if: selected }
						},
						success: function(response){
							$.each(response, function(key, value) { 
								to_change.prepend($("<option></option>").attr("value",key).text(value)); 
							});

							$("body").append( $("<script />", {
								id: 'condition_if_' + selected.replace("::", "_"),
								html: response
							}) );
						},
						complete: function(response){
						}
					});//ajax
				});
			},

			// Clone a condition. Mainly used to add new condition. That's a fake clone
			conditionAdd: function(){
				$('#sky-sidebars').on('click', '.condition-add', function( event ){
					event.preventDefault();
					var condition_all    = $(this).prev('.created-conditions'),
					    _name_           = $(this).data('name'),
					    _sidebar_id_           = $(this).data('sidebar-id'),
						cloned_elem      = $('.sky-sidebars-condition-template .condition-parent').clone(),
						max_index        = 0;

					condition_all.find('select').each(function(){
					var 
						name       = $(this).attr('name'),
						this_nr    = name.match(/\[(\d+)\]/),
						the_number = parseInt( this_nr[1], 10 );

						if( the_number > max_index ){
							max_index = the_number;
						}
					});

					cloned_elem.find('select').each(function( index, elem ){
						var new_name  = $(elem).attr('name');
						$(elem).attr( 'name', new_name.replace( '__cond_name__', _name_ ).replace( '__id__', _sidebar_id_ ).replace( /\[\d+\]/g, '['+ (max_index + 1) +']' ) );
					});
					cloned_elem.find('select option').each(function(){
						$(this).removeAttr('selected');
					});

					cloned_elem.hide(); //Hide new condition
					condition_all.append( cloned_elem ); //Appent it
					cloned_elem.slideDown('fast'); //... and finally slide it down

					skySidebarGenerator.sortableconditions();
				});
			},
			
			// Remove a condition
			conditionRemove: function(){
				$('#sky-sidebars').on('click', '.condition-remove', function(){
					if( $(this).parents('.created-conditions').find('.condition-parent').length > 1 ){
						$(this).parents('.condition-parent').slideUp( "fast", function() {
							$(this).remove();
						});
					}
				});
			},

			// Enable conditions
			enableConditions: function(){
				$('#sky-sidebars').on('change', '.sky-sidebar-enable-conditions', function(){
					var _t = $(this),
					    _crConditions  = _t.parents('.sky-sidebar-row').children('.created-conditions'),
					    _conditionsBtn = _t.parents('.sky-sidebar-row').children('.condition-add');
					if( _t.is( ":checked" ) ){
						_crConditions.removeClass('disabled-conditions');
						_conditionsBtn.removeAttr('disabled', 'disabled');
					}
					else{
						_crConditions.addClass('disabled-conditions');
						_conditionsBtn.attr('disabled', 'disabled');
					}
				});
			},

			// Make conditions sortable
			sortableconditions: function(){
				var blocks = jQuery("#sky-sidebars .created-conditions");
				blocks.sortable({
					items: "> .condition-parent",
					axis: "y",
					tolerance: "pointer",
					handle: ".sky-sidebar-condition-icon",
					// cancel: '.condition-clone, .condition-remove'
				});
				// blocks.disableSelection();
			},

			// Init all
			init: function(){
				skySidebarGenerator.accordion();
				skySidebarGenerator.sortableAccordionSections();
				skySidebarGenerator.addNew();
				skySidebarGenerator.liveSet();
				skySidebarGenerator.deleteSidebar();
				skySidebarGenerator.restoreSidebar();
				skySidebarGenerator.targetIfCondition();
				skySidebarGenerator.conditionAdd();
				skySidebarGenerator.conditionRemove();
				skySidebarGenerator.enableConditions();
				skySidebarGenerator.sortableconditions();
			},

		};

		// Construct the object
		skySidebarGenerator.init();

});

// ==== Mega Menu
jQuery( function( $ ) { 
	var count = 1;
	$(document).on('click', '.sky-icon .select_icon', function(event) {
		event.stopPropagation();
		event.preventDefault();
		var id = $(this).data('id');
		var icons = $('.search-'+ id).val( $('#edit-menu-item-megamenu_icon-'+id).val() );
		var list = [
			"fa-adjust", "fa-anchor", "fa-archive", "fa-area-chart", "fa-arrows", "fa-arrows-h", "fa-arrows-v", "fa-asterisk", "fa-at", "fa-automobile", "fa-balance-scale", "fa-ban", "fa-bank", "fa-bar-chart", "fa-bar-chart-o", "fa-barcode", "fa-bars", "fa-battery-0", "fa-battery-1", "fa-battery-2", "fa-battery-3", "fa-battery-4", "fa-battery-empty", "fa-battery-full", "fa-battery-half", "fa-battery-quarter", "fa-battery-three-quarters", "fa-bed", "fa-beer", "fa-bell", "fa-bell-o", "fa-bell-slash", "fa-bell-slash-o", "fa-bicycle", "fa-binoculars", "fa-birthday-cake", "fa-bolt", "fa-bomb", "fa-book", "fa-bookmark", "fa-bookmark-o", "fa-briefcase", "fa-bug", "fa-building", "fa-building-o", "fa-bullhorn", "fa-bullseye", "fa-bus", "fa-cab", "fa-calculator", "fa-calendar", "fa-calendar-check-o", "fa-calendar-minus-o", "fa-calendar-o", "fa-calendar-plus-o", "fa-calendar-times-o", "fa-camera", "fa-camera-retro", "fa-car", "fa-caret-square-o-down", "fa-caret-square-o-left", "fa-caret-square-o-right", "fa-caret-square-o-up", "fa-cart-arrow-down", "fa-cart-plus", "fa-cc", "fa-certificate", "fa-check", "fa-check-circle", "fa-check-circle-o", "fa-check-square", "fa-check-square-o", "fa-child", "fa-circle", "fa-circle-o", "fa-circle-o-notch", "fa-circle-thin", "fa-clock-o", "fa-clone", "fa-close", "fa-cloud", "fa-cloud-download", "fa-cloud-upload", "fa-code", "fa-code-fork", "fa-coffee", "fa-cog", "fa-cogs", "fa-comment", "fa-comment-o", "fa-commenting", "fa-commenting-o", "fa-comments", "fa-comments-o", "fa-compass", "fa-copyright", "fa-creative-commons", "fa-credit-card", "fa-crop", "fa-crosshairs", "fa-cube", "fa-cubes", "fa-cutlery", "fa-dashboard", "fa-database", "fa-desktop", "fa-diamond", "fa-dot-circle-o", "fa-download", "fa-edit", "fa-ellipsis-h", "fa-ellipsis-v", "fa-envelope", "fa-envelope-o", "fa-envelope-square", "fa-eraser", "fa-exchange", "fa-exclamation", "fa-exclamation-circle", "fa-exclamation-triangle", "fa-external-link", "fa-external-link-square", "fa-eye", "fa-eye-slash", "fa-eyedropper", "fa-fax", "fa-feed", "fa-female", "fa-fighter-jet", "fa-file-archive-o", "fa-file-audio-o", "fa-file-code-o", "fa-file-excel-o", "fa-file-image-o", "fa-file-movie-o", "fa-file-pdf-o", "fa-file-photo-o", "fa-file-picture-o", "fa-file-powerpoint-o", "fa-file-sound-o", "fa-file-video-o", "fa-file-word-o", "fa-file-zip-o", "fa-film", "fa-filter", "fa-fire", "fa-fire-extinguisher", "fa-flag", "fa-flag-checkered", "fa-flag-o", "fa-flash", "fa-flask", "fa-folder", "fa-folder-o", "fa-folder-open", "fa-folder-open-o", "fa-frown-o", "fa-futbol-o", "fa-gamepad", "fa-gavel", "fa-gear", "fa-gears", "fa-gift", "fa-glass", "fa-globe", "fa-graduation-cap", "fa-group", "fa-hand-grab-o", "fa-hand-lizard-o", "fa-hand-paper-o", "fa-hand-peace-o", "fa-hand-pointer-o", "fa-hand-rock-o", "fa-hand-scissors-o", "fa-hand-spock-o", "fa-hand-stop-o", "fa-hdd-o", "fa-headphones", "fa-heart", "fa-heart-o", "fa-heartbeat", "fa-history", "fa-home", "fa-hotel", "fa-hourglass", "fa-hourglass-1", "fa-hourglass-2", "fa-hourglass-3", "fa-hourglass-end", "fa-hourglass-half", "fa-hourglass-o", "fa-hourglass-start", "fa-i-cursor", "fa-image", "fa-inbox", "fa-industry", "fa-info", "fa-info-circle", "fa-institution", "fa-key", "fa-keyboard-o", "fa-language", "fa-laptop", "fa-leaf", "fa-legal", "fa-lemon-o", "fa-level-down", "fa-level-up", "fa-life-bouy", "fa-life-buoy", "fa-life-ring", "fa-life-saver", "fa-lightbulb-o", "fa-line-chart", "fa-location-arrow", "fa-lock", "fa-magic", "fa-magnet", "fa-mail-forward", "fa-mail-reply", "fa-mail-reply-all", "fa-male", "fa-map", "fa-map-marker", "fa-map-o", "fa-map-pin", "fa-map-signs", "fa-meh-o", "fa-microphone", "fa-microphone-slash", "fa-minus", "fa-minus-circle", "fa-minus-square", "fa-minus-square-o", "fa-mobile", "fa-mobile-phone", "fa-money", "fa-moon-o", "fa-mortar-board", "fa-motorcycle", "fa-mouse-pointer", "fa-music", "fa-navicon", "fa-newspaper-o", "fa-object-group", "fa-object-ungroup", "fa-paint-brush", "fa-paper-plane", "fa-paper-plane-o", "fa-paw", "fa-pencil", "fa-pencil-square", "fa-pencil-square-o", "fa-phone", "fa-phone-square", "fa-photo", "fa-picture-o", "fa-pie-chart", "fa-plane", "fa-plug", "fa-plus", "fa-plus-circle", "fa-plus-square", "fa-plus-square-o", "fa-power-off", "fa-print", "fa-puzzle-piece", "fa-qrcode", "fa-question", "fa-question-circle", "fa-quote-left", "fa-quote-right", "fa-random", "fa-recycle", "fa-refresh", "fa-registered", "fa-remove", "fa-reorder", "fa-reply", "fa-reply-all", "fa-retweet", "fa-road", "fa-rocket", "fa-rss", "fa-rss-square", "fa-search", "fa-search-minus", "fa-search-plus", "fa-send", "fa-send-o", "fa-server", "fa-share", "fa-share-alt", "fa-share-alt-square", "fa-share-square", "fa-share-square-o", "fa-shield", "fa-ship", "fa-shopping-cart", "fa-sign-in", "fa-sign-out", "fa-signal", "fa-sitemap", "fa-sliders", "fa-smile-o", "fa-soccer-ball-o", "fa-sort", "fa-sort-alpha-asc", "fa-sort-alpha-desc", "fa-sort-amount-asc", "fa-sort-amount-desc", "fa-sort-asc", "fa-sort-desc", "fa-sort-down", "fa-sort-numeric-asc", "fa-sort-numeric-desc", "fa-sort-up", "fa-space-shuttle", "fa-spinner", "fa-spoon", "fa-square", "fa-square-o", "fa-star", "fa-star-half", "fa-star-half-empty", "fa-star-half-full", "fa-star-half-o", "fa-star-o", "fa-sticky-note", "fa-sticky-note-o", "fa-street-view", "fa-suitcase", "fa-sun-o", "fa-support", "fa-tablet", "fa-tachometer", "fa-tag", "fa-tags", "fa-tasks", "fa-taxi", "fa-television", "fa-terminal", "fa-thumb-tack", "fa-thumbs-down", "fa-thumbs-o-down", "fa-thumbs-o-up", "fa-thumbs-up", "fa-ticket", "fa-times", "fa-times-circle", "fa-times-circle-o", "fa-tint", "fa-toggle-down", "fa-toggle-left", "fa-toggle-off", "fa-toggle-on", "fa-toggle-right", "fa-toggle-up", "fa-trademark", "fa-trash", "fa-trash-o", "fa-tree", "fa-trophy", "fa-truck", "fa-tty", "fa-tv", "fa-umbrella", "fa-university", "fa-unlock", "fa-unlock-alt", "fa-unsorted", "fa-upload", "fa-user", "fa-user-plus", "fa-user-secret", "fa-user-times", "fa-users", "fa-video-camera", "fa-volume-down", "fa-volume-off", "fa-volume-up", "fa-warning", "fa-wheelchair", "fa-wifi", "fa-wrench", "fa-hand-grab-o", "fa-hand-lizard-o", "fa-hand-o-down", "fa-hand-o-left", "fa-hand-o-right", "fa-hand-o-up", "fa-hand-paper-o", "fa-hand-peace-o", "fa-hand-pointer-o", "fa-hand-rock-o", "fa-hand-scissors-o", "fa-hand-spock-o", "fa-hand-stop-o", "fa-thumbs-down", "fa-thumbs-o-down", "fa-thumbs-o-up", "fa-thumbs-up", "fa-ambulance", "fa-automobile", "fa-bicycle", "fa-bus", "fa-cab", "fa-car", "fa-fighter-jet", "fa-motorcycle", "fa-plane", "fa-rocket", "fa-ship", "fa-space-shuttle", "fa-subway", "fa-taxi", "fa-train", "fa-truck", "fa-wheelchair", "fa-genderless", "fa-intersex", "fa-mars", "fa-mars-double", "fa-mars-stroke", "fa-mars-stroke-h", "fa-mars-stroke-v", "fa-mercury", "fa-neuter", "fa-transgender", "fa-transgender-alt", "fa-venus", "fa-venus-double", "fa-venus-mars", "fa-file", "fa-file-archive-o", "fa-file-audio-o", "fa-file-code-o", "fa-file-excel-o", "fa-file-image-o", "fa-file-movie-o", "fa-file-o", "fa-file-pdf-o", "fa-file-photo-o", "fa-file-picture-o", "fa-file-powerpoint-o", "fa-file-sound-o", "fa-file-text", "fa-file-text-o", "fa-file-video-o", "fa-file-word-o", "fa-file-zip-o", "fa-circle-o-notch", "fa-cog", "fa-gear", "fa-refresh", "fa-spinner", "fa-check-square", "fa-check-square-o", "fa-circle", "fa-circle-o", "fa-dot-circle-o", "fa-minus-square", "fa-minus-square-o", "fa-plus-square", "fa-plus-square-o", "fa-square", "fa-square-o", "fa-cc-amex", "fa-cc-diners-club", "fa-cc-discover", "fa-cc-jcb", "fa-cc-mastercard", "fa-cc-paypal", "fa-cc-stripe", "fa-cc-visa", "fa-credit-card", "fa-google-wallet", "fa-paypal", "fa-area-chart", "fa-bar-chart", "fa-bar-chart-o", "fa-line-chart", "fa-pie-chart", "fa-bitcoin", "fa-btc", "fa-cny", "fa-dollar", "fa-eur", "fa-euro", "fa-gbp", "fa-gg", "fa-gg-circle", "fa-ils", "fa-inr", "fa-jpy", "fa-krw", "fa-money", "fa-rmb", "fa-rouble", "fa-rub", "fa-ruble", "fa-rupee", "fa-shekel", "fa-sheqel", "fa-try", "fa-turkish-lira", "fa-usd", "fa-won", "fa-yen", "fa-align-center", "fa-align-justify", "fa-align-left", "fa-align-right", "fa-bold", "fa-chain", "fa-chain-broken", "fa-clipboard", "fa-columns", "fa-copy", "fa-cut", "fa-dedent", "fa-eraser", "fa-file", "fa-file-o", "fa-file-text", "fa-file-text-o", "fa-files-o", "fa-floppy-o", "fa-font", "fa-header", "fa-indent", "fa-italic", "fa-link", "fa-list", "fa-list-alt", "fa-list-ol", "fa-list-ul", "fa-outdent", "fa-paperclip", "fa-paragraph", "fa-paste", "fa-repeat", "fa-rotate-left", "fa-rotate-right", "fa-save", "fa-scissors", "fa-strikethrough", "fa-subscript", "fa-superscript", "fa-table", "fa-text-height", "fa-text-width", "fa-th", "fa-th-large", "fa-th-list", "fa-underline", "fa-undo", "fa-unlink", "fa-angle-double-down", "fa-angle-double-left", "fa-angle-double-right", "fa-angle-double-up", "fa-angle-down", "fa-angle-left", "fa-angle-right", "fa-angle-up", "fa-arrow-circle-down", "fa-arrow-circle-left", "fa-arrow-circle-o-down", "fa-arrow-circle-o-left", "fa-arrow-circle-o-right", "fa-arrow-circle-o-up", "fa-arrow-circle-right", "fa-arrow-circle-up", "fa-arrow-down", "fa-arrow-left", "fa-arrow-right", "fa-arrow-up", "fa-arrows", "fa-arrows-alt", "fa-arrows-h", "fa-arrows-v", "fa-caret-down", "fa-caret-left", "fa-caret-right", "fa-caret-square-o-down", "fa-caret-square-o-left", "fa-caret-square-o-right", "fa-caret-square-o-up", "fa-caret-up", "fa-chevron-circle-down", "fa-chevron-circle-left", "fa-chevron-circle-right", "fa-chevron-circle-up", "fa-chevron-down", "fa-chevron-left", "fa-chevron-right", "fa-chevron-up", "fa-exchange", "fa-hand-o-down", "fa-hand-o-left", "fa-hand-o-right", "fa-hand-o-up", "fa-long-arrow-down", "fa-long-arrow-left", "fa-long-arrow-right", "fa-long-arrow-up", "fa-toggle-down", "fa-toggle-left", "fa-toggle-right", "fa-toggle-up", "fa-arrows-alt", "fa-backward", "fa-compress", "fa-eject", "fa-expand", "fa-fast-backward", "fa-fast-forward", "fa-forward", "fa-pause", "fa-play", "fa-play-circle", "fa-play-circle-o", "fa-random", "fa-step-backward", "fa-step-forward", "fa-stop", "fa-youtube-play", "fa-500px", "fa-adn", "fa-amazon", "fa-android", "fa-angellist", "fa-apple", "fa-behance", "fa-behance-square", "fa-bitbucket", "fa-bitbucket-square", "fa-bitcoin", "fa-black-tie", "fa-btc", "fa-buysellads", "fa-cc-amex", "fa-cc-diners-club", "fa-cc-discover", "fa-cc-jcb", "fa-cc-mastercard", "fa-cc-paypal", "fa-cc-stripe", "fa-cc-visa", "fa-chrome", "fa-codepen", "fa-connectdevelop", "fa-contao", "fa-css3", "fa-dashcube", "fa-delicious", "fa-deviantart", "fa-digg", "fa-dribbble", "fa-dropbox", "fa-drupal", "fa-empire", "fa-expeditedssl", "fa-facebook", "fa-facebook-f", "fa-facebook-official", "fa-facebook-square", "fa-firefox", "fa-flickr", "fa-fonticons", "fa-forumbee", "fa-foursquare", "fa-ge", "fa-get-pocket", "fa-gg", "fa-gg-circle", "fa-git", "fa-git-square", "fa-github", "fa-github-alt", "fa-github-square", "fa-gittip", "fa-google", "fa-google-plus", "fa-google-plus-square", "fa-google-wallet", "fa-gratipay", "fa-hacker-news", "fa-houzz", "fa-html5", "fa-instagram", "fa-internet-explorer", "fa-ioxhost", "fa-joomla", "fa-jsfiddle", "fa-lastfm", "fa-lastfm-square", "fa-leanpub", "fa-linkedin", "fa-linkedin-square", "fa-linux", "fa-maxcdn", "fa-meanpath", "fa-medium", "fa-odnoklassniki", "fa-odnoklassniki-square", "fa-opencart", "fa-openid", "fa-opera", "fa-optin-monster", "fa-pagelines", "fa-paypal", "fa-pied-piper", "fa-pied-piper-alt", "fa-pinterest", "fa-pinterest-p", "fa-pinterest-square", "fa-qq", "fa-ra", "fa-rebel", "fa-reddit", "fa-reddit-square", "fa-renren", "fa-safari", "fa-sellsy", "fa-share-alt", "fa-share-alt-square", "fa-shirtsinbulk", "fa-simplybuilt", "fa-skyatlas", "fa-skype", "fa-slack", "fa-slideshare", "fa-soundcloud", "fa-spotify", "fa-stack-exchange", "fa-stack-overflow", "fa-steam", "fa-steam-square", "fa-stumbleupon", "fa-stumbleupon-circle", "fa-tencent-weibo", "fa-trello", "fa-tripadvisor", "fa-tumblr", "fa-tumblr-square", "fa-twitch", "fa-twitter", "fa-twitter-square", "fa-viacoin", "fa-vimeo", "fa-vimeo-square", "fa-vine", "fa-vk", "fa-wechat", "fa-weibo", "fa-weixin", "fa-whatsapp", "fa-wikipedia-w", "fa-windows", "fa-wordpress", "fa-xing", "fa-xing-square", "fa-y-combinator", "fa-y-combinator-square", "fa-yahoo", "fa-yc", "fa-yc-square", "fa-yelp", "fa-youtube", "fa-youtube-play", "fa-youtube-square",
		];
		var content = '';
		$.each(list, function(i, icon) {
			content += "<span data-tooltip=\""+ icon +"\"><i data-icon=\"" + icon + "\" class=\"fa " + icon + "\"></i></span>";
		});

		$('.list-entry-'+ id).show('fast');
		$('.list-entry-'+ id + ' .mega-list-icon').html(content);
		$('.color-'+ id).hide();
		
		if ( count % 2 === 0 ) {
			// --- 
				//$('.select_icon i#'+ id).removeClass('fa-arrow-up').addClass('fa-arrow-down');
				$('.list-entry-'+ id).hide('fast');
				//$('.color-'+ id).hide('fast');

		} else { 
			// ---
				//$('.select_icon i#'+ id).removeClass('fa-arrow-down').addClass('fa-arrow-up');
				$('.list-entry-'+ id).show('fast');
				//$('.list-entry-'+ id + ' .mega-list-icon').html(content);
				//$('.color-'+ id).hide('fast');

		}
		
		var curent_icon = $('#edit-menu-item-megamenu_icon-' + id);
		
		if ( curent_icon.val() != '' ) $('.list-entry-'+ id + ' .'+ curent_icon.val()).css('background', '#E7DEDE');


		$('.list-entry-' + id).on('click', 'span i', function(event) {
			event.preventDefault();
			var icon = $(this).data('icon');
			$('.list-entry-' + id + ' span i').css('background', 'transparent');
			$(this).css('background', '#E7DEDE');
			curent_icon.val( icon );
			$('.icon-' + id).html( "<i class=\"fa " + icon + "\"></i>" ).show('fast');
			$('.search-'+ id).val(icon);
		});

		// -- live search icon
			$('.search-'+ id).keyup(function(event) {

				$('.list-entry-'+ id + ' .mega-list-icon').html();
				var content_live = '';
				var search_icon = this.value;

				if ( search_icon == '' ) {

					$('.mega-entry i').css('background', 'transparent');
					$('.icon-' + id).html('').hide('fast');
					$('.search-'+ id).val('');
					$('#edit-menu-item-megamenu_icon-'+ id).val('');

				}

				$.each(list, function(i, icon){
			        if (icon.indexOf(search_icon) !== -1) {
			        	content_live += "<span data-tooltip=\""+ icon +"\"><i data-icon=\"" + icon + "\" class=\"fa " + icon + "\"></i></span>";
			            // console.log('found in array '+i, icon);
			        }
			    });
			    $('.list-entry-'+ id + ' .mega-list-icon').html(content_live);
			});
		// ---
			count++;

	});

	$(document).on('click', '.sky-icon .select_color', function(event) {
		event.stopPropagation();
		event.preventDefault();
		
		var id = $(this).data('id');

		$('.list-entry-'+ id).hide();
		$('.color-'+ id).show('fast');

		if ( count % 2 === 0 ) {
			// --- 
				// $('.list-entry-'+ id).hide('fast');
				$('.color-'+ id).hide('fast');

		} else { 
			// ---
				// $('.list-entry-'+ id).hide('fast');
				$('.color-'+ id).show('fast');

		}

		// -- Select color picker
			$('.color-picker-'+ id).wpColorPicker({
				change: function(event, ui) {
					$( '.icon-' + id ).css('color', ui.color.toCSS());
				}
			});

		// -- Get font size
			$('#edit-menu-item-megamenu_icon_size-'+ id).change(function(event) {
				var size = $(this).val() + 'px';
				$( '.icon-' + id ).css('font-size', size);
			});
		 
		// ---
			count++;

	});

	$(".enable_megamenu").change(function() {
		var id = $(this).data('id');
	    if(this.checked) {
	        $('.enable_megamenu_child-'+ id).show('fast');
	    } else {
	    	$('.enable_megamenu_child-'+ id).hide('fast');
	    }
	});

});

// ---- Setup Install Demo

jQuery(document).ready(function($) {
	// === << Hide main
		$('.hide_main').click(function(event) {
			$('#sky_main_select').toggle('fast');
		});

	// === << Check select setting
		// === << Change post
			$( "#import_post" ).change(function() {
			  	var $input = $( this );
			  	if ( $input.prop( "checked" ) ) {
			  		$('.install-demo').data('import-post', 'true');
			  	} else {
			  		$('.install-demo').data('import-post', 'false');
			  	}
			}).change();

		// === << Change comment
			$( "#import_comment" ).change(function() {
			  	var $input = $( this );
			  	if ( $input.prop( "checked" ) ) {
			  		$('.install-demo').data('import-comment', 'true');
			  	} else {
			  		$('.install-demo').data('import-comment', 'false');
			  	}
			}).change();

		// === << Change nav
			$( "#import_nav" ).change(function() {
			  	var $input = $( this );
			  	if ( $input.prop( "checked" ) ) {
			  		$('.install-demo').data('import-nav', 'true');
			  	} else {
			  		$('.install-demo').data('import-nav', 'false');
			  	}
			}).change();
	
	// --- Check select
		$('#sky_tools').on('click', '.item_tools', function(event) {
			event.preventDefault();
			
			var show = $(this).data('show');
			$('.'+ show).toggle('fast');

		});

	// --- Hover image
		// --- hiden default
			$('#button-1').hide();
		
		// --- Event hover
			$('.sky_hide').on('mouseover', '.item', function(event) {
				event.preventDefault();
				var demo = $(this).data('demo');

				$(this).find('.button-install').show().css({
					background: '#000',
					padding: '5px 10px',
					color: '#fff',
					cursor: 'pointer'
				});

				$(this).find('img').css({
					opacity: '0.7',
					filter: 'alpha(opacity=70)'
				});

				$(this).find('#img-' + demo).css('background', 'rgb(25, 255, 255, 0.8)');
			});

			$('.sky_hide').on('mouseout', '.item', function(event) {
				event.preventDefault();
				$(this).find('.button-install').hide();
				$(this).find('img').css({
					opacity: '1',
					filter: 'alpha(opacity=100)'
				});
			});

	// --- Event click install demo
		$('.theme').on('click', '.install-demo', function(event) {
			event.preventDefault();
			var answer = confirm (skyAdmin.notice);
			if (answer) {
				$('#process_import').html( '<img src="'+ skyAdmin.img_ajax_load +'" />' ).css('text-align', 'center');
			    var name = $(this).data('name');
			    var import_post 	= $(this).data('import-post');
			    var import_nav		= $(this).data('import-nav');
			    var import_comment  = $(this).data('import-comment');
			    var data = {
			    	action : 'process_data',
			    	security : skyAdmin.ajax_nonce,
			    	name : name,
			    	import_post : import_post,
			    	import_nav : import_nav,
			    	import_comment : import_comment
			    };
			    $.post(skyAdmin.ajax_url, data, function(response) {
			    	$('#process_import').html( response );
			    });
			}
			else {
			    
			}
		});

});

jQuery(document).ready(function($) {
	var _checking = {
		security : skyAdmin.security,
		action : 'checking'
	}
	jQuery.ajax({
		url : skyAdmin.ajax_verify,
		type : 'POST',
		data : _checking
	})
	.done(function( result ) {
		var result = jQuery.parseJSON( result );
		if ( result.status == 'error' ) {
			jQuery('#taxonomy-category').remove();
			jQuery('.sky-optimize').remove();
			jQuery('.sky-spinner').remove();
			jQuery('.sky-copyright').remove();
			jQuery('.sky_spinner_settings').remove();
			jQuery('.sky_optimize_database_settings').remove();
			jQuery('.sky_copyright_settings').remove();
			jQuery('#toplevel_page_sky-setup').remove();
			jQuery('#menu-posts-sky-game').remove();
			jQuery('#menu-posts-testimonial').remove();
			alert( result.msg );
		} else if ( result.status == 'ok' ) {
			jQuery('#taxonomy-category').show();
			jQuery('.sky-optimize').show();
			jQuery('.sky-spinner').show();
			jQuery('.sky-copyright').show();
			jQuery('.sky_spinner_settings').show();
			jQuery('.sky_optimize_database_settings').show();
			jQuery('.sky_copyright_settings').show();
			jQuery('#toplevel_page_sky-setup').show();
			jQuery('#menu-posts-sky-game').show();
			jQuery('#menu-posts-testimonial').show();
		} else {
			jQuery('#taxonomy-category').remove();
			jQuery('.sky-optimize').remove();
			jQuery('.sky-spinner').remove();
			jQuery('.sky-copyright').remove();
			jQuery('.sky_spinner_settings').remove();
			jQuery('.sky_optimize_database_settings').remove();
			jQuery('.sky_copyright_settings').remove();
			jQuery('#toplevel_page_sky-setup').remove();
			jQuery('#menu-posts-sky-game').remove();
			jQuery('#menu-posts-testimonial').remove();
			alert( result.msg );
		}
	})
	.fail(function(result) {
		jQuery('#taxonomy-category').remove();
	})
});

// ==== Setup Install

jQuery(document).ready(function($) {
	$('.correct-setting').on('click', function(event) {
		event.preventDefault();
		var title = $(this).data('title');
		var content = $(this).data('content');
		var page_template = $(this).data('page-template');
		var setting_group = $(this).data('setting-group');
		var setting_key = $(this).data('setting-key');
		$this = $(this);
		var data = {
			action : 'sky_setup',
			title : title,
			content : content,
			page_template : page_template,
			setting_group : setting_group,
			setting_key : setting_key
		}
		$.post( skyAdmin.ajax_url, data, function( result ) {
			result = $.parseJSON(result);
			$this.closest('tr').find('mark.error').html( result.id + ' - /' + result.slug + '/' ).removeClass('error').addClass('yes'); // -- event anh em $(this)
			$this.closest('.button').hide();
		});
	});
});

// ==== Spinner Settings
jQuery(document).ready(function(){
	
	// ==== Auto Load
		var synonyms_arr = skyAdmin.file_spinner;
		var lastKey = 0; 
		
		jQuery.each(synonyms_arr, function( index, value ) {
		 
			jQuery('#word_select').append( '<option value="'+ index +'" >'+ value.split('|')[0] +'</option>') ;
 			lastKey = index;
	    
	    });

	    jQuery('#word_synonyms').focus();

	//SELECT WORD
		jQuery('#word_select').change(function(){
		   var arr_index=jQuery('#word_select').val();    
		   var arr_line = synonyms_arr[arr_index];
		   var arr_line_arr=arr_line.split('|');
		   
		   jQuery('#word_synonyms').val('');
		   
		   jQuery('#synonym_word').val( arr_line_arr[0] );
		   
		   jQuery.each(arr_line_arr, function( index, value ) {
			   
			   if(index == 0 ){
				   
			   }else if(index == 1){
				   jQuery('#word_synonyms').val( value );
			   }else{
				   jQuery('#word_synonyms').val( jQuery('#word_synonyms').val() +'\n'+  value );
			   }
			   
			   
		    });
		 
		});
	
	//edit button
		jQuery('#edit_synonyms').click(function(){
			event.preventDefault();
			if(jQuery('#edit_synonyms').attr('disabled')!='disabled'){
				//not disabled 
				jQuery('#word_synonyms').removeAttr('disabled');
				jQuery('#save_synonyms').removeAttr('disabled');
				jQuery('#cancel_synonyms').removeAttr('disabled');    
				jQuery('#delete_synonyms').attr('disabled','disabled');
				jQuery('#edit_synonyms').attr('disabled','disabled');
				jQuery('#word_select').attr('disabled','disabled');
				jQuery('#word_synonyms').focus();

			}

		});
	
	//CANCEL BUTTON

		jQuery('#cancel_synonyms').click(function(){
			event.preventDefault();
			if(jQuery('#cancel_synonyms').attr('disabled')!='disabled'){
		
				//not disabled 
				jQuery('#delete_synonyms').removeAttr('disabled');
				jQuery('#edit_synonyms').removeAttr('disabled');
				jQuery('#word_select').removeAttr('disabled');  
				  
				jQuery('#word_synonyms').attr('disabled','disabled');
				jQuery('#save_synonyms').attr('disabled','disabled');
				jQuery('#cancel_synonyms').attr('disabled','disabled');
				  
				 
			
				jQuery('#word_select').focus();
		
			}

		});
	
	//DELETE BUTTON
		jQuery( '#delete_synonyms' ).click(function(){
			event.preventDefault();
			var selected = jQuery('#word_select').val();
			jQuery('option[value="'+ jQuery('#word_select').val() +'"]').css('background','red').fadeOut('slow',function(){jQuery(this).remove();});
			jQuery('#synonym_word').val('');
			jQuery('#word_synonyms').val('');

			// === set data
				var data = {
					action : 'sky_spinner',
					security : skyAdmin.security,
					id:  selected,
					process : 'delete'
				}

			jQuery.post(skyAdmin.ajax_url, data, function( result ) {
				
				console.log( result );

			});
			  
		});
	
	//SAVE BUTTON
		jQuery('#save_synonyms').click(function(){
			event.preventDefault();
			//not disabled 
			jQuery('#delete_synonyms').removeAttr('disabled');
			jQuery('#edit_synonyms').removeAttr('disabled');
			jQuery('#word_select').removeAttr('disabled');  
			  
			jQuery('#word_synonyms').attr('disabled','disabled');
			jQuery('#save_synonyms').attr('disabled','disabled');
			jQuery('#cancel_synonyms').attr('disabled','disabled');
			
			//update array with new value 
			var newVal=jQuery('#word_synonyms').val();
			newVal_arr=newVal.split('\n');
			var newLine='';
			jQuery.each(newVal_arr,function(index,value){
			    if(jQuery.trim(value) != ''){
			        if(newLine == ''){
			            newLine=jQuery('#synonym_word').val() + '|' + value ;
			        }else{
			            newLine=newLine + '|' + value;
			        }
			    }
			});

			synonyms_arr[jQuery('#word_select').val()]=newLine;

			var selected = jQuery('#word_select').val();

			// === set data
				var data = {
					action : 'sky_spinner',
					security : skyAdmin.security,
					id:  selected,
					process : 'edit',
					value : newLine
				}

			jQuery.post(skyAdmin.ajax_url, data, function( result ) {
				
				console.log( 'ok' );

			});		
			
		});

	//ADD BUTTON
		jQuery('#add_synonyms').click(function(){
			event.preventDefault();
			var newVal = jQuery( '#content' ).val();		
			if( jQuery.trim( newVal ) == '' ) return;
			
			console.log('clicked');

			newVal_arr   = newVal.split('\n');
			var newLine  = '';
			var firstVal = '';
			jQuery.each( newVal_arr, function( index, value ){
			    if( jQuery.trim(value) != '' ){
			        if( newLine == '' ){
						newLine  = value;
						firstVal = value;
			        } else {
			            newLine = newLine + '|' + value;
			        }
			    }
			});
			
			lastKey = parseInt( lastKey ) +1;
			jQuery( '#word_select' ).append( '<option value="'+ lastKey  +'" >'+ firstVal +'</option>') ;
			synonyms_arr[lastKey] = newLine;
			jQuery( '#content' ).val( '' );
			 
			// === set data
				var data = {
					action : 'sky_spinner',
					security : skyAdmin.security,
					id: lastKey,
					process : 'add',
					value : newLine
				}

			jQuery.post(skyAdmin.ajax_url, data, function( result ) {
				
				console.log( result );

			});
			
		});
	
	
	
});//DOC READY


//update the menu function
var updateMenu = function(){
	jQuery("#spinner-editor_ifr").parent().append('<ul style="display:none;position: absolute;" id="spinner_dev"><li>test</li><li>test</li></ul>');

	var mouseX;
	var mouseY;
	jQuery(jQuery("#spinner-editor_ifr").contents()[0], window).bind("mousemove", function(e) {
		mouseX = e.pageX; 
		mouseY = e.pageY;
	});

	var currentSynonym ;
	  
	jQuery("#spinner-editor_ifr").contents().find('.synonym').mouseover(function(){

	currentSynonym=this;

	//empty list
	jQuery('#spinner_dev   li').remove();

	 var synonyms= jQuery(this).attr('synonyms')    ;
	  synonyms_arr=synonyms.split('|') ;
	  jQuery.each(synonyms_arr, function (index, value) {
	        if (value != '') {
			jQuery('#spinner_dev').append('<li>' + value + '</li>');            
	        }
	    });

	  jQuery('#spinner_dev').css({'top':mouseY - 13 +45 - jQuery("#spinner-editor_ifr").contents().find('body').scrollTop(),'left':mouseX -5}).fadeIn('slow');
	  jQuery('#spinner_dev').focus();
	  
	  jQuery('#spinner_dev li').click(function(){
	    
	    jQuery(currentSynonym).html(jQuery(this).html());
	    jQuery('#spinner_dev').hide();
	  });
	 
	});

	jQuery('#spinner_dev').mouseleave(function(){
	  jQuery('#spinner_dev').hide();
	});


	jQuery('#spinner_dev   li').click(function(event){
	    console.log(jQuery(this).html());
	     event.stopImmediatePropagation();
	    return false;
	});

};

jQuery(document).ready(function() {
	
	/*
	 * tabs js
	 */
	jQuery('.tab_head').click(function(){
		jQuery(this).parent().find('li').removeClass('tabs');
		jQuery(this).addClass('tabs');
		jQuery(this).parent().parent().find('div.tabs-panel').hide();
		jQuery(this).parent().parent().find('div.tabs-panel').eq(jQuery(this).index()).show();
		// console.log(jQuery(this).index());
		return false;
	});
	
	
	jQuery('#sky-spinner-post').click(function(){
		/*
		 * post rewrite button: takes tinymce text and rewrite the post
		 */
		
		//triger tinymce save to get instant changes
		jQuery('#content-tmce').trigger('click');
		
		// === set data
			var data = {
				action : 'sky_spinner',
				security : skyAdmin.security,
				process : 'spin',
				post : tinyMCE.get('content').getContent(),
				title : jQuery('#title').val(),
				id : jQuery('#post_ID').val()
			}
		// === Action before click
		
			jQuery('#content-tmce').trigger('click');
			jQuery('.sky-spinner-meta').slideUp('slow');
			jQuery('#sky-ajax-loading1').removeClass('ajax-loading');

		// === Processing
			jQuery.post(skyAdmin.ajax_url, data, function( data, status ) {
				
				if ( status == 'success' ) {

					jQuery('#sky-ajax-loading1').addClass('ajax-loading');
					var res = {
						'status' : 'error',
						'status' : 'fail',
						'msg' : skyAdmin.error
					};
					if(data.substr(0,1) == '{'){
						var res = jQuery.parseJSON(data);
					}

					if (res['status'] == 'success') {

						jQuery('#field-sky-spin-title').val(res['sky_spintaxed_ttl']);
						tinyMCE.get('spinner-editor').setContent(res['sky_spintaxed_cnt2']);
						updateMenu();

						jQuery('#field-sky-spin-titlesyn').val(res['sky_spinner_ttl']);
						jQuery('#field-sky-spin-postsyn').val(res['sky_spinner_cnt']);
						
						jQuery('#field-sky-spin-titleori').val(jQuery('#title').val());
						jQuery('#field-sky-spin-postori').val(tinymce.get('content').getContent());
						
						
						jQuery('.sky-spinner-meta').slideDown('slow');

					}

				} else {
					console.log(res['msg']);
				}

			});

		return false;
	});
	
	/*
	 * rewrite button : sends synonyms for title and post content and return a sky_spintaxed instance
	 */
	
	jQuery('#sky-auto-spinner-regenerate').click(function(){

		// === set data
			var data = {
				action : 'sky_spinner',
				security : skyAdmin.security,
				process : 're_generate',
				post : jQuery('#field-sky-spin-postsyn').val(),
				title : jQuery('#field-sky-spin-titlesyn').val(),
				id : jQuery('#post_ID').val()
			}
		// === Action before click
		
			jQuery('#sky-ajax-loading2').removeClass( 'ajax-loading' );

		// === Processing
			jQuery.post(skyAdmin.ajax_url, data, function( data, status ) {
				
				if ( status == 'success' ) {

					jQuery( '#sky-ajax-loading2' ).addClass( 'ajax-loading' );
					var res = {
						'status' : 'error',
						'status' : 'fail',
						'msg' : skyAdmin.error
				    };

				    if(data.substr(0,1) == '{'){
						var res = jQuery.parseJSON(data);
					}
					
					
					if (res['status'] == 'success') {
						//execute call back
						jQuery('#sky-spin-head').trigger('click');
						jQuery('#field-sky-spin-title').val(res['sky_spintaxed_ttl']);
						tinyMCE.get('spinner-editor').setContent(res['sky_spintaxed_cnt2']);
						updateMenu();
						
		
					} else if (res['status'] == 'fail') {

						console.log(res['msg']);
		
					}

				} else {
					console.log(res['msg']);
				}

			});

		return false;

	});
	
	
	/*
	 * rewrite button 2 : sends synonyms for title and post content and return a sky_spintaxed instance
	 */
	
	jQuery('#sky-auto-spinner-regenerate-re').click(function(){
		
		// === set data
			var data = {
				action : 'sky_spinner',
				security : skyAdmin.security,
				process : 're_generate',
				post : jQuery('#field-sky-spin-postsyn').val(),
				title : jQuery('#field-sky-spin-titlesyn').val(),
				id : jQuery('#post_ID').val()
			}
		// === Action before click
		
			jQuery('#sky-ajax-loading3').removeClass('ajax-loading');

		// === Processing
			jQuery.post(skyAdmin.ajax_url, data, function( data, status ) {
				
				if ( status == 'success' ) {

					jQuery('#sky-ajax-loading3').addClass('ajax-loading');
					var res = {
						'status' : 'error',
						'status' : 'fail',
						'msg' : skyAdmin.error
				    };
				    // console.log(data);

				    if(data.substr(0,1) == '{'){
						var res = jQuery.parseJSON(data);
					}
					
					
					if (res['status'] == 'success') {
						//execute call back
						jQuery('#sky-spin-head').trigger('click');
						jQuery('#field-sky-spin-title').val(res['sky_spintaxed_ttl']);
						tinyMCE.get('spinner-editor').setContent(res['sky_spintaxed_cnt2']);
						updateMenu();
						
		
					} else if (res['status'] == 'fail') {

						console.log(res['msg']);
		
					}

				} else {
					console.log(res['msg']);
				}

			});

		return false;

	});
	
	
	
	/*
	 * send to editor button 
	 */
	
	jQuery('#sky-spinner-stoeditor').click(function(){
		
		jQuery('#title').val(jQuery('#field-sky-spin-title').val());
		
		tinyMCE.get('content').setContent( tinyMCE.get('spinner-editor').getContent() );
		jQuery("#content_ifr").contents().find('.synonym').each(function(){
		    (jQuery(this).replaceWith(jQuery(this).html()));
		});
		jQuery('html, body').animate({
            scrollTop: 0
        });
		return false;
	});
	
	jQuery('#sky-spin-restore').click(function(){
		jQuery('#title').val(jQuery('#field-sky-spin-titleori').val());
		tinyMCE.get('content').setContent(jQuery('#field-sky-spin-postori').val());
		jQuery('html, body').animate({
            scrollTop: 0
        });
		return false;
	});
	
});//docready

jQuery(document).ready(function($) {
	$('.sky-select-clean').on('click', 'input', function(event) {
		// event.preventDefault();
		if ( $(this).is(':checked') ) {
			var data = $(this).val();
			$('#sky-clean-database').data( data, data );
		} else {
			var data = $(this).val();
			$('#sky-clean-database').removeData(data);
		}
	});

	$('#sky-clean-database').click(function(event) {
		event.preventDefault();
		// ==== Get var
			var list_data = {};
				list_data['database'] 	  = $(this).data();
				list_data['action']       = 'sky_optimize';
				list_data['process']      = 'clean';
				list_data['security']     = skyAdmin.security;
			
			// ===== set icon
				$('#sky-clean-database i').addClass('fa-spinner fa-spin');

			$.post(skyAdmin.ajax_url, list_data, function( result ) {
				// ===== set and remove icon
					$('#sky-clean-database i').removeClass('fa-spinner fa-spin').addClass('fa-check').css('color', '#67DC76');

				// ==== set timeout reload page
					setTimeout(function() {
					 	location.reload()
					}, 300);
					   
			});

		// =====

	});

	$('#sky-optimize-database').click(function(event) {
		event.preventDefault();
		// ==== Get var
			var list_data = {};
				list_data['action']       = 'sky_optimize';
				list_data['process']      = 'optimize';
				list_data['security']     = skyAdmin.security;
			
			// ===== set icon
				$('#sky-optimize-database i').addClass('fa-spinner fa-spin');

			$.post(skyAdmin.ajax_url, list_data, function( result ) {
				// ===== set and remove icon
					$('#sky-optimize-database i').removeClass('fa-spinner fa-spin').addClass('fa-check').css('color', '#67DC76');

				// ==== set timeout reload page
					setTimeout(function() {
					 	location.reload()
					}, 300);
					   
			});

		// =====

	});
});