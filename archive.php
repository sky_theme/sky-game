<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Sky Game
 */

get_header(); ?>
<div <?php post_class( 'sky-container' ); ?>>
	<article id="post-<?php the_ID(); ?>" class="sky-xs-12 sky-md-8">
		<?php sky_breadcrumb(); ?>
		<?php if ( have_posts() ) : ?>

			<header>
				<?php
					the_archive_description( '<h2>', '</h2>' );
				?>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post();  $display_type = 'list'; ?>

				<?php

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					include(locate_template("layouts/sky-game-loop.php"));
				?>

			<?php endwhile; ?>

			<?php sky_pagination(); ?>

		<?php endif; ?>

	</article>

	<?php get_sidebar(); ?>
	
</div>
<?php get_footer(); ?>
